<?php

if (!defined('ABSPATH')) {
    exit();
} // Exit if accessed directly

add_filter('fx_section_types', 'fx_section_global_sections', 10, 1);

function fx_section_global_sections($sections) {
    $global_sections = get_posts([
        'post_type' => 'section',
        'posts_per_page' => -1,
    ]);

    foreach ($global_sections as $section) {
        // get_edit_link doesn't work for some reason :/
        $edit_link = get_admin_url(null, sprintf('post.php?post=%d&action=edit', $section->ID));

        $sections[] = [
            'title' => $section->post_title,
            'page' => true,
            'inputs' => [
                '_section_id_' => [
                    'type' => 'textfield',
                    'inputType' => 'hidden',
                    'default' => $section->ID,
                    'description' => sprintf(
                        '<a href="%s" target="_blank">%s</a>',
                        $edit_link,
                        'Edit section'
                    ),
                ],
            ],
        ];
    }

    return $sections;
}

/*------------------------------------*\
    Add meta to page template
\*------------------------------------*/

$page_templates = apply_filters('fx_section_page_templates', []);

$page_sections = new CustomPostMeta(
    'page-sections',
    [
        'title' => 'Page Builder',
        'position' => 'normal',
        'priority' => 'high',
        'prerequisite' => fx_is_using_page_template($page_templates),
    ],
    [
        'sections' => fx_get_sections_meta(),
    ],
    'page'
);

/*------------------------------------*\
    Add meta to other post types
\*------------------------------------*/
$post_types = apply_filters('fx_section_post_types', []);
if (count($post_types) > 0) {
    $post_type_sections = new CustomPostMeta(
        'page-sections',
        [
            'title' => 'Page Builder',
            'position' => 'normal',
            'priority' => 'high',
        ],
        [
            'sections' => fx_get_sections_meta(),
        ],
        $post_types
    );
}

/*------------------------------------*\
    Setup globals CPT
\*------------------------------------*/

$sections = new CPT('section', [
    'singular' => 'Section',
    'plural' => 'Sections',
    'icon' => 'f538',
    'exclude_from_search' => true,
    'publicly_queryable' => false,
    'has_archive' => false,
    'supports' => ['title', 'page-attributes']
]);

$sections->add_meta_section(
    'type',
    [
        'title' => 'Section Content',
        'position' => 'normal',
        'priority' => 'high',
    ],
    [
        'content' => [
            'type' => 'switcher',
            'label' => 'Type',
            'inputs' => fx_get_section_inputs('global'),
        ],
    ]
);

/*------------------------------------*\
    Style gutenberg editor for sections
    CPT and pages using sections
\*------------------------------------*/

function fx_style_gutenberg_for_sections($classes) {
    $should_hide_blocks = apply_filters('fx_hide_gutenberg_blocks_for_sections', true);
    $templates_with_sections = apply_filters('fx_section_page_templates', []);

    global $post;

    if (!(is_admin() && !empty($_GET['post']))) {
        return $classes;
    }

    if ($post->post_type === 'section' || fx_is_using_page_template($templates_with_sections)) {
        if ($should_hide_blocks) {
            $classes .= ' fx-meta-gutenberg--hide-blocks';
        }

        $classes .= ' fx-meta-gutenberg';
    }

    return $classes;
}

add_filter('admin_body_class', 'fx_style_gutenberg_for_sections', 10, 1);

/*------------------------------------*\
    Disable gutenberg for sections CPT
    and pages using sections
\*------------------------------------*/

function fx_disable_gutenberg_for_sections($can_edit, $post_type) {
    $templates_with_sections = apply_filters('fx_section_page_templates', []);

    if (!(is_admin() && !empty($_GET['post']))) {
        return $can_edit;
    }


    if ($post_type === 'section' || fx_is_using_page_template($templates_with_sections)) {
        $should_use = apply_filters('fx_use_gutenberg_for_sections', false);
        return $should_use;
    }

    return $can_edit;
}

add_filter('gutenberg_can_edit_post_type', 'fx_disable_gutenberg_for_sections', 10, 2);
add_filter('use_block_editor_for_post_type', 'fx_disable_gutenberg_for_sections', 10, 2);
