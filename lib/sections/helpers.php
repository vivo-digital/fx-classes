<?php

if (!defined('ABSPATH')) {
    exit();
} // Exit if accessed directly

function fx_register_section($id, $args, $merge_options = []) {
    add_filter('fx_section_types', function($sections) use ($id, $args, $merge_options) {
        if (isset($sections[$id])) {
            // Sections exists already
            $merge_strategy = isset($merge_options['strategy']) ? $merge_options['strategy'] : 'merge';

            switch ($merge_strategy) {
                case 'replace':
                    $sections[$id] = $args;
                    break;
                case 'merge':
                    $sections[$id] = array_merge($sections[$id], $args);
                break;
                case 'merge-deep':
                    $sections[$id] = array_merge_recursive($sections[$id], $args);
                    break;
            }
        } else {
            $sections[$id] = $args;
        }

        return $sections;
    }, 10, 1);
}

function fx_get_section_inputs($type) {
    $sections = [];

    $sections = apply_filters('fx_section_types', $sections);

    $filtered_sections = [];

    foreach ($sections as $id => $section) {
        if (isset($section[$type]) && $section[$type]) {
            $filtered_sections[$section['title']] = array_merge($section['inputs'], [
                '_section_type_' => [
                    'type' => 'textfield',
                    'inputType' => 'hidden',
                    'default' => $id,
                ],
            ]);
        }
    }
    return $filtered_sections;
}

// Helper function to use sections in templates directly
function fx_get_section_part($section_path, $section_args = []) {
    global $section;

    $section = $section_args;

    get_template_part($section_path);
}

function fx_sections_loop($sections) {
    if (!$sections || count($sections) === 0) {
        return;
    }

    $section_types = apply_filters('fx_section_types', []);

    // Declare the global outside the foreach loop
    global $section;

    foreach ($sections as $section_meta) {
        $data = $section_meta['data'];

        $is_global_section = isset($data['_section_id_']);

        if ($is_global_section) {
            $section_post_id = $data['_section_id_'];
            $data = json_decode(fx_get_meta('content', $section_post_id), true);
        }

        // Forward the section _id_ value by adding it to our data object
        if (isset($section_meta['_id_'])) {
            $data['_id_'] = $section_meta['_id_'];
        }

        // Do the same for options
        if (isset($section_meta['options'])) {
            // Use underscores to avoid any conflicts
            $data['_options_'] = $section_meta['options'];
        }

        $section = $data;

        $type = $data['_section_type_'];
        $section_options = $section_types[$type];

        if (!$section_options) {
            if (defined('IS_DEV') && IS_DEV) {
                echo 'MISSING SECTION TYPE: ' . $type;
                var_dump($section_meta);
            }

            // Oh dear!
            continue;
        }

        $template = $section_options['template'];

        get_template_part($template);
    }
}

function fx_get_section_options() {
    $options = [];
    $option_inputs = apply_filters('fx_section_global_options', []);

    if ($option_inputs && count($option_inputs) > 0) {
        $options = [
            'options' => array_merge($option_inputs, [
                'type' => 'group',
                'display' => 'modal',
                'trigger_type' => 'text',
                'class' => 'fx-page-sections__options',
            ]),
        ];
    }

    return $options;
}

function fx_get_sections_meta($args = [], $level = 'page') {
    $options = fx_get_section_options();

    return array_replace_recursive([
        'type' => 'section',
        'use_as_title' => 'data._type_',
        'button_text' => 'Add page section',
        'class' => 'fx-page-sections',
        'class_prefix' => 'fx-',
        'include_id_input' => true,
        'inputs' => array_merge($options, [
            'data' => [
                'type' => 'switcher',
                'label' => 'Section Type:',
                'inputs' => fx_get_section_inputs('page'),
            ],
        ]),
    ], $args);
}
