<?php

if (!defined('ABSPATH')) {
    exit();
} // Exit if accessed directly

// Always import the helpers incase the project relies on them
require_once 'helpers.php';


function fx_register_sections() {
    $sections_enabled = apply_filters('fx_sections_enabled', true);
    if ($sections_enabled) {
        require_once 'register.php';
    }
}

// Register sections after the theme has been setup, this means the theme can delare
// all section types before hand.
add_action('after_setup_theme', 'fx_register_sections');
