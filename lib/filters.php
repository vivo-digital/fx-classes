<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

function fx_unserialize_grouped_data($data, $grouped) {
    // TODO: Remove this after a while, it's just for versions < v1.6.11
    if ($grouped && fx_is_json_encoded($data)) {
        return json_decode($data, true);
    }

    $temp_data = base64_decode($data);
    if (!is_serialized($temp_data)) {
        return $data;
    }

    $unserialized_data = array_map(function($a) {
        return stripcslashes($a);
    }, unserialize($temp_data));

    return $unserialized_data;
}

function fx_is_json_encoded($string){
    return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE);
}

add_filter('fx_meta_content', 'fx_unserialize_grouped_data', 10, 4);

/*------------------------------------*\
    Remove editor (and gutenberg)
    for page templates
\*------------------------------------*/

function fx_remove_editor_for_page_templates() {
    $templates_without_editor = apply_filters('fx_remove_editor_page_templates', []);
    if (fx_is_using_page_template($templates_without_editor)) {
        remove_post_type_support('page', 'editor');
    }
    remove_post_type_support('section', 'editor');
}

add_action('admin_head', 'fx_remove_editor_for_page_templates');
