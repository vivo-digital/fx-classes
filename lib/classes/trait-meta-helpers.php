<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

trait MetaHelpers {
    public function get_input_args($input_args, $override_args) {
        $args = array_merge($input_args, $override_args);
        $value = $args['value'];
        $options = $args['options'];
        // Maintain backwards compatability
        $options = is_callable($options) ? $options() : $options;

        if ($value === '' && isset($args['default'])) {
            $value = $args['default'];
        }

        $value_escaped = is_string($value) ? htmlspecialchars($value) : $value;

        if ($args['inputs'] && is_array($args['inputs'])) {
            $this->format_nested_inputs($args['inputs'], $args['type']);
        }

        return array_merge($args, [
            'options' => $options,
            'value' => $value,
            'value_escaped' => $value_escaped,
        ]);
    }

    private function format_nested_inputs(&$inputs, $type = null) {
        if ($type === 'switcher') {
            foreach ($inputs as $id => &$switcher_inputs) {
                $this->format_nested_inputs($switcher_inputs);
            }
            return;
        }

        foreach ($inputs as $id => &$input) {
            // var_dump($id);
            // Check if options is a function, if so, evaluate it now
            if ($input['options'] && is_callable($input['options'])) {
                $input_options = $input['options']();
                // Update the original reference, not $input
                $input['options'] = $input_options;
            }

            // Set default values on nested inputs
            if (!$input['value'] && isset($input['default'])) {
                $input['value'] = $input['default'];
            }

            if ($input['inputs']) {
                $this->format_nested_inputs($input['inputs'], $input['type']);
            }
        }
    }

    private function get_meta_options($options, $options_args, $post_id) {
        if (is_string($options)) {
            if ($options === 'all') {
                return $this::get_posts_helper(null, $options_args, $post_id);
            }

            $options_string = preg_replace('/\s+/', '', $options);
            $options_array = explode(',', $options_string);

            return $this::get_posts_helper($options_array, $options_args, $post_id);
        }

        return $options;
    }

    static function get_posts_helper($post_types = [], $query_args = [], $post_id = null) {
        $options = [];
        $all_post_types = get_post_types();
        // remove unwanted post types
        unset($all_post_types['attachment']);
        unset($all_post_types['revision']);
        unset($all_post_types['nav_menu_item']);
        unset($all_post_types['custom_css']);
        unset($all_post_types['customize_changeset']);

        // Safety check
        if (!is_array($query_args)) {
            $query_args = [];
        }

        $current_post_id = $post_id ? intval($post_id) : get_the_ID();

        if (isset($query_args['post__not_in'])) {
            // We allow 'this' in post__not_in, so look for it and replace with the current post id
            $query_args['post__not_in'] = array_map(function($p) use ($current_post_id) {
                return $p === 'this' ? $current_post_id : $p;
              }, $query_args['post__not_in']);
        }

        $query_post_types = $post_types ? $post_types : $all_post_types;
        $posts = get_posts(array_merge([
            'post_type' => $query_post_types,
            'posts_per_page' => -1
        ], $query_args));

        foreach ($posts as $post) : setup_postdata($post);
            $options[] = [
                'id' => $post->ID,
                'slug' => $post->post_name,
                'title' => $post->post_title,
                'post_type' => $post->post_type,
            ];
        endforeach;

        wp_reset_postdata();

        return $options;
    }

    static function serialize_grouped_data($data) {
        $serialized_data = base64_encode(serialize($data));

        return $serialized_data;
    }

    public function unserialize_grouped_data($data) {
        // TODO: Remove this after a while, it's just for versions < v1.6.11
        if ($this::is_json_encoded($data)) {
            return json_decode($data, true);
        }

        $unserialized_data = array_map(function($a) {
            return stripcslashes($a);
        }, unserialize(base64_decode($data)));

        return $unserialized_data;
    }

    static function is_json_encoded($string){
       return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE);
    }
}
