<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly


class CustomPostMeta
{

    use MetaBoxes;
    use MetaHelpers;

    static $prefix = FX_CLASSES_PREFIX;

    public function __construct($id, $options, $meta_boxes, $post_type)
    {
        $this->id = $id;
        $this->post_type = $post_type;
        $this->options = $options;

        // Store all meta data as one entry
        $this->is_grouped = isset($options['grouped']) && $options['grouped'] === true;

        // Need to check if meta section is laid out in columns
        $this->has_columns = isset($this->options['columns']) && $this->options['columns'] === true;

        if ($this->has_columns) {
            $this->columns = $meta_boxes;
            $this->meta_boxes = [];
            // Need to concat all column inputs into one large array for saving
            // Use a union to preserve array keys
            foreach ($this->columns as $column) {
                $this->meta_boxes += $column['inputs'];
            }
        } else {
            // No columns, just set meta boxes normally
            $this->meta_boxes = $meta_boxes;
        }

        $this->add_actions();

        if (is_admin()) {
            // Enqueue scripts & styles required by the editor.
            // https://codex.wordpress.org/Javascript_Reference/wp.editor
            wp_enqueue_editor();
        }
    }

    private function add_actions()
    {
        add_action('add_meta_boxes', [$this, 'add_meta_box'], 10, 2);
        add_action('save_post', [$this, 'save_post_data'], 10, 2);
    }

    public function save_post_data($post_id, $post)
    {
        if (!$this->passes_prerequisite($post)) {
            return;
        }

        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return $post_id;
        }

        if (defined('DOING_AJAX') && DOING_AJAX) {
            // Prevents the metaboxes from being overwritten while quick editing.
            return $post_id;
        }

        if (preg_match('/\edit\.php/', $_SERVER['REQUEST_URI'])) {
            // Detects if the save action is coming from a quick edit/batch edit.
            return $post_id;
        }

        foreach ($this->meta_boxes as $slug => $meta_box) {
            $name = $this::$prefix . $slug;
            $ref = $name . '_value';

            if ($this->validate_meta_box($post_id, $name, $meta_box) === true) {
                $data = isset($_POST[$ref]) ? $_POST[$ref] : '';

                // Check if data is to be grouped
                if ($this->is_grouped) {
                    if (!$grouped_data) {
                        $grouped_data = [];
                    }

                    $grouped_data[$slug] = $data;
                    continue;
                }

                // Data is saved individually
                $this->save_meta_data($post_id, $ref, $data);
            }
        }

        if ($this->is_grouped && $grouped_data) {
            $name = $this::$prefix . $this->id;
            $ref = $name . '_value';

            // Encode grouped data
            $data = $this::serialize_grouped_data($grouped_data);

            // Save grouped data
            $this->save_meta_data($post_id, $ref, $data);
        }
    }

    private function validate_meta_box($post_id, $name, $meta_box)
    {
        // Check for readonly
        if (isset($meta_box['readOnly']) && $meta_box['readOnly'] === true) {
            return false;
        }

        // Verify
        if (!isset($_POST[$name . '_noncename']) || !wp_verify_nonce($_POST[$name . '_noncename'])) {
            return false;
        }

        if (isset($_POST['post_type']) && 'page' == $_POST['post_type']) {
            if (!current_user_can('edit_page', $post_id)) {
                return false;
            }
        } else {
            if (!current_user_can('edit_post', $post_id)) {
                return false;
            }
        }

        return true;
    }

    private function save_meta_data($post_id, $ref, $data)
    {
        if (!$post_id || !$ref) {
            return;
        }

        if (get_post_meta($post_id, $ref) === '') {
            add_post_meta($post_id, $ref, $data, true);
        } elseif ($data != get_post_meta($post_id, $ref, true)) {
            update_post_meta($post_id, $ref, $data);
        } elseif ($data === '') {
            delete_post_meta($post_id, $ref, get_post_meta($post_id, $ref, true));
        }
    }

    public function add_meta_box($post_type, $post)
    {
        if (!$this->passes_prerequisite($post)) {
            return;
        }

        // Enqueue media lightbox
        wp_enqueue_media();

        // Check input contains meta array
        if (!function_exists('add_meta_box') || !is_array($this->meta_boxes)) {
            return;
        }

        add_meta_box(
            $this::$prefix . 'meta_' . $this->id,
            $this->options['title'],
            [$this, 'render_meta_box_group'],
            $this->post_type,
            $this->options['position'],
            $this->options['priority'],
            [
                'inputs' => $this->meta_boxes
            ]
        );
    }

    public function render_meta_box_group($post, $metabox)
    {
        if ($this->has_columns) {
            echo '<div class="row">';

            foreach ($this->columns as $column) {
                echo '<div class="' . $column['class'] . '">';
                $this->render_meta_boxes($column['inputs']);
                echo '</div>';
            }

            echo '</div>';
        } else {
            // No colums, render normally
            $this->render_meta_boxes($this->meta_boxes);
        }
    }

    public function render_meta_boxes($meta_boxes_inputs)
    {
        if (!is_array($meta_boxes_inputs)) {
            return;
        }

        global $post;

        foreach ($meta_boxes_inputs as $slug => $meta_box) {
            $name = $this::$prefix . $slug;
            $input_id = $name;
            $ref = $name . '_value';
            $value = $this->is_grouped ?
                $this->get_grouped_data($post->ID, $slug) : get_post_meta($post->ID, $ref, true);

            $input_args = $this->get_input_args($meta_box, [
                'id' => $input_id,
                'name' => $name,
                'ref' => $ref,
                'value' => $value,
            ]);

            MetaBoxes::render_input($input_args);
        }
    }

    private function get_grouped_data($post_id, $slug)
    {
        $name = $this::$prefix . $this->id;
        $ref = $name . '_value';

        $grouped_data = $this->unserialize_grouped_data(get_post_meta($post_id, $ref, true));

        if ($grouped_data && is_array($grouped_data)) {
            return $grouped_data[$slug];
        }

        return null;
    }

    private function passes_prerequisite($post)
    {
        // // Exit early if not the right post type
        if ((is_array($this->post_type) && !in_array($post->post_type, $this->post_type)) ||
            (is_string($this->post_type) && $post->post_type !== $this->post_type)
        ) {
            return false;
        }

        return !isset($this->options['prerequisite']) ?: $this->options['prerequisite'];
    }
}
