<?php

if (!defined('ABSPATH')) {
  exit;
} // Exit if accessed directly

class FXMailgun
{
  function __construct()
  {
    add_action('rest_api_init', [$this, 'fx_register_api_mailgun_hooks']);
    add_filter(
      'pre_wp_mail',
      function ($null, $atts) {
        $message = $atts['message'];
        $to = $atts['to'];
        $subject = $atts['subject'];
        $headers = $atts['headers'];
        $attachments = $atts['attachments'];

        $send = $this->fx_send_mail($to, $subject, $message, $headers, $attachments);
        // @TODO template here as well (from WREN)
        return true;
      },
      10,
      2
    );
    add_action('wp_mail_failed', [$this, 'onMailError'], 10, 1);
    add_filter('mg_mutate_to_rcpt_vars', [$this, 'mg_mutate_to_rcpt_vars_cb']);
  }

  public function fx_register_api_mailgun_hooks()
  {
    register_rest_route(API_NAMESPACE, '/mailgun/test', [
      'methods' => 'POST',
      'callback' => [$this, 'fx_api_mailgun_test'],
    ]);
  }

  function fx_api_mailgun_test()
  {
    if (!is_admin() && !is_user_logged_in()) {
      $response_data = [
        'success' => false,
        'message' => 'This route is restricted to admin',
      ];
    }

    $form_data = $_POST;
    $token = $form_data['token'];
    $success = true;

    global $verified_nonce;

    if (!isset($token) || !$verified_nonce) {
      $response_data = [
        'success' => false,
        'message' => 'Invalid form nonce.',
        'token' => $token,
      ];
    } else {
      try {
        $testDefaults = [
          'to' => get_bloginfo('admin_email'),
          'subject' => 'Test message from ViVO Digital',
          'message' => 'This is a test email message',
          'headers' => ['Content-Type: text/html; charset=UTF-8'],
          'attachments' => []
        ];
        $send = $this->fx_send_mail(
          $testDefaults['to'],
          $testDefaults['subject'],
          $testDefaults['message'],
          $testDefaults['headers'],
          $testDefaults['attachments']
        );
        $success = $send->data['success'];
        $message = $send->data['message'];
      } catch (Exception $e) {
        $success = false;
      }

      $response_data = [
        'success' => $success,
        'message' => $message,
      ];
    }

    return fx_api_response($response_data);
  }

  function onMailError($wp_error)
  {
    if (defined('IS_DEV') && IS_DEV) {
      echo '<pre>';
      print_r($wp_error);
      echo '</pre>';
    }
  }

  function fx_send_mail($to, $subject, $message, $headers, $attachments)
  {

    $send = $this->fx_wp_mail($to, $subject, $message, $headers, $attachments);

    $response_data = [
      'success' => $send,
      'message' => $send ? 'Success! Mailgun API is configured correctly.' : 'Mailgun API has not sent this message.',
    ];

    return fx_api_response($response_data);
  }

  /*
  * The below code is adapated from Mailgun wordpress plugin Version 1.7.9
  */

  /**
   * Tries several methods to get the MIME Content-Type of a file.
   *
   * @param	string	$filepath
   * @param	string	$default_type	If all methods fail, fallback to $default_type
   *
   * @return	string	Content-Type
   *
   * @since	1.5.4
   */
  function get_mime_content_type($filepath, $default_type = 'text/plain')
  {
    if (function_exists('mime_content_type')) {
      return mime_content_type($filepath);
    } elseif (function_exists('finfo_file')) {
      $fi = finfo_open(FILEINFO_MIME_TYPE);
      $ret = finfo_file($fi, $filepath);
      finfo_close($fi);

      return $ret;
    } else {
      return $default_type;
    }
  }


  function mg_build_payload_from_body($body, $boundary)
  {
    $payload = '';

    // Iterate through pre-built params and build payload:
    foreach ($body as $key => $value) {
      if (is_array($value)) {
        $parent_key = $key;
        foreach ($value as $key => $value) {
          $payload .= '--' . $boundary;
          $payload .= "\r\n";
          $payload .= 'Content-Disposition: form-data; name="' . $parent_key . "\"\r\n\r\n";
          $payload .= $value;
          $payload .= "\r\n";
        }
      } else {
        $payload .= '--' . $boundary;
        $payload .= "\r\n";
        $payload .= 'Content-Disposition: form-data; name="' . $key . '"' . "\r\n\r\n";
        $payload .= $value;
        $payload .= "\r\n";
      }
    }

    return $payload;
  }

  /**
   * mg_api_last_error is a compound getter/setter for the last error that was
   * encountered during a Mailgun API call.
   *
   * @param	string	$error	OPTIONAL
   *
   * @return	string	Last error that occurred.
   *
   * @since	1.5.0
   */
  function mg_api_last_error($error = null)
  {
    static $last_error;

    if (null === $error) {
      return $last_error;
    } else {
      $tmp = $last_error;
      $last_error = $error;

      return $tmp;
    }
  }

  function mg_build_payload_from_mime($body, $boundary)
  {
  }

  /*
  * Wordpress filter to mutate a `To` header to use recipient variables.
  * Uses the `mg_use_recipient_vars_syntax` filter to apply the actual
  * change. Otherwise, just a list of `To` addresses will be returned.
  *
  * @param string|array $to_addrs Array or comma-separated list of email addresses to mutate.
  *
  * @return array Array containing list of `To` addresses and recipient vars array
  *
  * @since 1.5.7
  */

  function mg_mutate_to_rcpt_vars_cb($to_addrs)
  {
    if (is_string($to_addrs)) {
      $to_addrs = explode(',', $to_addrs);
    }

    if (has_filter('mg_use_recipient_vars_syntax')) {
      $use_rcpt_vars = apply_filters('mg_use_recipient_vars_syntax', null);
      if ($use_rcpt_vars) {
        $vars = [];

        $idx = 0;
        foreach ($to_addrs as $addr) {
          $rcpt_vars[$addr] = ['batch_msg_id' => $idx];
          $idx++;
        }

        // TODO: Also add folding to prevent hitting the 998 char limit on headers.
        return [
          'to' => '%recipient%',
          'rcpt_vars' => json_encode($rcpt_vars),
        ];
      }
    }

    return [
      'to' => $to_addrs,
      'rcpt_vars' => null,
    ];
  }

  function mg_build_attachments_payload($attachments, $boundary)
  {
    $payload = '';

    // If we have attachments, add them to the payload.
    if (!empty($attachments)) {
      $i = 0;
      foreach ($attachments as $attachment) {
        if (!empty($attachment)) {
          $payload .= '--' . $boundary;
          $payload .= "\r\n";
          $payload .=
            'Content-Disposition: form-data; name="attachment[' .
            $i .
            ']"; filename="' .
            basename($attachment) .
            '"' .
            "\r\n\r\n";
          $payload .= file_get_contents($attachment);
          $payload .= "\r\n";
          $i++;
        }
      }
    } else {
      return null;
    }

    return $payload;
  }

  /**
   * Set the API endpoint based on the region selected.
   * Value can be "0" if not selected, "us" or "eu"
   *
   * @param	string	$getRegion	Region value set either in config or Mailgun plugin settings.
   *
   * @return	bool|string
   *
   * @since	1.5.12
   */
  function mg_api_get_region($getRegion)
  {
    switch ($getRegion) {
      case 'us':
        return 'https://api.mailgun.net/v3/';
      case 'eu':
        return 'https://api.eu.mailgun.net/v3/';
      default:
        return false;
    }
  }

  function fx_wp_mail($to, $subject, $message, $headers = '', $attachments = [])
  {
    // Compact the input, apply the filters, and extract them back out
    extract(
      apply_filters('wp_mail', compact('to', 'subject', 'message', 'headers', 'attachments'))
    );;
    $mailgun = [
      'region' => 'us', // us || eu
      'track-clicks' => '',
      'track-opens' => '',
      'campaign-id' => '',
      'override-from' => '0',
      'tag' => 'localhost',
    ];


    $region = $mailgun['region'];
    $apiKey = empty(get_mailgun_config('apiKey')) ? MAILGUN_API_KEY : get_mailgun_config('apiKey');
    $domain = empty(get_mailgun_config('domain')) ? MAILGUN_DOMAIN : get_mailgun_config('domain');

    if (empty($apiKey) || empty($domain)) {
      return false;
    }

    if (!is_array($attachments)) {
      $attachments = explode("\n", str_replace("\r\n", "\n", $attachments));
    }

    // Headers
    if (empty($headers)) {
      $headers = [];
    } else {
      if (!is_array($headers)) {
        // Explode the headers out, so this function can take both
        // string headers and an array of headers.
        $tempheaders = explode("\n", str_replace("\r\n", "\n", $headers));
      } else {
        $tempheaders = $headers;
      }
      $headers = [];
      $cc = [];
      $bcc = [];

      // If it's actually got contents
      if (!empty($tempheaders)) {
        // Iterate through the raw headers
        foreach ((array) $tempheaders as $header) {
          if (strpos($header, ':') === false) {
            if (false !== stripos($header, 'boundary=')) {
              $parts = preg_split('/boundary=/i', trim($header));
              $boundary = trim(str_replace(["'", '"'], '', $parts[1]));
            }
            continue;
          }
          // Explode them out
          list($name, $content) = explode(':', trim($header), 2);

          // Cleanup crew
          $name = trim($name);
          $content = trim($content);

          switch (strtolower($name)) {
              // Mainly for legacy -- process a From: header if it's there
            case 'from':
              if (strpos($content, '<') !== false) {
                // So... making my life hard again?
                $from_name = substr($content, 0, strpos($content, '<') - 1);
                $from_name = str_replace('"', '', $from_name);
                $from_name = trim($from_name);

                $from_email = substr($content, strpos($content, '<') + 1);
                $from_email = str_replace('>', '', $from_email);
                $from_email = trim($from_email);
              } else {
                $from_email = trim($content);
              }
              break;
            case 'content-type':
              if (strpos($content, ';') !== false) {
                list($type, $charset) = explode(';', $content);
                $content_type = trim($type);
                if (false !== stripos($charset, 'charset=')) {
                  $charset = trim(str_replace(['charset=', '"'], '', $charset));
                } elseif (false !== stripos($charset, 'boundary=')) {
                  $boundary = trim(
                    str_replace(['BOUNDARY=', 'boundary=', '"'], '', $charset)
                  );
                  $charset = '';
                }
              } else {
                $content_type = trim($content);
              }
              break;
            case 'cc':
              $cc = array_merge((array) $cc, explode(',', $content));
              break;
            case 'bcc':
              $bcc = array_merge((array) $bcc, explode(',', $content));
              break;
            default:
              // Add it to our grand headers array
              $headers[trim($name)] = trim($content);
              break;
          }
        }
      }
    }

    $from_name = empty(get_mailgun_config('from_name')) ? MAILGUN_FROM_NAME : get_mailgun_config('from_name');
    $from_email = empty(get_mailgun_config('from_email')) ? MAILGUN_FROM_EMAIL : get_mailgun_config('from_email');

    $body = [
      'from' => "{$from_name} <{$from_email}>",
      'to' => $to,
      'subject' => $subject,
    ];


    $rcpt_data = apply_filters('mg_mutate_to_rcpt_vars', $to);
    if (!is_null($rcpt_data['rcpt_vars'])) {
      $body['recipient-variables'] = $rcpt_data['rcpt_vars'];
    }

    $body['o:tag'] = [];
    $body['o:tracking-clicks'] = !empty($mailgun['track-clicks']) ? $mailgun['track-clicks'] : 'no';
    $body['o:tracking-opens'] = empty($mailgun['track-opens']) ? 'no' : 'yes';

    // this is the wordpress site tag
    if (isset($mailgun['tag'])) {
      $tags = explode(',', str_replace(' ', '', $mailgun['tag']));
      $body['o:tag'] = $tags;
    }

    // campaign-id now refers to a list of tags which will be appended to the site tag
    if (!empty($mailgun['campaign-id'])) {
      $tags = explode(',', str_replace(' ', '', $mailgun['campaign-id']));
      if (empty($body['o:tag'])) {
        $body['o:tag'] = $tags;
      } elseif (is_array($body['o:tag'])) {
        $body['o:tag'] = array_merge($body['o:tag'], $tags);
      } else {
        $body['o:tag'] .= ',' . $tags;
      }
    }

    /**
     * Filter tags.
     *
     * @param array  $tags        Mailgun tags.
     * @param string $to          To address.
     * @param string $subject     Subject line.
     * @param string $message     Message content.
     * @param array  $headers     Headers array.
     * @param array  $attachments Attachments array.
     * @param string $region      Mailgun region.
     * @param string $domain      Mailgun domain.
     *
     * @return array              Mailgun tags.
     */
    $body['o:tag'] = apply_filters(
      'mailgun_tags',
      $body['o:tag'],
      $to,
      $subject,
      $message,
      $headers,
      $attachments,
      $region,
      $domain
    );

    if (!empty($cc) && is_array($cc)) {
      $body['cc'] = implode(', ', $cc);
    }

    if (!empty($bcc) && is_array($bcc)) {
      $body['bcc'] = implode(', ', $bcc);
    }

    // If we are not given a Content-Type in the supplied headers,
    // write the message body to a file and try to determine the mimetype
    // using get_mime_content_type.
    if (!isset($content_type)) {
      $tmppath = tempnam(get_temp_dir(), 'mg');
      $tmp = fopen($tmppath, 'w+');

      fwrite($tmp, $message);
      fclose($tmp);

      $content_type = $this->get_mime_content_type($tmppath, 'text/plain');

      unlink($tmppath);
    }

    // Allow external content type filter to function normally
    if (has_filter('wp_mail_content_type')) {
      $content_type = apply_filters('wp_mail_content_type', $content_type);
    }

    if ('text/plain' === $content_type) {
      $body['text'] = $message;
    } elseif ('text/html' === $content_type) {
      $body['html'] = $message;
    } else {
      // Unknown Content-Type??
      error_log('[mailgun] Got unknown Content-Type: ' . $content_type);
      $body['text'] = $message;
      $body['html'] = $message;
    }

    // Some plugins, such as WooCommerce (@see WC_Email::handle_multipart()), to handle multipart/alternative with html
    // and plaintext messages hooks into phpmailer_init action to override AltBody property directly in $phpmailer,
    // so we should allow them to do this, and then get overridden plain text body from $phpmailer.
    // Partly, this logic is taken from original wp_mail function.
    if (false !== stripos($content_type, 'multipart')) {
      global $phpmailer;

      // (Re)create it, if it's gone missing.
      if (!($phpmailer instanceof PHPMailer\PHPMailer\PHPMailer)) {
        require_once ABSPATH . WPINC . '/PHPMailer/PHPMailer.php';
        require_once ABSPATH . WPINC . '/PHPMailer/SMTP.php';
        require_once ABSPATH . WPINC . '/PHPMailer/Exception.php';
        $phpmailer = new PHPMailer\PHPMailer\PHPMailer(true);

        $phpmailer::$validator = static function ($email) {
          return (bool) is_email($email);
        };
      }

      /**
       * Fires after PHPMailer is initialized.
       *
       * @param PHPMailer $phpmailer The PHPMailer instance (passed by reference).
       */
      do_action_ref_array('phpmailer_init', [&$phpmailer]);

      $plainTextMessage = $phpmailer->AltBody;

      if ($plainTextMessage) {
        $body['text'] = $plainTextMessage;
      }
    }

    // If we don't have a charset from the input headers
    if (!isset($charset)) {
      $charset = get_bloginfo('charset');
    }

    // Set the content-type and charset
    $charset = apply_filters('wp_mail_charset', $charset);
    if (isset($headers['Content-Type'])) {
      if (!strstr($headers['Content-Type'], 'charset')) {
        $headers['Content-Type'] =
          rtrim($headers['Content-Type'], '; ') . "; charset={$charset}";
      }
    }

    // Set custom headers
    if (!empty($headers)) {
      foreach ((array) $headers as $name => $content) {
        $body["h:{$name}"] = $content;
      }

      // TODO: Can we handle this?
      //if ( false !== stripos( $content_type, 'multipart' ) && ! empty($boundary) )
      //  $phpmailer->AddCustomHeader( sprintf( "Content-Type: %s;\n\t boundary=\"%s\"", $content_type, $boundary ) );
    }

    /*
      * Deconstruct post array and create POST payload.
      * This entire routine is because wp_remote_post does
      * not support files directly.
      */

    $payload = '';

    // First, generate a boundary for the multipart message.
    $boundary = sha1(uniqid('', true));

    // Allow other plugins to apply body changes before creating the payload.
    $body = apply_filters('mg_mutate_message_body', $body);
    if (($body_payload = $this->mg_build_payload_from_body($body, $boundary)) != null) {
      $payload .= $body_payload;
    }

    // Allow other plugins to apply attachment changes before writing to the payload.
    $attachments = apply_filters('mg_mutate_attachments', $attachments);
    if (($attachment_payload = $this->mg_build_attachments_payload($attachments, $boundary)) != null) {
      $payload .= $attachment_payload;
    }

    $payload .= '--' . $boundary . '--';

    $data = [
      'body' => $payload,
      'headers' => [
        'Authorization' => 'Basic ' . base64_encode("api:{$apiKey}"),
        'Content-Type' => 'multipart/form-data; boundary=' . $boundary,
      ],
    ];

    $endpoint = $this->mg_api_get_region($region);
    $endpoint = $endpoint ? $endpoint : 'https://api.mailgun.net/v3/';
    $url = $endpoint . "{$domain}/messages";

    // TODO: Mailgun only supports 1000 recipients per request, since we are
    // overriding this function, let's add looping here to handle that
    $response = wp_remote_post($url, $data);
    if (is_wp_error($response)) {
      // Store WP error in last error.
      $this->mg_api_last_error($response->get_error_message());

      return false;
    }

    $response_code = wp_remote_retrieve_response_code($response);
    $response_body = json_decode(wp_remote_retrieve_body($response));

    // Mailgun API should *always* return a `message` field, even when
    // $response_code != 200, so a lack of `message` indicates something
    // is broken.
    if ((int) $response_code != 200 || !isset($response_body->message)) {
      // Store response code and HTTP response message in last error.
      $response_message = wp_remote_retrieve_response_message($response);
      $errmsg = "$response_code - $response_message";
      $this->mg_api_last_error($errmsg);

      return false;
    }

    // Not sure there is any additional checking that needs to be done here, but why not?
    if ($response_body->message != 'Queued. Thank you.') {
      $this->mg_api_last_error($response_body->message);

      return false;
    }

    return true;
  }

  //End class
}
