<?php

if (!defined('ABSPATH')) {
  exit();
} // Exit if accessed directly

class SiteConfig extends AdminPages
{
  public $options_name = 'site_config';
  public $base_name = 'site-config-admin';

  public function __construct()
  {
    parent::__construct();
  }

  public function register_admin_page()
  {
    add_menu_page(
      'Site Config',
      'Site Config',
      'manage_options',
      $this->base_name,
      [$this, 'render_config_options_page'],
      'dashicons-admin-generic'
    );
  }

  public function page_init()
  {
    $gtm_section_fields = [
      'gtm_config' => [
        'type' => 'textfield',
        'title' => 'GTM Code',
        'description' => 'GTM-XXXXXXX',
        'sanitize' => true,
      ],
    ];

    $coming_soon_section_fields = [
      'coming_soon_active' => [
        'type' => 'wp-toggle',
        'title' => 'Enable Coming Soon Mode',
        'description' => 'Hides the website and puts up a coming soon landing page. Can override the coming soon page by adding coming-soon template to the project.',
      ],
      'coming_soon_title' => [
        'type' => 'textfield',
        'title' => 'Coming Soon Title',
        'placeholder' => 'Something new is coming'
      ],
      'coming_soon_bg_color' => [
        'type' => 'color',
        'buttonText' => 'Pick a background colour',
        'title' => 'Background Colour',
      ]
    ];

    $gtm_section = array(
      'id' => 'gtm_section_id',
      'title' => 'GTM Code',
      'description' => '',
      'page' => $this->base_name,
      'fields' => $gtm_section_fields
    );

    $coming_soon_section = array(
      'id' => 'coming_soon_section_id',
      'title' => 'Coming Soon',
      'description' => '',
      'page' => $this->base_name,
      'fields' => $coming_soon_section_fields
    );

    $config_sections = [
      $gtm_section,
      $coming_soon_section
    ];

    // Meta fields here.
    $this->register_options_page($this->base_name, $config_sections);
  }


  public function render_config_options_page()
  {
    $this->options = get_option($this->options_name);
    echo '<form method="post" class="fx-admin-page wrap" action="options.php">';
    settings_fields($this->options_group);
    do_settings_sections($this->base_name);
    submit_button();
    echo '</form>';
  }
}

class MailgunConfig extends AdminPages
{
  public $options_name = 'mailgun_config';
  public $base_name = 'site-config-admin';
  public $mailgun_page_name = 'site-mailgun-options';

  public function __construct()
  {
    parent::__construct();
  }

  public function register_admin_page()
  {
    add_submenu_page(
      $this->base_name,
      'Mailgun',
      'Mailgun',
      'manage_options',
      $this->mailgun_page_name,
      [$this, 'render_mailgun_options_page']
    );
  }

  public function page_init()
  {
    $mailgun_section_fields = [
      'apiKey' => [
        'type' => 'textfield',
        'title' => 'Mailgun API Key',
        'description' => '',
        'sanitize' => true,
        'description' => 'Defaults to default API Key',
      ],
      'domain' => [
        'type' => 'textfield',
        'title' => 'Mailgun Domain',
        'description' => '',
        'sanitize' => true,
        'description' => 'Defaults to ' . MAILGUN_DOMAIN . '',

      ],
      'from_email' => [
        'type' => 'textfield',
        'title' => 'Mailgun From Email',
        'description' => 'Overrides sitewide from address',
        'sanitize' => true,
        'description' => 'Defaults to ' . MAILGUN_FROM_EMAIL . '',
      ],
      'from_name' => [
        'type' => 'textfield',
        'title' => 'Mailgun From Name',
        'description' => 'Defaults to ' . MAILGUN_FROM_NAME . '',
        'sanitize' => true,
      ],
    ];

    $mailgun_section = array(
      'id' => 'site_mailgun_section_id',
      'title' => 'Mailgun Settings',
      'description' => '',
      'page' => $this->mailgun_page_name,
      'fields' => $mailgun_section_fields
    );

    $mailgun_sections = [
      $mailgun_section
    ];


    // Meta fields here.
    $this->register_options_page($this->mailgun_page_name, $mailgun_sections);
  }


  // Mailgun options page
  public function render_mailgun_options_page()
  {
    $this->options = get_option($this->options_name);
    echo '<form method="post" class="fx-admin-page wrap" action="options.php">';
    settings_fields($this->options_group);
    do_settings_sections($this->mailgun_page_name);
    echo '<style>.fx-admin-page p.submit{margin: 0 1rem 0 0; padding: 0;}</style><div style="display:flex; margin-top: 2rem;">';
    submit_button();
    echo '<button type="button" class="button" id="fx-test-mail-configuration">Test Configuration</button>';
    echo '</div>';
    echo '<p id="fx-mail-configuration-message"></p>';
    echo '</form>';
?>
    <script>
      const testBtn = document.getElementById('fx-test-mail-configuration');
      if (testBtn) {
        const testMessage = document.getElementById('fx-mail-configuration-message');
        testBtn.addEventListener('click', () => {
          testMessage.innerText = '';
          const formData = new FormData();
          formData.append('token', window.CONSTANTS.API_NONCE);
          fetch(`${window.CONSTANTS.API_URL}/mailgun/test`, {
              method: 'POST',
              mode: 'same-origin',
              credentials: 'same-origin',
              body: formData
            })
            .then(response => response.json()) //or text()?
            .then(data => {
              testMessage.innerText = data.message;
              data.success ? testMessage.style.color = 'green' : testMessage.style.color = 'red';
            })
            .catch(error => {
              testMessage.innerText = error;
              testMessage.style.color = 'red';
            });
        });
      }
    </script>
<?php
  }
}


// Init the new class only for admin
if (is_admin()) {
  $my_settings_page = new SiteConfig();
  $my_settings_page = new MailgunConfig();
}
