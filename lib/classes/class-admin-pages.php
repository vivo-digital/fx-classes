<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

abstract class AdminPages
{

    use MetaBoxes;
    use MetaHelpers;

    protected $options;
    protected $fields = [];

    protected $options_name;

    public function __construct()
    {
        add_action('admin_menu', [$this, 'register_admin_page']);
        add_action('admin_init', [$this, 'page_init']);

        $this->options_page = $this->options_name;
        $this->options_group = $this->options_name . '_group';
    }

    abstract public function register_admin_page();

    abstract public function page_init();

    public function register_options_page($page, $sections)
    {
        register_setting(
            $this->options_group,
            $this->options_name,
            [$this, 'sanitize_input']
        );

        $this->register_field_sections($page, $sections);
    }

    public function register_field_sections($page, $sections)
    {
        foreach ($sections as $section) {
            if (!$section) {
                continue;
            }

            $section_id = $section['id'];
            $title = $section['title'];
            $section_fields = $section['fields'] ?: [];

            add_settings_section(
                $section_id,
                $title,
                [$this, 'render_section_info'],
                $page
            );

            $this->fields = array_merge($this->fields, $section_fields);

            foreach ($section_fields as $field_id => $field) {

                $args = array_merge($field, [
                    'id' => $field_id,
                    'hide_title' => true,
                ]);

                $callback = [$this, 'render_input_fields'];

                add_settings_field(
                    $field_id,
                    $field['title'],
                    $callback,
                    $page,
                    $section_id,
                    $args
                );
            }
        }
    }

    public function sanitize_input($input)
    {
        $new_input = [];
        $fields = $this->fields;

        foreach ($fields as $id => $field) {
            $sanitize = $field['sanitize'];

            if (!isset($input[$id])) {
                continue;
            }

            if ($sanitize) {
                $new_input[$id] = sanitize_text_field($input[$id]);
            } else {
                $new_input[$id] = $input[$id];
            }
        }

        return $new_input;
    }

    public function render_section_info($info)
    {
        echo '';
    }

    public function render_input_fields($args)
    {
        $id = $args['id'];
        $input_id = $this->options_name . $id;
        $name = $this->options_name . "[$id]";
        $ref = $name;
        $value = $this->options[$id];

        $input_args = $this->get_input_args($args, [
            'id' => $input_id,
            'name' => $name,
            'ref' => $ref,
            'value' => $value,
        ]);

        MetaBoxes::render_input($input_args);
    }
}
