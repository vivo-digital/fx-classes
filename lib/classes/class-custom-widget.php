<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

abstract class CustomWidget extends WP_Widget {
    use MetaBoxes;
    use MetaHelpers;

    static $widget_id = '';
    static $options = [];
    static $fields = [];

    function __construct() {
        if (!$this::$widget_id || empty($this::$options) || empty($this::$fields)) {
            return;
        }

        $this->domain = $this::$widget_id . '_domain';

        $title = $this::$options['title'];
        $description = $this::$options['description'];

        WP_Widget::__construct(
            $this::$widget_id,
            __($title, $this->domain),
            [
                'description' => __($description, $this->domain),
                'classname' => 'widget--' . $this::$widget_id
            ]
        );
    }


    // You must create this function to render the widget
    // public function widget($args, $instance);

    public function form($instance) {
        if ($this::$fields) :
            foreach ($this::$fields as $slug => $options) {
                $this->render_input_field($instance, $slug, $options);
            }
        endif;
    }

    private function render_input_field($instance, $slug, $args) {
        // Enqueue media lightbox
        wp_enqueue_media();

        $input_id = $this->get_field_id($slug);
        $name = $this->get_field_name($slug);
        $ref = $name;
        $value = isset($instance[$slug]) ? $instance[$slug] : null;

        if (isset($args['default']) && !$value) {
            $value = $args['default'];
        }

        $input_args = $this->get_input_args($args, [
            'id' => $input_id,
            'name' => $name,
            'ref' => $ref,
            'value' => $value,
        ]);

        MetaBoxes::render_input($input_args);
    }

    public function update($new_instance, $old_instance) {
        $instance = [];
        foreach ($this::$fields as $slug => $options) {
            $instance[$slug] = (!empty($new_instance[$slug])) ? strip_tags($new_instance[$slug]) : '';

            // Convert checkbox to boolean
            if ($options['type'] === 'checkbox') {
                $instance[$slug] = $instance[$slug] === 'true' || $instance[$slug] === 'on';
            }

        }
        return $instance;
    }

}
