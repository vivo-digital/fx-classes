<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class ProcessForm {
    static $encode_response = false;
    static $form_name = '';
    static $fields = [];
    static $form_data = [];

    public $data = [
        'success' => false,
        'errors' => [],
    ];
    public $errors = [];
    public $options = [
        'to' => null,
        'from' => null,
        'subject' => '',
    ];

    function __construct($form_name, $fields) {
        date_default_timezone_set('Australia/Sydney');

        $this::$form_name = $form_name;
        $this::$fields = $fields;
    }

    public function set_options($options) {
        $this->options = array_merge($this->options, $options);
    }

    public function submit($form_data) {
        $this::$form_data = $this->get_post_data($form_data);

        if ($this->validate()) {
            $this->handle_submit();
        } else {
            $this->set_message('Form is missing required information.');
        }
    }

    private function handle_submit() {
        $body = $this->get_message_body();
        $body = $this->htmlize($body);

        $to = $this->options['to'];
        $from = $this->options['from'];
        $subject = $this->options['subject'];

        $headers = $from ? [
            'From: ' . $from,
            'Reply-To: ' . $from,
        ] : [];

        // Send mail
        if ($to) {
            if ($this->send_mail($to, $body, $subject, $headers)) {
                $this->data['success'] = true;
            }
        } else {
            $this->data['success'] = true;
        }

        $this->on_submit($this->data['success']);
    }

    protected function on_submit($success) {
        return;
    }

    public function response() {
        return $this::$encode_response ? json_encode($this->data) : $this->data;
    }

    private function check_session() {
        // TODO: Should probably do something here
        return true;
    }

    private function get_message_body() {
        $message = 'Submitted: ' . date('m/d/Y h:i:s a') . '<br /><br />';

        foreach ($this::$form_data as $field_name => $field_value) {
            // Build message
            $message .= '<b>' . ucfirst($field_name) . ':</b><br />';
            $message .= $field_value;
            $message .= '<br /><br />';
        }

        return $message;
    }

    private function htmlize($body) {
        $header = '<!doctype html>
        <html xmlns="http://www.w3.org/1999/xhtml"><head>
            <title>' . esc_html($this->options['subject']) . '</title>
        </head>
        <body>';
        $footer = '</body></html>';

        $html = $header . $body . $footer;

        return $html;
    }

    protected function get_post_data($form_data) {
        $fields = $this::$fields;
        $post = [];
        // Only collect inputs defined
        foreach ($fields as $field) {
            $field_name = $field[0];
            $field_type = $field[1];
            $value = $this->sanitize_input($form_data[$field_name]);

            // convert data to correct format
            switch ($field_type) {
                case 'bool':
                    // based on checkbox value
                    $value = $value !== '';
                break;
                case 'number':
                    $value = str_replace(' ', '', $value);
                    $value = (float)$value;
                default:
                break;
            }

            $post[$field_name] = $value;
        }
        return $post;
    }

    protected function set_message($message = '') {
        $this->data['message'] = $message;
    }

    protected function set_error($error) {
        if ($error && !array_key_exists($error, $this->data['errors'])) {
            $this->data['errors'][] = $error;
        }
    }

    private function sanitize_input($input) {
        $data = strip_tags($input);
        $data = trim($data);
        $data = stripslashes($data);
        return htmlspecialchars($data);
    }

    protected function validate() {
        foreach ($this::$fields as $field) {

            $field_name = $field[0];
            $field_type = $field[1];
            $required = $field[2];

            $value = $this::$form_data[$field_name];

            if (!$required) {
                continue;
            }

            switch ($field_type) {
                case 'email':
                    if (!filter_var($value , FILTER_VALIDATE_EMAIL)) {
                        $this->set_error($field_name);
                    }
                    break;
                case 'bool':
                    if (!filter_var($value, FILTER_VALIDATE_BOOLEAN)) {
                        $this->set_error($field_name);
                    }
                    break;
                case 'number':
                    if (!is_numeric($value)) {
                        $this->set_error($field_name);
                    }
                    break;
                default:
                    if (empty($value)) {
                        $this->set_error($field_name);
                    }
                    break;
            }
        }

        return empty($this->data['errors']);
    }

    protected function send_mail($to, $body, $subject, $custom_headers = []) {
        $headers = array_merge($custom_headers, [
            'Content-Type: text/html; charset=UTF-8'
        ]);

        return wp_mail($to, $subject, $body, $headers);
    }
}
