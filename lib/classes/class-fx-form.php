<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

add_action('phpmailer_init', 'fx_phpmailer_init', 10, 1);

function fx_phpmailer_init($phpmailer)
{
    $custom_headers = $phpmailer->getCustomHeaders();
    $phpmailer->clearCustomHeaders();
    $fx_content_type = false;

    foreach ((array) $custom_headers as $custom_header) {
        $name = $custom_header[0];
        $value = $custom_header[1];

        if ('X-FX-Content-Type' === $name) {
            $fx_content_type = trim($value);
        } else {
            $phpmailer->addCustomHeader($name, $value);
        }
    }

    if ('text/html' === $fx_content_type) {
        $phpmailer->msgHTML($phpmailer->Body);
    } elseif ('text/plain' === $fx_content_type) {
        $phpmailer->AltBody = '';
    }
}

class FxForm
{
    public $name = '';
    public $data = null;
    public $response = [
        'success' => false,
        'errors' => null,
    ];
    public $fields = [];
    public $options = [
        'to' => [],
        'from' => null,
        'subject' => '',
        'headers' => [],
    ];

    function __construct($name, $data)
    {
        date_default_timezone_set('Australia/Sydney');

        $this->name = $name;
        $this->fields = $this->apply_form_filter('fields', $this->fields, $this);
        $this->data = $this->sanitize_data($data);

        $options = $this->apply_form_filter('options', $this->options, $this);

        if ($options) {
            $this->set_options($options);
        }
    }

    public function set_options($options)
    {
        $this->options = array_merge($this->options, $options);
    }

    public function submit()
    {
        $body = $this->generate_body();
        $body = $this->htmlize($body);

        $to = $this->options['to'];
        $from = $this->options['from'];
        $subject = $this->options['subject'];

        // Filter out dudd emails
        $to = array_filter($to, function ($email) {
            return filter_var($email, FILTER_VALIDATE_EMAIL);
        });

        $headers = $from ? [
            'From: ' . $from,
            'Reply-To: ' . $from,
        ] : [];

        $headers = array_merge($headers, $this->options['headers']);

        // Send mail
        if ($to && count($to) > 0) {
            if ($this->send_mail($to, $body, $subject, $headers)) {
                $this->response['success'] = true;
            }
        } else {
            $this->response['success'] = true;
        }

        $this->do_form_action('submit', $this);
    }

    public function get_response()
    {
        return $this->response;
    }

    public function validate()
    {
        foreach ($this->fields as $field) {

            $field_name = $field[0];
            $field_type = $field[1];
            $required = $field[2];

            $value = $this->data[$field_name];

            if (!$required) {
                continue;
            }

            switch ($field_type) {
                case 'email':
                    if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                        $this->set_error($field_name);
                    }
                    break;
                case 'bool':
                    if (!filter_var($value, FILTER_VALIDATE_BOOLEAN)) {
                        $this->set_error($field_name);
                    }
                    break;
                case 'number':
                    if (!is_numeric($value)) {
                        $this->set_error($field_name);
                    }
                    break;
                default:
                    if (empty($value)) {
                        $this->set_error($field_name);
                    }
                    break;
            }
        }

        return empty($this->response['errors']);
    }

    private function do_form_action($action, ...$args)
    {
        return do_action('fx_form_' . $action . '_' . $this->name, ...$args);
    }

    private function apply_form_filter($filter, ...$args)
    {
        return apply_filters('fx_form_' . $filter . '_' . $this->name, ...$args);
    }

    private function generate_body()
    {
        $message = 'Submitted: ' . date('m/d/Y h:i:s a') . '<br /><br />';

        foreach ($this->data as $field_name => $field_value) {
            // Build message
            $message .= '<b>' . ucfirst($field_name) . ':</b><br />';
            $message .= $field_value;
            $message .= '<br /><br />';
        }

        $message = $this->apply_form_filter('email_body', $message, $this);

        return $message;
    }

    private function htmlize($body)
    {
        $header = '<!doctype html>
        <html xmlns="http://www.w3.org/1999/xhtml"><head>
            <title>' . esc_html($this->options['subject']) . '</title>
        </head>
        <body>';
        $footer = '</body></html>';

        $html = $header . $body . $footer;

        return $html;
    }

    private function sanitize_data($data)
    {
        $post = [];
        // Only collect inputs defined
        foreach ($this->fields as $field) {
            $field_name = $field[0];
            $field_type = $field[1];
            $value = $this->sanitize_input($data[$field_name]);

            // convert data to correct format
            switch ($field_type) {
                case 'bool':
                    // based on checkbox value
                    $value = $value !== '';
                    break;
                case 'number':
                    $value = str_replace(' ', '', $value);
                    $value = (float)$value;
                default:
                    break;
            }

            $post[$field_name] = $value;
        }
        return $post;
    }

    private function set_error($error)
    {
        if (!$this->response['errors']) {
            $this->response['errors'] = [];
        }

        if ($error && !array_key_exists($error, $this->response['errors'])) {
            $this->response['errors'][] = $error;
        }
    }

    private function sanitize_input($input)
    {
        $data = strip_tags($input);
        $data = trim($data);
        $data = stripslashes($data);
        return htmlspecialchars($data);
    }

    private function send_mail($to, $body, $subject, $custom_headers = [])
    {
        $headers = array_merge($custom_headers, [
            'Content-Type: text/html',
            'X-FX-Content-Type: text/html',
        ]);

        $headers = join("\n", $headers);

        return wp_mail($to, $subject, $body, $headers);
    }
}
