<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class CPT
{
    public function __construct($slug, $options)
    {
        if (!$slug || !is_array($options)) {
            return;
        }

        $this->slug = $slug;
        $this->options = $options;
        $this->custom_columns = [];

        $this->add_actions();
    }

    private function add_actions()
    {
        // Register the post type
        add_action('init', [$this, 'register']);

        // Enqueue the icon styles
        add_action('admin_head', [$this, 'admin_icon_styles']);

        // Add admin edit columns
        add_filter('manage_edit-' . $this->slug . '_columns', [$this, 'add_admin_columns']);

        // Populate admin edit columns
        add_action('manage_' . $this->slug . '_posts_custom_column', [$this, 'populate_admin_columns'], 10, 2);

        // Run filter to make columns sortable
        add_filter('manage_edit-' . $this->slug . '_sortable_columns', [$this, 'sortable_admin_columns']);

        // Run action that sorts columns on request
        add_action('pre_get_posts', [$this, 'sort_admin_columns']);
    }

    public function add_meta_section($id, $options, $meta_boxes)
    {
        $meta = new CustomPostMeta($id, $options, $meta_boxes, $this->slug);
    }

    public function add_column($id, $title, $callback, $sort = null)
    {
        $this->custom_columns[$id] = [
            'title' => $title,
            'callback' => $callback,
            'sort' => $sort,
        ];
    }

    public function add_admin_columns($defaults)
    {
        $new_columns = [];
        foreach ($defaults as $key => $value) {
            if ($key === 'date') {
                // Add all custom columns
                foreach ($this->custom_columns as $id => $column) {
                    $new_columns[$id] = $column['title'];
                }
            }
            $new_columns[$key] = $value;
        }
        return $new_columns;
    }

    public function sortable_admin_columns($columns)
    {
        foreach ($this->custom_columns as $column_id => $column) {
            if (isset($column['sort']) && $column['sort']) {
                $columns[$column_id] = $column_id;
            }
        }

        return $columns;
    }

    public function populate_admin_columns($column_id, $post_id)
    {
        // Get wordpress $post object
        global $post;

        // First check if it's a custom column
        if (isset($this->custom_columns[$column_id])) {
            $column = $this->custom_columns[$column_id];

            // Make sure callback actually is a function
            if (isset($column['callback']) && is_callable($column['callback'])) {
                // Run the function.
                call_user_func_array($column['callback'], [$post]);
            }
        }
    }

    public function sort_admin_columns($query)
    {
        if (!is_admin()) {
            return;
        }

        $orderby = $query->get('orderby');

        foreach ($this->custom_columns as $column_id => $column) {
            // Check if we are ordering by this column
            if ($orderby !== $column_id) {
                continue;
            }

            if (isset($column['sort']) && is_callable($column['sort'])) {
                call_user_func_array($column['sort'], [$query]);
            }
        }
    }

    public function register()
    {
        $singular = $this->options['singular'];
        $plural = $this->options['plural'];
        $rewrite = isset($this->options['rewrite']) ? $this->options['rewrite'] : $plural;
        $breadcrumb = isset($this->options['breadcrumb']) ? $this->options['breadcrumb'] : $plural;

        $labels = [
            'name'               => sprintf(__('%s', 'fx'), $plural),
            'breadcrumb'         => sprintf(__('%s', 'fx'), $breadcrumb),
            'singular_name'      => sprintf(__('%s', 'fx'), $singular),
            'menu_name'          => sprintf(__('%s', 'fx'), $plural),
            'all_items'          => sprintf(__('%s', 'fx'), $plural),
            'add_new'            => __('Add New', 'fx'),
            'add_new_item'       => sprintf(__('Add New %s', 'fx'), $singular),
            'edit_item'          => sprintf(__('Edit %s', 'fx'), $singular),
            'new_item'           => sprintf(__('New %s', 'fx'), $singular),
            'view_item'          => sprintf(__('View %s', 'fx'), $singular),
            'search_items'       => sprintf(__('Search %s', 'fx'), $plural),
            'not_found'          => sprintf(__('No %s found', 'fx'), strtolower($plural)),
            'not_found_in_trash' => sprintf(__('No %s found in Trash', 'fx'), strtolower($plural)),
            'parent_item_colon'  => sprintf(__('Parent %s:', 'fx'), $singular)
        ];

        // Default options.
        $defaults = [
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'menu_icon' => '',
            'query_var' => true,
            'show_in_rest' => true,
            'exclude_from_search' => false,
            'rewrite' => [
                'slug' => sanitize_title($rewrite),
                'hierarchical' => true,
                'with_front' => false
            ],
            'capability_type' => 'post',
            'hierarchical' => true,
            'taxonomies' => [],
            'supports' => ['title', 'editor', 'page-attributes', 'thumbnail']
        ];

        $options = array_merge($defaults, $this->options);

        if (!post_type_exists($this->slug)) {
            register_post_type($this->slug, $options);
            flush_rewrite_rules(false);
        }
    }

    private function post_icon()
    {
        $slug = $this->slug;
        $icon = $this->options['icon'];

        if (empty($slug) || !is_string($slug)) {
            return;
        }

        $icon = !empty($icon) ? $icon : 'f533';

        return '#adminmenu .menu-icon-' . $slug . ' div.wp-menu-image:before { content: "\\' . $icon . '"; }';
    }

    public function admin_icon_styles()
    {
        echo '<style>' . $this->post_icon() . '</style>';
    }
}
