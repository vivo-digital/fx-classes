<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

trait MetaBoxMediaSVG {

    public static function render_media_svg($args) {
        $button_text = $args['button_text'] ? $args['button_text'] : 'Select SVG';
        $id = $args['id'];
        $name = $args['name'];
        $placeholder = $args['placeholder'] ? $args['placeholder'] : 'Select an SVG or place code directly';
        $ref = $args['ref'];
        $value = $args['value'];
        $value_escaped = $args['value_escaped'];

        echo '<div class="select-media-svg edit-active" id="select-media-svg_' . $id . '">';
        echo '<div class="select-media-svg__preview"></div>';
        echo '
            <textarea data-meta-value placeholder="' . $placeholder . '" name="' . $ref .'" id="select-media-svg__input-' . $id . '" class="select-media-svg_input">'
                . htmlspecialchars($value) .
            '</textarea>';

        echo '
            <label class="button button-primary select-media-svg__upload">
                <span>' . $button_text . '<span>
                <input type="file" name="select-media-svg_file"/>
            </label>';
        echo '<button type="button" class="button select-media-svg__change">View Code</button>';
    ?>
        <script type="text/javascript">
            jQuery(function($) {
                var metaBox = $('#select-media-svg_<?php echo $id; ?>');
                var svgTextArea = metaBox.find('textarea');
                var svgPreview = metaBox.find('.select-media-svg__preview');
                var svgChangeButton = metaBox.find('.select-media-svg__change');
                var svgUploadInput = metaBox.find('.select-media-svg__upload input');

                function updatePreview() {
                    if (svgTextArea.val()) {
                        metaBox.addClass('has-value');
                        metaBox.removeClass('edit-active');
                        svgChangeButton.text('View Code');
                    }

                    svgPreview.html(svgTextArea.val());
                }

                function toggleEdit() {
                    metaBox.toggleClass('edit-active');
                    svgChangeButton.text(metaBox.hasClass('edit-active') ? 'Close' : 'View Code');
                }

                function handleUpload() {
                    if (svgUploadInput[0].files.length) {
                        var f = svgUploadInput[0].files[0];
                        var reader = new FileReader();

                        reader.onload = function(e) {
                            svgTextArea[0].value = e.target.result;
                            updatePreview();
                        };

                        reader.readAsText(f);
                    };
                }

                svgUploadInput.on('change', function() {
                    handleUpload();
                })

                svgTextArea.on('blur', function() {
                    if (!$(this).val()) {
                        metaBox.removeClass('has-value');
                    }

                    updatePreview();
                });

                svgChangeButton.on('click', function() {
                    toggleEdit();
                });

                updatePreview();
            });
        </script>
        <?php

        echo '</div>';
    }
}
