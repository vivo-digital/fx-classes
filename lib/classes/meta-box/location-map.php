<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

trait MetaBoxLocationMap {

    public static function render_location_map($args) {
        $google_map_api_key = GOOGLE_API_KEY;

        $id = $args['id'];
        $name = $args['name'];
        $ref = $args['ref'];
        $value = $args['value'];
        $value_escaped = $args['value_escaped'];

        $values = json_decode($value);
        if (is_object($values)) {
            $lat = $values->lat;
            $lng = $values->lng;
        }

        $map_id = 'location_map_input__' . $id;

        echo '<div id="location_map_input_' . $id . '" class="location_map_input">';
        echo '
            <p>
                <div class="location_map_input__map-container">
                    <div id="' . $map_id . '" class="location_map_input__map"></div>
                </div>
                <br />
                <sub>
                    Latitude: <span class="location_lat">' . $lat . '</span>
                    Longitude: <span  class="location_lng">' . $lng . '</span>
                </sub>
            </p>';

        echo '<input data-meta-value type="hidden" class="location_result" name="' . $ref . '" id="' . $id . '" value="' . $value_escaped . '" />';

        echo '</div>';

        ?>
        <script>

        var submitting = false;
        var locationBox = document.getElementById('location_map_input_<?php echo $id; ?>');
        var locationLat = locationBox.querySelector('.location_lat');
        var locationLng = locationBox.querySelector('.location_lng');
        var locationResult = locationBox.querySelector('.location_result');
        var currentValue = locationResult.value && JSON.parse(locationResult.value);

        // Setup map & marker as global
        // May cause problems if multiple maps
        var map;
        var location_map_input__marker;

        function addMarker(latLng) {
            location_map_input__marker = new google.maps.Marker({
                position: latLng,
                map: map,
                animation: google.maps.Animation.DROP,
            });
        }

        function locationData(value) {
            if (!value) {
                return;
            }

            locationLat.innerText = parseFloat(value.lat).toFixed(5);
            locationLng.innerText = parseFloat(value.lng).toFixed(5);
        }

        // Add current data;
        locationData(currentValue);

        function handleMapClick(event) {
            // Remove old marker
            if (location_map_input__marker) {
                location_map_input__marker.setMap(null);
            }

            // Add new marker
            addMarker(event.latLng);

            // Save data
            var data = {
                lat: event.latLng.lat(),
                lng: event.latLng.lng(),
            };

            // Update input
            locationResult.value = JSON.stringify(data);
            // Re-render UI
            locationData(data);
        }

        function loadMap() {
            var centerLat = currentValue ? currentValue.lat : -34.4278;
            var centerLng = currentValue ? currentValue.lng : 150.8931;

            map = new google.maps.Map(document.getElementById('<?php echo $map_id; ?>'), {
                center: new google.maps.LatLng(centerLat, centerLng),
                zoom: 14,
            });

            google.maps.event.addListener(map, 'click', handleMapClick);

            // Add current marker
            addMarker(new google.maps.LatLng(currentValue.lat, currentValue.lng))
        }
        </script>
        <script src="//maps.googleapis.com/maps/api/js?v=3&amp;sensor=false&amp;key=<?php echo $google_map_api_key; ?>&amp;callback=loadMap" defer></script>
        <?php
    }
}
