<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

trait MetaBoxes {
    public static function render_input($args) {
        $name = $args['name'];
        $type = $args['type'];

        printf(
            '<input type="hidden" name="%1$s_noncename" value="%2$s" />',
            $name,
            wp_create_nonce()
        );

        printf(
            '<fx-meta-%1$s args="%2$s"></fx-meta-%1$s>',
            $type,
            fx_stringify_object($args)
        );
    }
}
