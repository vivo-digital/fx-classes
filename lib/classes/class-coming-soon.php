<?php

if (!defined('ABSPATH')) {
  exit;
} // Exit if accessed directly

class FXComingSoon
{
  function __construct()
  {
    $comingSoonActive = get_config('coming_soon_active');
    if ($comingSoonActive) {
      add_action('get_header', [$this, 'wp_maintenance_mode']);
    }
  }

  function wp_maintenance_mode()
  {
    $bgColor = empty(get_config('coming_soon_bg_color')) ? 'black' : get_config('coming_soon_bg_color');
    $title = empty(get_config('coming_soon_title')) ? 'Something new is coming' : get_config('coming_soon_title');
    if (!current_user_can('edit_themes') || !is_user_logged_in()) {
      wp_head();
      get_template_part('meta');
      // check project for template override, else default content
      $comingSoonProjectTemplate = get_template_part('coming-soon');
      if (false === $comingSoonProjectTemplate) {
        echo '
        <style>
          .coming-soon {
              background: ' . $bgColor . ';
              height: 100vh;
              width: 100vw;
              display: flex;
              justify-content: center;
              align-items: center;
              color: white;
              text-align: center;
              position: relative;
          }
          .coming-soon-logo {
              max-width: 450px;
              margin-bottom: 4rem;
          }
          @media(max-width: 992px) {
            .coming-soon-logo {
              max-width: 300px;
              margin-bottom: 3rem;
            }
          }
          .coming-soon-title {
            font-size: 3rem;
          }
          @media(max-width: 992px) {
            .coming-soon-title {
              font-size: 1.75rem;
            }
          }
          .coming-soon-vivo-logo {
            position: absolute;
            width: 95px;
            left: 0;
            right: 0;
            bottom: 4rem;
            margin-left: auto;
            margin-right: auto;
          }
        </style>
        <div class="coming-soon">
            <div class="container">
                <img class="coming-soon-logo" src=" ' . theme_dir('/assets/img/logo.svg', false) . '" alt="Logo" />
                <h1 class="coming-soon-title">' . $title . '</h1>
                <a class="coming-soon-vivo-logo" href="https://vivo.digital" title="ViVO Digital | Web Design Wollongong" target="_blank">
                  <svg viewBox="0 0 45.7 30">
                    <g>
                        <polygon points="0,7.4 6.6,13.9 3.3,4.1" />
                        <polygon points="3.8,3.5 7,13.1 7,0.4" />
                        <polygon points="22.2,14.9 9.5,14.9 19,11.7" />
                        <polygon points="18.5,11.1 8.6,14.4 11.5,11.5 11.5,11.5 15.2,7.9" />
                        <polygon points="14.7,7.4 11.4,10.6 8.1,0.8" />
                        <polygon points="7.7,1.7 10.9,11.2 7.7,14.4" />
                        <polygon points="8.2,15.6 20.9,15.6 11.4,18.7" />
                        <polygon points="11.9,19.3 21.7,16 15.2,22.6" />
                        <polygon points="15.7,23 22.2,29.6 19,19.8" />
                        <polygon points="22.7,28.7 19.5,19.2 22.7,16" />
                        <polygon points="26.8,11.9 30.1,21.8 25.6,17.3 23.5,15.2" />
                        <polygon points="23.4,30 26.6,26.9 23.4,17.3" />
                        <polygon points="27.1,26.3 23.8,16.5 28.3,21 30.4,23" />
                        <polygon points="30.5,20.9 27.4,11.4 30.5,8.2" />
                        <polygon points="33,7 45.7,7 42.6,3.8" />
                        <polygon points="38.7,0 32.2,6.5 42,3.3 38.7,0" />
                        <polygon points="44.4,7.7 34.9,10.9 31.7,7.7" />
                        <polygon points="31.2,22.2 31.2,9.5 34.4,19" />
                        <polygon points="35,18.5 31.7,8.7 34.6,11.6 34.6,11.6 38.2,15.2" />
                        <polygon points="35.4,11.4 38.7,14.7 45.3,8.1" />
                    </g>
                  </svg>
                </a>
            </div>
        </div>
      ';
      }
      wp_footer();
      die();
    }
  }
}
