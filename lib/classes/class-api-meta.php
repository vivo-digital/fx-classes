<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class APIMeta {
    use MetaHelpers;

    static $namespace = FX_ADMIN_API_BASE . FX_ADMIN_API_VERSION;

    public function __construct() {
        $this->add_actions();
    }

    private function add_actions() {
        add_action('rest_api_init', [$this, 'register_route_hooks']);
    }

    public function register_route_hooks() {
        // Get options
        register_rest_route($this::$namespace, '/options', [
            'methods'  => 'POST',
            'callback' => [$this, 'get_options'],
        ]);
    }

    public function get_options($request) {
        // $data = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $data = $request->get_params();
        $options = $data['options'];
        $options_args = $data['options_args'];
        $post_id = $data['post_id'];

        return $this->get_meta_options($options, $options_args, $post_id);
    }
}
