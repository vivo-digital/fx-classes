<?php

function fx_clean_stringified_value($string)
{
  return fx_stringify_object(json_decode($string));
}

function fx_stringify_object($object)
{
  return htmlspecialchars(json_encode($object), ENT_QUOTES, 'UTF-8');
}

function fx_get_argument_value($value, $default)
{
  return isset($value) ? $value : $default;
}

function fx_migrate_select_meta($posts)
{
  $posts = is_string($posts) ? json_decode($posts, true) : $posts;
  return array_map(function ($p) {
    if (is_array($p)) {
      return $p['id'];
    }
    if (is_object($p)) {
      return $p->id;
    }
    return intval($p);
  }, $posts);
}

function fx_get_current_post_id()
{
  global $post;

  if ($post) {
    return $post->ID;
  }

  if (isset($_GET['post'])) :
    $post_id = $_GET['post'];
  elseif (isset($_POST['post_ID'])) :
    $post_id = $_POST['post_ID'];
  else :
    $post_id = get_the_ID();
  endif;

  return $post_id;
}

function fx_is_using_page_template($template = null)
{
  if (!$template) {
    return false;
  }

  $post_id = fx_get_current_post_id();

  if (!$post_id) {
    return false;
  }

  $page_template = str_replace('.php', '', get_post_meta($post_id, '_wp_page_template', true));

  if (is_array($template)) {
    return in_array($page_template, $template);
  }

  return $template === $page_template;
}

/**
 * Get Config Options
 * @return array Returns all theme and site config options
 * This is interchangeable with the old `get_opts`
 */

 function get_config($name)
 {
   $config_options = get_option('site_config');
   $theme_options = get_option('theme_options');
   $options = [];
 
   if ($config_options && $theme_options) {
     $options = array_merge($theme_options, $config_options);
   } else if ($config_options) {
    $options = $config_options;
   } else if ($theme_options) {
    $options = $theme_options;
   }
 
   $result = isset($options[$name]) ? $options[$name] : null;
 
   if (!$result) {
     return null;
   }
 
   return $result;
 }

function get_mailgun_config($name)
{
  $mailgun_config = get_option('mailgun_config');

  $result = isset($mailgun_config[$name]) ? $mailgun_config[$name] : null;

  if (!$result) {
    return null;
  }

  return $result;
}
