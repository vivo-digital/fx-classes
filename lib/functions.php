<?php
/*
 *  Version: 1.7.10
 *  Author: Mitch Heddles
 *  URL: http://mitchheddles.com
 */

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

require_once 'constants.php';
require_once 'helpers.php';
require_once 'filters.php';

/*------------------------------------*\
	External Traits/Classes
\*------------------------------------*/

// Load helpers and meta box traits
require_once 'classes/trait-meta-helpers.php';
require_once 'classes/trait-meta-boxes.php';

// Now load classes
require_once 'classes/class-admin-pages.php';
require_once 'classes/class-cpt-meta.php';
require_once 'classes/class-cpt.php';
require_once 'classes/class-custom-widget.php';
require_once 'classes/class-process-form.php';
require_once 'classes/class-fx-form.php';
require_once 'classes/class-site-config.php';

// Setup API
require_once 'classes/class-api-meta.php';
$fx_APIMETA = new APIMeta();

// Sections
require_once 'sections/index.php';

// Setup Mailgun
require_once 'classes/class-mailgun.php';
$fx_mailgun = new FXMailgun();

// Coming Soon
require_once 'classes/class-coming-soon.php';
$fx_coming_soon = new FXComingSoon();



/*------------------------------------*\
   Enqueue Scripts & Styles
\*------------------------------------*/

function fx_admin_enqueue_scripts_styles()
{
    $vendor_path = get_template_directory_uri() . '/inc/fx-classes';
    // Admin styles
    wp_enqueue_style('fx-admin-styles', $vendor_path . '/main.css', [], '1');

    // Admin scripts
    wp_register_script('fx-admin-scripts', $vendor_path . '/main.js', ['jquery'], '1');
    wp_enqueue_script('fx-admin-scripts');

    // Vendor
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_style('jquery-style', '//ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');

    wp_localize_script('fx-admin-scripts', 'FX_CONSTANTS', [
        'FX_API_URL' => get_site_url() . '/wp-json/' . FX_ADMIN_API_BASE . FX_ADMIN_API_VERSION,
        'FX_CLASSES_PREFIX' => FX_CLASSES_PREFIX,
        'FX_POST_ID' => get_the_ID(),
        'GOOGLE_API_KEY' => GOOGLE_API_KEY,
    ]);
}

add_action('admin_enqueue_scripts', 'fx_admin_enqueue_scripts_styles');

// Mailgun
// require_once 'mailgun/index.php';
