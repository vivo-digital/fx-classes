import { props, withComponent } from 'skatejs';
import withPreact from '@skatejs/renderer-preact';
import { h } from 'preact';
import kebabCase from 'lodash.kebabcase';

import '@webcomponents/custom-elements';
import './nativeShim';

import { TunnelPlaceholder, TunnelProvider } from './components/Tunnel';
import MetaField from './components/MetaField';
import { metaComponents } from './components/Meta';
import { getDisplayName, transformArgsToProps } from './utils/helpers';
import { TUNNELS } from './constants';
import './overrides';
import './version';

function initializeMetaComponents() {
  metaComponents.forEach(WrappedComponent => {
    class MetaComponent extends withComponent(withPreact()) {
      static props = {
        args: props.string,
      };

      get renderRoot() {
        return this;
      }

      render() {
        const componentProps = transformArgsToProps(this.props.args, ['value']);
        return (
          <TunnelProvider>
            <MetaField {...componentProps}>
              <WrappedComponent {...componentProps} />
              <TunnelPlaceholder id={TUNNELS.MODAL} />
            </MetaField>
          </TunnelProvider>
        );
      }
    }

    const tag = kebabCase(getDisplayName(WrappedComponent));

    customElements.define(`fx-meta-${tag}`, MetaComponent);
  });
}

document.addEventListener('DOMContentLoaded', () => initializeMetaComponents(), false);
