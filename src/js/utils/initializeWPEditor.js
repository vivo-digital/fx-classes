/**
 * The initialize method is taken from wp-admin/js/editor.js, but doesn't add the media
 * or visual/text mode buttons. We add these ourselves in the component
 */

const $ = window.jQuery;

function getDefaultSettings() {
  if (wp.editor && typeof wp.editor.getDefaultSettings === 'function') {
    return wp.editor.getDefaultSettings();
  }

  return { tinymce: {}, quicktags: {} };
}

export function initializeEditor(id, args) {
  if (!window.tinymce) {
    console.warn('Can not create wp-editor, tinymce was not found on the window!');
    return;
  }

  const settings = getDefaultSettings();

  const tinymceArgs = {
    ...settings.tinymce,
    ...args.tinymce,
  };

  $(document).trigger('wp-before-tinymce-init', tinymceArgs);
  window.tinymce.init(tinymceArgs);

  if (window.quicktags && settings.quicktags) {
    const quicktagsArgs = {
      ...settings.quicktags,
      ...args.quicktags,
    };

    $(document).trigger('wp-before-quicktags-init', quicktagsArgs);
    window.quicktags(quicktagsArgs);
  }
}

export function removeEditor(id) {
  let mceInstance;
  let qtInstance;

  if (window.tinymce) {
    mceInstance = window.tinymce.get(id);

    if (mceInstance) {
      if (!mceInstance.isHidden()) {
        mceInstance.save();
      }

      mceInstance.remove();
    }
  }

  if (window.quicktags) {
    qtInstance = window.QTags.getInstance(id);

    if (qtInstance) {
      qtInstance.remove();
    }
  }
}
