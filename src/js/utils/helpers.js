import camelCase from 'lodash.camelcase';

export function sanitizeValue(value) {
  if (typeof value === 'object') {
    return JSON.stringify(value);
  }

  return value;
}

export function formatMetaProps(props) {
  const transformedProps = {};

  Object.keys(props).forEach(key => {
    transformedProps[camelCase(key)] = props[key];
  });

  // We can't send through reserved Preact props
  transformedProps.metaRef = props.ref || props.id;
  transformedProps.metaKey = props.key;
  transformedProps.className = props.class;

  delete transformedProps.ref;
  delete transformedProps.key;
  delete transformedProps.class;

  return transformedProps;
}

export function transformArgsToProps(args, toTransform = []) {
  let props = {};

  try {
    props = JSON.parse(args);
  } catch (e) {
    // Ignore
  }

  const transformedProps = formatMetaProps(props);

  toTransform.forEach(key => {
    if (typeof props[key] === 'string' && props[key]) {
      try {
        transformedProps[camelCase(key)] = JSON.parse(props[key] || '');
      } catch (e) {
        // Ignore
      }
    }
  });

  return transformedProps;
}

export function isImage(image = '') {
  return /\.(jpe?g|png|bmp)$/.test(image);
}

export function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

export function uniqueConcat(arr1, arr2, getComparator) {
  const keys = new Set();
  const result = [];

  arr1.concat(arr2).forEach(val => {
    const key = getComparator(val);

    if (!keys.has(key)) {
      result.push(val);
      keys.add(key);
    }
  });

  return result;
}
