import { Component } from 'preact';
import shallowEqual from 'shallowequal';

import makeCancelable from './makeCancelable';

class AsyncWrapper extends Component {
  constructor(props) {
    super(props);

    this.state = this.getInitialActionState();
  }

  cancelHandlers = new Map();

  shouldComponentUpdate(props, state) {
    return !shallowEqual(props, this.props) || !shallowEqual(state, this.state);
  }

  componentWillUnmount() {
    this.cancelHandlers.forEach(handler => {
      handler();
    });
  }

  getActionsFromProps() {
    if (this.props.actions && typeof this.props.actions === 'object') {
      return this.props.actions;
    }

    if (typeof this.props.actions === 'function') {
      return this.props.actions(this.props.childrenProps);
    }

    return {};
  }

  getInitialActionState() {
    const state = {};
    const actions = this.getActionsFromProps();

    Object.keys(actions).forEach(id => {
      state[id] = {
        canceled: false,
        error: undefined,
        loading: false,
      };
    });

    return state;
  }

  getConnectedActions() {
    const connectedActions = {};
    const actions = this.getActionsFromProps();

    Object.keys(actions).forEach(id => {
      connectedActions[id] = {
        run: (...args) => this.runAction(id, args),
        cancel: () => this.cancelAction(id),
        ...this.state[id],
      };
    });

    return connectedActions;
  }

  runAction = async (id, args) => {
    const actions = this.getActionsFromProps();
    const action = actions[id];

    if (!action) {
      return Promise.resolve();
    }

    const cancelableAction = makeCancelable(action(...args));
    // Update the cancel handler
    this.cancelHandlers.set(id, cancelableAction.cancel);

    // NOTE: Considering allowing actions to be objects, with the shape
    // { promise: Function<Promise>, options: Object }

    this.setActionState(id, {
      loading: true,
      error: undefined,
    });

    let result;

    try {
      result = await cancelableAction.promise();
    } catch (e) {
      // Check for a canceled promise
      if (e && e.isCanceled) {
        throw e;
      }

      this.setActionState(id, {
        loading: false,
        error: e,
      });
      // Throw the error to allowing chaining
      throw e;
    }

    this.setActionState(id, {
      loading: false,
    });

    return result;
  };

  cancelAction = id => {
    if (!this.state[id].loading || !this.cancelHandlers.has(id)) {
      return;
    }

    const cancel = this.cancelHandlers.get(id);
    cancel();
  };

  setActionState(id, nextState) {
    this.setState(prevState => ({
      [id]: {
        ...prevState[id],
        ...nextState,
      },
    }));
  }

  render() {
    return this.props.children[0](this.getConnectedActions());
  }
}

export default AsyncWrapper;
