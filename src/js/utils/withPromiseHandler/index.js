import { h, Component } from 'preact';
import AsyncWrapper from './AsyncWrapper';

function withPromiseHandler(actions) {
  return WrappedComponent => {
    class WithAsyncWrapper extends Component {
      render() {
        return (
          <AsyncWrapper actions={actions} childrenProps={this.props}>
            {connectedActions => (
              <WrappedComponent
                {...this.props}
                {...connectedActions}
                ref={this.props.getRef}
              />
            )}
          </AsyncWrapper>
        );
      }
    }

    return WithAsyncWrapper;
  };
}

export default withPromiseHandler;
