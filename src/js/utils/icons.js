import { addRuntimeOverride } from '../overrides';

const icons = [];

function defaultGetIconClass(icon) {
  return `icon--${icon}`;
}

addRuntimeOverride('setIconsList', (iconList, getIconClass = defaultGetIconClass) => {
  iconList.forEach(name => {
    icons.push({
      name,
      className: getIconClass(name),
    });
  });
});

export function getIconsList() {
  return icons;
}
