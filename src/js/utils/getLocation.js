import { GOOGLE_API_URL, GOOGLE_API_KEY } from '../constants';

function stringify(args) {
  return Object.keys(args)
    .map(arg => `${arg}=${encodeURIComponent(args[arg])}`)
    .join('&');
}

export default function getLocation(address) {
  const path = `${GOOGLE_API_URL}?${stringify({ address, key: GOOGLE_API_KEY })}`;

  return fetch(path, {
    method: 'POST',
  })
    .then(res => res.json())
    .then(res => {
      if (res && res.results && res.results.length > 0) {
        return res.results[0];
      }

      throw new Error('Google location search failed.');
    });
}
