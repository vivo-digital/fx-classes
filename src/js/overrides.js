import { OVERRIDES_WINDOW_KEY } from './constants';

const OVERRIDES = {};

export function addRuntimeOverride(name, method) {
  OVERRIDES[name] = method;
}

window[OVERRIDES_WINDOW_KEY] = OVERRIDES;
