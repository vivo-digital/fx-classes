import { API_GET_OPTIONS, FX_POST_ID } from '../constants';

const cache = new Map();

function getKey(options, optionsArgs) {
  if (typeof options === 'string') {
    const args = optionsArgs ? JSON.stringify(optionsArgs) : '';
    return `${options}${args ? `.${args}` : ''}`;
  }

  return undefined;
}

export function fetchOptions(options, optionsArgs) {
  const key = getKey(options, optionsArgs);

  if (!key) {
    return null;
  }

  if (cache.has(key)) {
    return cache.get(key);
  }

  const fetchPromise = fetch(API_GET_OPTIONS, {
    body: JSON.stringify({ options, options_args: optionsArgs, post_id: FX_POST_ID }),
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
  })
    .then(res => res.json());

  cache.set(key, fetchPromise);

  return cache.get(key, fetchPromise);
}
