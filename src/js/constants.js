export const API_BASE = window.FX_CONSTANTS.FX_API_URL;
export const API_GET_OPTIONS = `${API_BASE}/options`;
export const GOOGLE_API_URL = 'https://maps.googleapis.com/maps/api/geocode/json';
export const GOOGLE_API_KEY = window.FX_CONSTANTS.GOOGLE_API_KEY;
export const FX_POST_ID = window.FX_CONSTANTS.FX_POST_ID;
export const OVERRIDES_WINDOW_KEY = 'FX_OVERRIDES';
export const KEY_CODES = {
  RETURN: 13,
  ESC: 27,
  LEFT: 37,
  UP: 38,
  RIGHT: 39,
  DOWN: 40,
};
export const TUNNELS = {
  MODAL: 'MODAL',
};
