// eslint-disable-next-line
window.FX_VERSION = preval`
  const fs = require('fs');
  const path = require('path');
  const pck = fs.readFileSync(require.resolve(path.join(process.cwd(), 'package.json')), 'utf8');
  module.exports = JSON.parse(pck).version;
`;
