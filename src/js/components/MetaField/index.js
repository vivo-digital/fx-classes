import { h } from 'preact';
import classnames from 'classnames';

const MetaField = props => {
  const classes = classnames('meta-field', props.className, {
    [`meta-field-${props.type}`]: props.type,
  });

  return (
    <div className={classes}>
      {Boolean(props.title && !props.hideTitle) && (
        <div className="meta-field__title">{props.title}</div>
      )}
      {props.children}
      {props.description && (
        <p
          className="meta-field__description"
          dangerouslySetInnerHTML={{ __html: props.description }}
        />
      )}
    </div>
  );
};

export default MetaField;
