import PropTypes from 'prop-types';
import { h, Component } from 'preact';

class TunnelPlaceholder extends Component {
  static propTypes = {
    children: PropTypes.func,
    id: PropTypes.string.isRequired,
    multiple: PropTypes.bool,
  };

  static contextTypes = {
    tunnelState: PropTypes.object,
  };

  componentDidMount() {
    const { id } = this.props;
    const { tunnelState } = this.context;
    tunnelState.subscribe(id, this.handlePropsChange);
  }

  componentWillUnmount() {
    const { id } = this.props;
    const { tunnelState } = this.context;
    tunnelState.unsubscribe(id, this.handlePropsChange);
  }

  handlePropsChange = () => {
    this.forceUpdate();
  };

  render() {
    const { tunnelState } = this.context;
    const { id, children: renderChildren, multiple } = this.props;
    const tunnelProps = tunnelState.getTunnelProps(id);

    if (typeof renderChildren === 'function') {
      if (Array.isArray(tunnelProps) || multiple) {
        return !tunnelProps
          ? renderChildren({ items: [] })
          : renderChildren({
            items: Array.isArray(tunnelProps) ? tunnelProps : [tunnelProps],
          });
      }
      return renderChildren(tunnelProps || {});
    }

    if (!tunnelProps) {
      return null;
    }

    return tunnelProps.children[0];
  }
}

export default TunnelPlaceholder;
