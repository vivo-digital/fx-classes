import PropTypes from 'prop-types';
import { h, Component } from 'preact';
import TunnelState from './TunnelState';

class TunnelProvider extends Component {
  tunnelState = new TunnelState();

  static propTypes = {
    children: PropTypes.node,
  };

  static childContextTypes = {
    tunnelState: PropTypes.object,
  };

  getChildContext() {
    return {
      tunnelState: this.tunnelState,
    };
  }

  render() {
    return this.props.children[0];
  }
}

export default TunnelProvider;
