import { Component } from 'preact';
import PropTypes from 'prop-types';
import shortid from 'shortid';

class Tunnel extends Component {
  itemId = shortid.generate();

  static propTypes = {
    id: PropTypes.string,
  };

  static contextTypes = {
    tunnelState: PropTypes.object,
  };


  componentDidMount() {
    this.setTunnelProps(this.props);
  }

  componentDidUpdate() {
    this.setTunnelProps(this.props);
  }

  componentWillUnmount() {
    const { id } = this.props;
    const { tunnelState } = this.context;
    tunnelState.setTunnelProps(id, this.itemId, null);
  }

  setTunnelProps(newProps) {
    const { id, ...props } = newProps;
    const { tunnelState } = this.context;
    tunnelState.setTunnelProps(id, this.itemId, props);
  }

  render() {
    return null;
  }
}

export default Tunnel;
