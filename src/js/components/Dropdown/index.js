import { h, Component } from 'preact';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import ButtonIncognito from '../ButtonIncognito';

class Dropdown extends Component {
  // eslint-disable-next-line react/sort-comp
  node;

  constructor(props) {
    super(props);

    this.state = {
      active: props.defaultActive,
    };
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.handleWindowClick);
    clearTimeout(this.delayTimeout);
  }

  handleWindowClick = e => {
    if (!this.node || this.node.contains(e.target)) {
      return;
    }

    this.hide(e);
  };

  show = e => {
    clearTimeout(this.delayTimeout);
    window.addEventListener('click', this.handleWindowClick);

    this.setState({
      active: true,
    });

    if (typeof this.props.onOpen === 'function') {
      this.props.onOpen(e);
    }
  };

  hide = (e, delay) => {
    clearTimeout(this.delayTimeout);
    if (typeof delay === 'number') {
      this.delayTimeout = setTimeout(this.hide, () => delay(e, delay));
      return;
    }

    window.removeEventListener('click', this.handleWindowClick);

    this.setState({
      active: false,
    });

    if (typeof this.props.onClose === 'function') {
      this.props.onClose(e);
    }
  };

  toggle = e => {
    if (this.state.active) {
      this.hide(e);
      return;
    }

    this.show(e);
  };

  renderChildren() {
    if (typeof this.props.children[0] === 'function') {
      return this.props.children[0]({
        active: this.state.active,
        show: this.show,
        hide: this.hide,
        toggle: this.toggle,
      });
    }

    return this.props.children;
  }

  renderTrigger() {
    if (typeof this.props.triggerComponent === 'function') {
      return this.props.triggerComponent({
        active: this.state.active,
        show: this.show,
        hide: this.hide,
        toggle: this.toggle,
      });
    }

    return (
      <ButtonIncognito className="fx-dropdown__trigger" onClick={this.toggle}>
        {this.props.triggerComponent}
      </ButtonIncognito>
    );
  }

  render() {
    return (
      <div
        className="fx-dropdown"
        ref={c => {
          this.node = c;
        }}
      >
        {this.renderTrigger()}
        <div
          align={this.props.align}
          boxShadow={this.props.boxShadow}
          borderRadius={this.props.borderRadius}
          className={classnames('fx-dropdown__popup', `fx-dropdown__popup--${this.props.align}`, {
            'fx-dropdown__popup--active': this.state.active,
            'fx-dropdown__popup--above': this.props.above,
          })}
        >
          {this.renderChildren()}
        </div>
      </div>
    );
  }
}

Dropdown.propTypes = {
  align: PropTypes.oneOf(['left', 'right', 'center']),
  above: PropTypes.bool,
  children: PropTypes.oneOfType([PropTypes.func, PropTypes.node]),
  defaultActive: PropTypes.bool,
  onOpen: PropTypes.func,
  onClose: PropTypes.func,
  triggerComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.node]),
};

Dropdown.defaultProps = {
  align: 'left',
  defaultActive: false,
};

export default Dropdown;
