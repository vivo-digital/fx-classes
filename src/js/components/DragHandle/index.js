import { h } from 'preact';
import { SortableHandle } from 'react-sortable-hoc';
import Icon from '../Icon';

const DragHandle = SortableHandle(props => (
  <span className={props.className}>
    <Icon.drag size="14px" />
  </span>
));

export default DragHandle;
