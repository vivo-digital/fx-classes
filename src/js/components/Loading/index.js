import { h } from 'preact';

export default ({
  color = 'primary',
  size = '24px',
  thickness = '3px',
}) => (
  <div
    className="loading-spinner"
    style={{
      color,
      width: size,
      height: size,
      borderWidth: thickness,
    }}
  />
);
