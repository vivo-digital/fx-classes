import { h, Component } from 'preact';
import shallowEqual from 'shallowequal';
import memoize from 'memoize-one';

import MetaField from '../MetaField';
import { inputMap } from '../Meta';
import { formatMetaProps } from '../../utils/helpers';

export default class MetaInputs extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: props.value,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (!shallowEqual(prevState.value, this.state.value)) {
      if (typeof this.props.onChange === 'function') {
        this.props.onChange(this.state.value);
      }
    }

    if (!shallowEqual(prevProps.inputs, this.props.inputs)) {
      this.resetAllValues(this.props.value);
    }
  }

  get prefix() {
    // All inputs need to be prefixed to avoid id duplication
    return `${this.props.id}-`;
  }

  // Calculate the inputs. If these arguments haven't changed
  // since the last render, `memoize-one` will reuse the last return value.
  getInputs = memoize((inputs, value) => Object.keys(inputs).map(id => {
    const input = inputs[id];
    return formatMetaProps({
      ...input,
      id: `${this.prefix}${id}`,
      originalId: id,
      metaRef: `${this.prefix}${input.metaRef}`,
      name: `${this.prefix}${id}`,
      value: value ? value[id] : undefined,
    });
  }));

  getInputProps = input => ({
    ...input,
    onChange: value => this.handleInputChange(input.id, value),
  });

  resetAllValues = value => {
    this.setState({
      value,
    });
  };

  handleInputChange = (wholeId, value) => {
    const id = wholeId.replace(this.prefix, '');
    // Deep update the value in our state
    this.setState(prevState => ({
      value: {
        ...prevState.value,
        [id]: value,
      },
    }));
  };

  render() {
    const inputs = this.getInputs(this.props.inputs, this.props.value);

    if (typeof this.props.children[0] === 'function') {
      return this.props.children[0]({ inputs, inputMap, getInputProps: this.getInputProps });
    }

    return (
      <div className="meta-input-group">
        {inputs.map(input => {
          const MetaComponent = inputMap[input.type];
          if (MetaComponent) {
            return (
              <MetaField {...input} key={input.id}>
                <MetaComponent {...this.getInputProps(input)} />
              </MetaField>
            );
          }

          return null;
        })}
      </div>
    );
  }
}
