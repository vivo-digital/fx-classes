export function transformOptions(options = [], excludePostType) {
  return options.map(option => {
    let label = option.label || option;
    if (typeof option.title === 'string') {
      label = option.title;
    }

    const value = options.value || option.id || option;

    if (!excludePostType && option.post_type) {
      label = `${label} (${option.post_type})`;
    }

    return {
      label,
      // All option values are strings
      value: `${value}`,
    };
  });
}
