import { h, Component } from 'preact';
import Select from 'react-select';
import Creatable from 'react-select/lib/Creatable';
import classnames from 'classnames';
import shortid from 'shortid';

import SaveInput from '../SaveInput';
import InlineLabel from '../InlineLabel';

import DropdownIndicator from './DropdownIndicator';
import { transformOptions } from './utils';

export default class SelectInput extends Component {
  static defaultProps = {
    isClearable: true,
    isSearchable: true,
  };

  constructor(props) {
    super(props);

    this.state = {
      multiValue: props.isMulti ? props.value : undefined,
    };
  }

  get name() {
    // We need to alias the select inputs name if it's multi
    return this.props.isMulti ? `${shortid.generate()}-multi` : this.props.name;
  }

  getDefaultValue(defaultValue, options) {
    if (defaultValue) {
      if (Array.isArray(defaultValue)) {
        // Recursively look for the default value
        return defaultValue.map(v => this.getDefaultValue(v, options));
      }

      const foundOption = options.find(option => `${option.value}` === `${defaultValue}`);
      if (foundOption) {
        return foundOption;
      }

      if (this.props.creatable) {
        // All option values are strings
        return { value: `${defaultValue}`, label: defaultValue };
      }
    }

    return undefined;
  }

  handleChange = newValue => {
    if (this.hiddenInput) {
      // Trigger a fake change event so the UI knows something has changed
      // https://github.com/JedWatson/react-select/issues/2869
      const event = new Event('change', { bubbles: true });
      this.hiddenInput.dispatchEvent(event);
    }

    if (this.props.isMulti) {
      this.setState(
        {
          multiValue: newValue.map(option => option.value),
        },
        () => {
          if (typeof this.props.onChange === 'function') {
            this.props.onChange(this.state.multiValue);
          }
        },
      );

      return;
    }

    if (typeof this.props.onChange === 'function') {
      this.props.onChange(newValue ? newValue.value : undefined);
    }
  };

  handleInputChange = inputValue => {
    if (typeof this.props.onInputChange === 'function') {
      this.props.onInputChange(inputValue);
    }
  };

  renderArrow() {
    return <span className="Select-arrow" />;
  }

  render() {
    const SelectComponent = this.props.creatable ? Creatable : Select;
    const options = transformOptions(this.props.options, this.props.excludePostType);

    const value = this.props.value || this.props.defaultValue;
    const defaultValue = this.getDefaultValue(value, options);

    return (
      <div className={classnames(this.props.wrapperClassName, 'meta-select')}>
        {this.props.label && (
          <InlineLabel className="meta-select__label">{this.props.label}</InlineLabel>
        )}
        <SelectComponent
          components={{ DropdownIndicator }}
          classNamePrefix="meta-select-react"
          className={classnames(this.props.className, 'meta-select__input')}
          defaultValue={defaultValue}
          id={this.props.id}
          isClearable={this.props.isClearable}
          isSearchable={this.props.isSearchable}
          isMulti={this.props.isMulti}
          maxMenuHeight="200px"
          name={this.name}
          onChange={this.handleChange}
          onInputChange={this.handleInputChange}
          options={options}
          placeholder={this.props.placeholder}
        />
        {this.props.isMulti && <SaveInput name={this.props.name} value={this.state.multiValue} />}
        <input
          type="hidden"
          ref={c => {
            this.hiddenInput = c;
          }}
          name={`${shortid.generate()}-hidden`}
        />
      </div>
    );
  }
}
