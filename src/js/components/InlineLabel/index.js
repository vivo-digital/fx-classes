import { h } from 'preact';
import classnames from 'classnames';

const InlineLabel = props => (
  <label
    {...props}
    className={classnames('meta-input__label', props.className)}
  />
);

export default InlineLabel;
