import { h } from 'preact';
import classnames from 'classnames';

export default props => (
  <button {...props} className={classnames('fx-button-incognito', props.className)} type="button" />
);
