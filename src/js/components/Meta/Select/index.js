import { h, Component } from 'preact';
import PropTypes from 'prop-types';

import SelectInput from '../../SelectInput';
import withOptions from '../../WithOptions/hoc';

/**
 * Dropdown input with optional multi-select support
 *
 * ## Value
 * **Returns** *multiple false* `(string|number)`
 * ```php
 * $value = fx_get_meta('id');
 * ```
 *
 * **Returns** *multiple true* `(string|number)[]`
 * ```php
 * $value = json_decode(fx_get_meta('id'), true);
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'select',
 *   'className' => 'my-class-name',
 *   'creatable' => false,
 *   'excludePostType' => false,
 *   'multiple' => true,
 *   'options' => 'page, post',
 *   'placeholder' => 'Pick some content',
 *   'wrapperClassName' => 'wrapper-class-name',
 * ],
 * ```
 * ## Functions
 * Used in conjunction with `creatable: true`.
 * Allows a user to input an external link or choose a post.
 * The function detects the two and outputs either a permalink, or external link accordingly.
 * ```php
 * echo fx_get_url_from_select('id');
 * ```
 */
class Select extends Component {
  static displayName = 'Select';

  static propTypes = {
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    /**
     * Allow new options to be created
     */
    creatable: PropTypes.bool,
    /**
     * A list of ids to exlude from the options list
     */
    exclude: PropTypes.array,
    /**
     * Hide the post type if the option is a post
     */
    excludePostType: PropTypes.bool,
    id: PropTypes.string.isRequired,
    /**
     * Optional label that appears next to the input, rather than ontop
     */
    label: PropTypes.string,
    /**
     * Allow multiple selections
     */
    multiple: PropTypes.bool,
    metaRef: PropTypes.string.isRequired,
    /**
     * Options to choose from. See [options](options.md) for more information.
     */
    options: PropTypes.array.isRequired,
    /**
     * Placeholder text for the input
     */
    placeholder: PropTypes.string,
    /**
     * Metabox title
     */
    title: PropTypes.string,
    value: PropTypes.string.isRequired,
    /**
     * Metabox wrapping div class name
     */
    wrapperClassName: PropTypes.string,
  };

  static defaultProps = {
    creatable: false,
    excludePostType: false,
    multiple: false,
    placeholder: 'Please select...',
  };

  handleChange = (value) => {
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(value);
    }
  };

  render() {
    return (
      <SelectInput
        className={this.props.className}
        creatable={this.props.creatable}
        defaultValue={this.props.defaultValue}
        excludePostType={this.props.excludePostType}
        id={this.props.id}
        isMulti={this.props.multiple}
        label={this.props.label}
        name={this.props.metaRef}
        options={this.props.options}
        onChange={this.handleChange}
        placeholder={this.props.placeholder}
        value={this.props.value}
        wrapperClassName={this.props.wrapperClassName}
      />
    );
  }
}

export default withOptions(Select);
