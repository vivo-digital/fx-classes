import { h, Component } from 'preact';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import shallowEqual from 'shallowequal';
// Import the sketch picker directly, parcel isn't too good at tree shaking yet
import SketchPicker from 'react-color/lib/Sketch';

import Dropdown from '../../Dropdown';
import SaveInput from '../../SaveInput';
import WpButton from '../../WpButton';

/**
 * Select a colour using a circular colour wheel.
 *
 * ## Value
 * **Returns** `string`
 * ```php
 * $color = fx_get_meta('id');
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'color',
 *   'className' => 'my-class-name',
 *   'buttonText' => 'Pick a colour',
 *   'title' => 'Colour',
 * ],
 * ```
 */
class Color extends Component {
  static displayName = 'Color';

  static propTypes = {
    /**
     * Alternate text for the select color button
     */
    buttonText: PropTypes.string.isRequired,
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    id: PropTypes.string.isRequired,
    metaRef: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    /**
     * Metabox title
     */
    title: PropTypes.string,
    value: PropTypes.string.isRequired,
  };

  static defaultProps = {
    buttonText: 'Select colour',
  };

  constructor(props) {
    super(props);

    this.state = {
      value: this.getColorAsObject(props.value),
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (!shallowEqual(prevState.value, this.state.value)) {
      if (typeof this.props.onChange === 'function') {
        this.props.onChange(this.value);
      }
    }
  }

  get value() {
    if (this.state.value && this.state.value.rgb) {
      const { r, g, b, a } = this.state.value.rgb;

      return `rgba(${r},${g},${b},${a})`;
    }

    return '';
  }

  getColorAsObject(color) {
    // Convert our rgba string back to an object with the same shape as the color picker
    if (color && typeof color === 'string' && color.startsWith('rgba(')) {
      const [r, g, b, a] = color
        .replace('rgba(', '')
        .split(',')
        .map((v) => v.trim().replace(')', ''));

      return {
        rgb: {
          r,
          g,
          b,
          a,
        },
      };
    }

    return {};
  }

  handleColorChange = (value) => {
    this.setState({
      value,
    });
  };

  render() {
    const stringValue = this.value;

    return (
      <div className={classnames(this.props.className, 'meta-color')}>
        <Dropdown
          triggerComponent={({ toggle }) => (
            <div className="meta-color__trigger">
              {stringValue && (
                <div className="meta-color__preview" style={{ backgroundColor: stringValue }} />
              )}
              <WpButton className="meta-color__button" onClick={toggle}>
                {this.props.buttonText}
              </WpButton>
            </div>
          )}
        >
          <div className="meta-color__dropdown">
            <SketchPicker color={this.state.value} onChangeComplete={this.handleColorChange} />
          </div>
        </Dropdown>
        <SaveInput name={this.props.metaRef} value={stringValue} />
      </div>
    );
  }
}

export default Color;
