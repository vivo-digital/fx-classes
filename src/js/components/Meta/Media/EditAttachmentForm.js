import { h, Component } from 'preact';

import Input from '../../Input';
import TextareaInput from '../../TextareaInput';
import WpButton from '../../WpButton';

class EditAttachmentForm extends Component {
  inputs = {};

  componentDidMount() {
    if (this.inputs.title) {
      this.inputs.title.focus();
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();

    this.props.onSubmit({
      ...this.props.attachment,
      description: this.inputs.description.value,
      name: this.inputs.name.value,
      title: this.inputs.title.value,
    });
  };

  render() {
    const { description, name, title } = this.props.attachment;
    const id = this.props.attachment._fx_;

    return (
      <form className="meta-media__form" onSubmit={this.handleSubmit}>
        <div className="fx-form__field">
          <label className="fx-form__label" htmlFor="title">
            Title
          </label>
          <Input
            id={`${id}.title`}
            name={`${id}.title`}
            defaultValue={title}
            getInputRef={(c) => {
              this.inputs.title = c;
            }}
          />
        </div>
        <div className="fx-form__field">
          <label className="fx-form__label" htmlFor="name">
            Name
          </label>
          <Input
            id={`${id}.name`}
            name={`${id}.name`}
            defaultValue={name}
            getInputRef={(c) => {
              this.inputs.name = c;
            }}
          />
        </div>
        <div className="fx-form__field">
          <label className="fx-form__label" htmlFor="description">
            Description
          </label>
          <TextareaInput
            id={`${id}.description`}
            name={`${id}.description`}
            defaultValue={description}
            getInputRef={(c) => {
              this.inputs.description = c;
            }}
          />
        </div>
        <div className="fx-form__field fx-form__field--submit">
          <WpButton primary type="submit">
            Submit
          </WpButton>
        </div>
      </form>
    );
  }
}

export default EditAttachmentForm;
