import { h, Component } from 'preact';

import SortableDragHandle from './SortableDragHandle';
import SortableGrid from './SortableGrid';
import SortableGridItem from './SortableGridItem';
import EditAttachmentForm from './EditAttachmentForm';
import ButtonIncognito from '../../ButtonIncognito';
import Icon from '../../Icon';
import Modal from '../../Modal';
import { isImage } from '../../../utils/helpers';

const ACTIONS = {
  EDIT: 'EDIT',
};

class MediaGrid extends Component {
  state = {
    action: undefined,
    attachment: undefined,
  };

  handleEdit = (attachment) => {
    this.setState({
      action: ACTIONS.EDIT,
      attachment,
    });
  };

  closeActionModal = () => {
    this.setState({
      action: undefined,
      attachment: undefined,
    });
  };

  handleEditSubmit = (attachment) => {
    this.props.editAttachment(attachment);
    this.closeActionModal();
  };

  renderActionModal() {
    switch (this.state.action) {
      case ACTIONS.EDIT:
        return (
          <Modal active close={this.closeActionModal} width="400px">
            <EditAttachmentForm
              attachment={this.state.attachment}
              key={this.state.attachment._fx_}
              onSubmit={this.handleEditSubmit}
            />
          </Modal>
        );
      default:
        return null;
    }
  }

  renderPreview(attachment) {
    return <img src={attachment.url || attachment.filename} alt="" />;
  }

  render() {
    if (!this.props.items) {
      return null;
    }

    const sortable = this.props.items.length > 1;

    if (sortable) {
      return (
        <SortableGrid
          axis="xy"
          className="meta-media__grid"
          helperClass="sorting-helper"
          disabled={!sortable}
          items={this.props.items}
          onSortEnd={this.props.onSortEnd}
          useDragHandle
        >
          {this.props.items.map((attachment, index) => (
            <SortableGridItem className="meta-media__grid__col" key={attachment._fx_} index={index}>
              <div className="meta-media__grid__preview">
                <div className="meta-media__grid__btns">
                  {sortable && <SortableDragHandle className="meta-media__grid__btn" />}
                  {this.props.canEdit && (
                    <ButtonIncognito
                      className="meta-media__grid__btn"
                      onClick={() => this.handleEdit(attachment)}
                    >
                      <Icon.pencil size="12px" />
                    </ButtonIncognito>
                  )}
                  <ButtonIncognito
                    className="meta-media__grid__btn"
                    onClick={() => this.props.removeAttachment(attachment)}
                  >
                    <Icon.cross size="10px" />
                  </ButtonIncognito>
                </div>
                {this.renderPreview(attachment)}
              </div>
            </SortableGridItem>
          ))}
          {this.renderActionModal()}
        </SortableGrid>
      );
    }

    return (
      <div className="meta-media__single">
        {this.props.items.map((attachment) => {
          return (
            <div>
              {this.renderPreview(attachment)}
              <ButtonIncognito
                className="meta-media__grid__btn"
                onClick={() => this.props.removeAttachment(attachment)}
              >
                <Icon.cross size="10px" />
              </ButtonIncognito>
            </div>
          );
        })}
      </div>
    );
  }
}

export default MediaGrid;
