import { h } from 'preact';
import { SortableContainer } from 'react-sortable-hoc';

const SortableGrid = props => (
  <div {...props} className={props.className}>
    {props.children}
  </div>
);

export default SortableContainer(SortableGrid);
