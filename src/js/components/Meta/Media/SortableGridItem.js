import { h } from 'preact';
import { SortableElement } from 'react-sortable-hoc';

const SortableGridItem = props => <div {...props}>{props.children}</div>;

export default SortableElement(SortableGridItem);
