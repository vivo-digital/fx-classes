import { h } from 'preact';
import { SortableHandle } from 'react-sortable-hoc';
import Icon from '../../Icon';

const DragHandle = SortableHandle(props => (
  <span className={props.className}>
    <Icon.move size="12px" />
  </span>
));

export default DragHandle;
