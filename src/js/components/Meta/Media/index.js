import { h, Component } from 'preact';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import shallowEqual from 'shallowequal';
import shortid from 'shortid';
import { arrayMove } from 'react-sortable-hoc';

import MediaGrid from './MediaGrid';
import SaveInput from '../../SaveInput';
import WpButton from '../../WpButton';
import ButtonIncognito from '../../ButtonIncognito';
import Icon from '../../Icon';
import { uniqueConcat } from '../../../utils/helpers';

/**
 * Select media using the default WP media popup.
 *
 * ## Value
 *
 * #### `detailed => false`
 * **Returns** `(string)`
 * ```php
 * $image_url = fx_get_meta('id');
 * ```
 *
 * #### `detailed => true`
 * **Returns** `ImageObject`
 * ```php
 * $image = json_decode(fx_get_meta('id'), true);
 * $image_description = $image['description'];
 * $image_id = $image['id'];
 * $image_name = $image['name'];
 * $image_title = $image['title'];
 * $image_url = $image['url'];
 * ```
 *
 * #### `multiple => true`
 * **Returns** `(string|ImageObject)[]`
 * ```php
 * $images = json_decode(fx_get_meta('id'), true);
 * foreach($images as $image) {
 *   // If detailed = true
 *   $image_url = $image;
 *   // If detailed = false
 *   $image_description = $image['description'];
 *   $image_id = $image['id'];
 *   $image_name = $image['name'];
 *   $image_title = $image['title'];
 *   $image_url = $image['url'];
 * }
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'media',
 *   'allowDuplicates' => false,
 *   'className' => 'my-class-name',
 *   'detailed' => true,
 *   'multiple' => true,
 *   'title' => 'Gallery',
 *   'buttonText' => 'Add gallery image',
 * ],
 * ```
 */
export default class Media extends Component {
  static displayName = 'Media';

  static propTypes = {
    /**
     * Allow the same attachment multiple times
     */
    allowDuplicates: PropTypes.bool,
    /**
     * Alternate text for the add media button
     */
    buttonText: PropTypes.string,
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    /**
     * Enable detail editing for attachments
     */
    detailed: PropTypes.bool,
    id: PropTypes.string.isRequired,
    metaRef: PropTypes.string.isRequired,
    /**
     * Enable multiple attachment selections
     */
    multiple: PropTypes.bool,
    /**
     * Metabox title
     */
    title: PropTypes.string,
    value: PropTypes.string.isRequired,
  };

  static defaultProps = {
    allowDuplicates: false,
    buttonText: 'Add media',
    detailed: false,
    multiple: false,
  };

  constructor(props) {
    super(props);

    let attachments = [];
    if (props.value) {
      attachments = Array.isArray(props.value) ? props.value : [props.value];
    }

    // Cleanup the input value if the media multiple prop has been changed
    if (!props.multiple && attachments.length > 1) {
      attachments.length = 1;
    }

    // Check if we have enough data, if not rebuild a mock attachment from the string
    attachments = attachments.map((attachment) =>
      typeof attachment === 'object'
        ? attachment
        : {
            _fx_: shortid.generate(),
            id: attachment,
            filename: attachment,
            url: attachment,
          },
    );

    this.state = {
      attachments,
    };

    this.frame = undefined;
  }

  componentDidUpdate(prevProps, prevState) {
    if (!shallowEqual(prevState.attachments, this.state.attachments)) {
      if (typeof this.props.onChange === 'function') {
        this.props.onChange(this.value);
      }
    }
  }

  get value() {
    const { attachments } = this.state;
    if (!attachments || attachments.length === 0) {
      return '';
    }

    const { multiple, detailed } = this.props;

    const value = detailed ? attachments : attachments.map((attachment) => attachment.url);
    return multiple ? value : value[0];
  }

  handleOpen = () => {
    if (this.frame) {
      this.frame.open();
      return;
    }

    this.frame = wp.media({
      title: 'Select or Upload Media',
      button: {
        text: 'Use this media',
      },
      multiple: this.props.multiple,
    });

    this.frame.on('select', () => {
      const originalAttachments = this.frame.state().get('selection').toJSON();

      // Store additional meta data about the attachment
      const attachments = originalAttachments.map((attachment) => ({
        _fx_: shortid.generate(),
        alt: attachment.alt,
        caption: attachment.caption,
        description: attachment.description,
        id: attachment.id,
        filename: attachment.filename,
        name: attachment.name,
        title: attachment.title,
        url: attachment.url,
      }));

      if (this.props.multiple) {
        this.setState((prevState) => {
          if (this.props.allowDuplicates) {
            return {
              attachments: prevState.attachments.concat(attachments),
            };
          }

          return {
            attachments: uniqueConcat(prevState.attachments, attachments, (a) => a.id),
          };
        });

        return;
      }

      this.setState({
        attachments,
      });
    });

    this.frame.open();
  };

  handleRemove = () => {
    this.setState({
      attachments: undefined,
    });
  };

  handleSortSections = ({ oldIndex, newIndex }) => {
    this.setState((prevState) => ({
      attachments: arrayMove(prevState.attachments, oldIndex, newIndex),
    }));
  };

  removeAttachment = (attachment) => {
    this.setState((prevState) => ({
      attachments: prevState.attachments.filter((a) => a._fx_ !== attachment._fx_),
    }));
  };

  editAttachment = (attachment) => {
    this.setState((prevState) => ({
      // Lazy way to edit the attachment, map over the current attachment and return the new
      // attachment once we find the right entry.
      attachments: prevState.attachments.map((originalAttachment) => {
        if (originalAttachment._fx_ !== attachment._fx_) {
          // Not the attachment we are editing, return the original
          return originalAttachment;
        }

        return attachment;
      }),
    }));
  };

  render() {
    const hasImage = this.state.attachments.length > 0;
    return (
      <div
        className={classnames('meta-media', this.props.className, {
          'meta-media--detailed': this.props.detailed,
          'meta-media--multiple': this.props.multiple,
          'meta-media--single': !this.props.multiple,
          'meta-media--populated': hasImage,
          'meta-media--empty': !hasImage,
        })}
      >
        <MediaGrid
          canEdit={this.props.detailed}
          editAttachment={this.editAttachment}
          items={this.state.attachments}
          onSortEnd={this.handleSortSections}
          removeAttachment={this.removeAttachment}
        />
        {hasImage && (
          <ButtonIncognito
            className="meta-media__grid__btn meta-media__grid__btn--edit"
            onClick={this.handleOpen}
          >
            <Icon.pencil size="10px" />
          </ButtonIncognito>
        )}
        {!hasImage && (
          <WpButton className="meta-media__trigger" onClick={this.handleOpen} primary>
            {this.props.buttonText}
          </WpButton>
        )}
        <SaveInput className="meta-media__input" name={this.props.metaRef} value={this.value} />
      </div>
    );
  }
}
