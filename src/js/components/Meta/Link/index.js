import { h, Component } from 'preact';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import shallowEqual from 'shallowequal';

import ButtonIncognito from '../../ButtonIncognito';
import Icon from '../../Icon';
import SaveInput from '../../SaveInput';
import WpButton from '../../WpButton';

let originalCloseHandler;
const LINK_URL_INPUT = '#wp-link-url';
const LINK_TEXT_INPUT = '#wp-link-text';
const LINK_TARGET_INPUT = '#wp-link-target';

/**
 * Use the WP Link popup to select existing content and a title.
 *
 * ## Value
 * **Returns** `{ text: string, href: string, target: string }`
 * ```php
 * $value = json_decode(fx_get_meta('id'), true);
 * $text = $value['text'];
 * $href = $value['href'];
 * $target = $value['target'];
 *
 * printf('<a href="%s" target="%s">%s</a>', $href, $target, $text);
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'link',
 *   'className' => 'my-class-name',
 *   'title' => 'Button',
 *   'buttonText' => 'Add button link',
 *   'buttonEditText' => 'Edit button link',
 * ],
 * ```
 * ## Functions
 * ```php
 * echo fx_get_link('id', 'link-class');
 * ```
 * **Returns**
 * `<a class="link-class" href="link-url" target="link-target" aria-label="Link Text">Link Text</a>`
 */
class Link extends Component {
  static displayName = 'Link';

  static propTypes = {
    /**
     * Alternate text for the add link button
     */
    buttonText: PropTypes.string,
    /**
     * Alternate text for the edit link button
     */
    buttonEditText: PropTypes.string,
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    id: PropTypes.string.isRequired,
    metaRef: PropTypes.string.isRequired,
    /**
     * Metabox title
     */
    title: PropTypes.string,
    value: PropTypes.string.isRequired,
  };

  static defaultProps = {
    buttonText: 'Add link',
    buttonEditText: 'Edit link',
  };

  constructor(props) {
    super(props);

    this.state = {
      value: props.value,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (!shallowEqual(prevState.value, this.state.value)) {
      if (typeof this.props.onChange === 'function') {
        this.props.onChange(this.state.value);
      }
    }
  }

  openLinkBox = () => {
    window.wpLink.open(this.props.id);
    // Clean the input value
    this.input.value = '';
    // Bind will create a new function each time
    originalCloseHandler = window.wpLink.close.bind(window.wpLink);
    // Override window settings
    window.wpLink.close = this.handleClose;
    if (this.state.value.href) {
      this.populateLinkBox();
    }
  };

  handleClose = () => {
    originalCloseHandler();
    this.handleChange();
    // Restore window settings
    window.wpLink.close = originalCloseHandler;
  };

  handleCancel = () => {
    this.setState({
      value: {},
    });
  };

  handleChange = () => {
    if (!this.input || !this.input.value) {
      return;
    }

    const htmlString = this.input.value;

    const div = document.createElement('div');
    div.innerHTML = htmlString;
    const link = div.querySelector('a');

    if (!link) {
      return;
    }

    const text = link.innerText;
    const href = link.getAttribute('href');
    const target = link.getAttribute('target');

    this.setState({
      value: {
        text,
        href,
        target,
      },
    });
  };

  populateLinkBox() {
    const urlInput = document.querySelector(LINK_URL_INPUT);
    const textInput = document.querySelector(LINK_TEXT_INPUT);
    const targetInput = document.querySelector(LINK_TARGET_INPUT);

    if (!urlInput) {
      return;
    }

    urlInput.value = this.state.value.href;
    textInput.value = this.state.value.text;
    targetInput.checked = this.state.value.target === '_blank';
  }

  render() {
    const { text, href } = this.state.value || {};

    const hasValue = Boolean(href);
    return (
      <div
        className={classnames(this.props.className, 'meta-link', {
          'meta-link--has-value': hasValue,
        })}
      >
        <div className="meta-link__hidden-input">
          <input
            type="text"
            id={this.props.id}
            ref={(c) => {
              this.input = c;
            }}
          />
        </div>
        {hasValue && (
          <div className="meta-link__value">
            {Boolean(text) && <div className="meta-link__text">{text}</div>}
            <div className="meta-link__href">{href}</div>
          </div>
        )}
        <WpButton onClick={this.openLinkBox}>
          {hasValue ? this.props.buttonEditText : this.props.buttonText}
        </WpButton>
        {hasValue && (
          <ButtonIncognito className="meta-link__cancel" onClick={this.handleCancel}>
            <Icon.cross />
          </ButtonIncognito>
        )}
        <SaveInput name={this.props.metaRef} value={this.state.value} />
      </div>
    );
  }
}

export default Link;
