import { h, Component, useRef } from 'preact';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { init, exec } from 'pell';
import shallowEqual from 'shallowequal';
import striptags from 'striptags';

import SaveInput from '../../SaveInput';
import InlineLabel from '../../InlineLabel';
import TextareaInput from '../../TextareaInput';

/**
 * Simple texteditor input.
 * https://github.com/jaredreich/pell
 *
 *  ![Text Editor](/img/meta/text-editor.png)
 *
 * ## Value
 * **Returns** `html`
 * ```php
 * $value = fx_get_meta('id');
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'text-editor',
 *   'className' => 'my-class-name',
 *   'label' => 'Enter something below',
 *   'title' => 'Description',
 * ],
 * ```
 */
class TextEditor extends Component {
  editor = null;

  static displayName = 'TextEditor';

  static propTypes = {
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    id: PropTypes.string.isRequired,
    /**
     * Optional label that appears next to the input, rather than ontop
     */
    label: PropTypes.string,
    metaRef: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    /**
     * Metabox title
     */
    value: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = { value: props.value };
  }

  componentDidMount() {
    this.mountEditor();
  }

  componentDidUpdate(prevProps, prevState) {
    if (!shallowEqual(prevState.value, this.state.value)) {
      if (typeof this.props.onChange === 'function') {
        this.props.onChange(this.state.value);
      }
    }
  }

  handleChange = (value) => {
    this.setState({
      value,
    });
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(value);
    }
  };

  mountEditor = () => {
    this.editor = init({
      element: document.getElementById(`${this.props.id}`),
      onChange: this.handleChange,
      actions: [
        {
          name: 'heading2',
          title: 'Heading2',
          result: () => exec('formatBlock', '<h2>'),
        },
        {
          name: 'heading3',
          icon: '<b>H<sub>3</sub></b>',
          title: 'Heading3',
          result: () => exec('formatBlock', '<h3>'),
        },
        {
          name: 'bold',
          icon: '<svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" role="img" aria-hidden="true" focusable="false"><path d="M14.7 11.3c1-.6 1.5-1.6 1.5-3 0-2.3-1.3-3.4-4-3.4H7v14h5.8c1.4 0 2.5-.3 3.3-1 .8-.7 1.2-1.7 1.2-2.9.1-1.9-.8-3.1-2.6-3.7zm-5.1-4h2.3c.6 0 1.1.1 1.4.4.3.3.5.7.5 1.2s-.2 1-.5 1.2c-.3.3-.8.4-1.4.4H9.6V7.3zm4.6 9c-.4.3-1 .4-1.7.4H9.6v-3.9h2.9c.7 0 1.3.2 1.7.5.4.3.6.8.6 1.5s-.2 1.2-.6 1.5z"></path></svg>',
          title: 'Bold',
          result: () => exec('bold'),
        },
        {
          name: 'italic',
          icon: '<svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" role="img" aria-hidden="true" focusable="false"><path d="M12.5 5L10 19h1.9l2.5-14z"></path></svg>',
          title: 'Italic',
          result: () => exec('italic'),
        },
        {
          name: 'olist',
          icon: '<svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" role="img" aria-hidden="true" focusable="false"><path d="M11.1 15.8H20v-1.5h-8.9v1.5zm0-8.6v1.5H20V7.2h-8.9zM5 6.7V10h1V5.3L3.8 6l.4 1 .8-.3zm-.4 5.7c-.3.1-.5.2-.7.3l.1 1.1c.2-.2.5-.4.8-.5.3-.1.6 0 .7.1.2.3 0 .8-.2 1.1-.5.8-.9 1.6-1.4 2.5h2.7v-1h-1c.3-.6.8-1.4.9-2.1.1-.3 0-.8-.2-1.1-.5-.6-1.3-.5-1.7-.4z"></path></svg>',
          title: 'Ordered List',
          result: () => exec('insertOrderedList'),
        },
        {
          name: 'ulist',
          icon: '<svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" role="img" aria-hidden="true" focusable="false"><path d="M11.1 15.8H20v-1.5h-8.9v1.5zm0-8.6v1.5H20V7.2h-8.9zM6 13c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0-7c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"></path></svg>',
          title: 'Unordered List',
          result: () => exec('insertUnorderedList'),
        },
        {
          name: 'link',
          icon: '<svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" role="img" aria-hidden="true" focusable="false"><path d="M15.6 7.2H14v1.5h1.6c2 0 3.7 1.7 3.7 3.7s-1.7 3.7-3.7 3.7H14v1.5h1.6c2.8 0 5.2-2.3 5.2-5.2 0-2.9-2.3-5.2-5.2-5.2zM4.7 12.4c0-2 1.7-3.7 3.7-3.7H10V7.2H8.4c-2.9 0-5.2 2.3-5.2 5.2 0 2.9 2.3 5.2 5.2 5.2H10v-1.5H8.4c-2 0-3.7-1.7-3.7-3.7zm4.6.9h5.3v-1.5H9.3v1.5z"></path></svg>',
          title: 'Link',
          result: () => {
            const url = window.prompt('Enter the link URL');
            if (url) exec('createLink', url);
          },
        },
        {
          name: 'Clear Formatting',
          icon: '<svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M3.27 5L2 6.27L8.97 13.24L6.5 19H9.5L11.07 15.34L16.73 21L18 19.73L3.55 5.27L3.27 5ZM6 5V5.18L8.82 8H11.22L10.5 9.68L12.6 11.78L14.21 8H20V5H6Z"/></svg>',
          title: 'Clear Formatting',
          result: () => exec('removeFormat'),
        },
      ],
      defaultParagraphSeparator: 'p',
      classes: {
        actionbar: 'editor-actionbar',
        button: 'editor-button',
        content: 'editor-content',
        selected: 'editor-button-selected',
      },
    });
    if (this.state.value) {
      this.editor.content.innerHTML = this.state.value;
    }
    this.editor.onpaste = (e) => {
      // Clear formatting on paste except whitelist tags
      e.stopPropagation();
      e.preventDefault();

      const clipboardData = e.clipboardData || window.clipboardData;
      let pastedData = clipboardData.getData('text/html');
      if (pastedData === '') {
        pastedData = clipboardData.getData('text/plain');
      }

      const tagWhiteList = ['p', 'br', 'ul', 'ol', 'li', 'a'];
      pastedData = striptags(pastedData, tagWhiteList); // remove all html except the listed tags

      const wrapper = document.createElement('div');
      wrapper.innerHTML = pastedData;

      const allChildren = [...wrapper.getElementsByTagName('*')];

      allChildren.forEach((element) => {
        element.removeAttribute('id');
        element.removeAttribute('class');
        element.removeAttribute('style');
      });

      pastedData = wrapper.innerHTML;
      exec('insertHTML', pastedData);
    };
  };

  render() {
    return (
      <div className={classnames(this.props.className, 'meta-wp-text-editor')}>
        {this.props.label && <label className="meta-wp-editor__label">{this.props.label}</label>}
        <div id={`${this.props.id}`} className="texteditor" />
        <SaveInput name={this.props.metaRef} value={this.state.value} />
      </div>
    );
  }
}

export default TextEditor;
