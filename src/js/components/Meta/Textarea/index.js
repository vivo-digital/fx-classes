import { h, Component } from 'preact';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import InlineLabel from '../../InlineLabel';
import TextareaInput from '../../TextareaInput';

/**
 * Simple textarea input.
 *
 * ## Value
 * **Returns** `string`
 * ```php
 * $value = fx_get_meta('id');
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'textarea',
 *   'className' => 'my-class-name',
 *   'label' => 'Enter something below',
 *   'placeholder' => '...',
 *   'readOnly' => false,
 *   'rows' => 5,
 *   'title' => 'Description',
 * ],
 * ```
 */
class Textarea extends Component {
  static displayName = 'Textarea';

  static propTypes = {
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    id: PropTypes.string.isRequired,
    /**
     * Optional label that appears next to the input, rather than ontop
     */
    label: PropTypes.string,
    metaRef: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    /**
     * Placeholder text for the input
     */
    placeholder: PropTypes.string,
    /**
     * Disable editing of the input
     */
    readOnly: PropTypes.bool,
    /**
     * Textarea row count
     */
    rows: PropTypes.number,
    /**
     * Metabox title
     */
    title: PropTypes.string,
    value: PropTypes.string.isRequired,
  };

  static defaultProps = {
    readOnly: false,
    rows: 4,
  };

  handleChange = (e) => {
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(e.target.value);
    }
  };

  render() {
    return (
      <div className={classnames(this.props.className, 'meta-textarea')}>
        {this.props.label && (
          <InlineLabel className="meta-textarea__label">{this.props.label}</InlineLabel>
        )}
        <TextareaInput
          className="meta-textarea__input"
          id={this.props.id}
          name={this.props.metaRef}
          onChange={this.handleChange}
          placeholder={this.props.placeholder}
          readOnly={this.props.readOnly}
          rows={this.props.rows}
        >
          {this.props.value}
        </TextareaInput>
      </div>
    );
  }
}

export default Textarea;
