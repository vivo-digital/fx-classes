import { h, Component } from 'preact';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import omit from 'lodash.omit';
import shallowEqual from 'shallowequal';
import { arrayMove } from 'react-sortable-hoc';

import SortableDragHandle from '../Media/SortableDragHandle';
import SortableGrid from '../Media/SortableGrid';
import SortableGridItem from '../Media/SortableGridItem';

import ButtonIncognito from '../../ButtonIncognito';
import Icon from '../../Icon';
import SaveInput from '../../SaveInput';
import WpButton from '../../WpButton';
import WpRichText from '../WpRichText';
import Media from '../Media';
import Textarea from '../Textarea';

/**
 * Repeatable Rich Text Editor with Media
 *
 * ## Value
 * **Returns** `{ content: string, media: string }[]`
 * ```php
 * $items = json_decode(fx_get_meta('id'), true);
 * foreach($items as $item) {
 *   $content = $item['content'];
 *   $media = $item['media'];
 * }
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'rich-text-media',
 *   'buttonText' => 'Add',
 *   'className' => 'my-class-name',
 *   'limit' => 4,
 *   'textLabel' => 'Title',
 *   'imageLabel' => 'Value',
 * ],
 * ```
 */
export default class RichTextMedia extends Component {
  static displayName = 'RichTextMedia';

  static propTypes = {
    /**
     * Alternate text for the add button
     */
    buttonText: PropTypes.string,
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    id: PropTypes.string.isRequired,
    /**
     * Limit the number of inputs allowed
     */
    limit: PropTypes.number,
    /**
     * Show the inputs in a list view
     */
    listView: PropTypes.bool,
    metaRef: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    /**
     * Metabox title
     */
    title: PropTypes.string,
    /**
     * Label for the title input
     */
    textLabel: PropTypes.string,
    value: PropTypes.string.isRequired,
    /**
     * Label for the value input
     */
    imageLabel: PropTypes.string,
  };

  static defaultProps = {
    buttonText: 'Add',
    textLabel: 'Title',
    imageLabel: 'Description',
  };

  constructor(props) {
    super(props);

    const { value } = props;
    const id = Date.now();

    // Add ids for old values
    if (Array.isArray(props.value)) {
      props.value.forEach((pair, index) => {
        if (!pair.id) {
          pair.id = `${id}-${index}`;
        }
      });
    }

    this.state = {
      value,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (!shallowEqual(prevState.value, this.state.value)) {
      if (typeof this.props.onChange === 'function') {
        this.props.onChange(this.value);
      }
    }
  }

  get value() {
    // Save the pairs as an array
    return Object.keys(this.state.value).map((id) => this.state.value[id]);
  }

  // Update value of each field in state array dependently @TODO
  handleInputChange = (newValue, id, key) => {
    // Deep update the value in our state
    this.setState((prevState) => ({
      value: prevState.value.map((value, index) => {
        if (index === id) {
          return {
            ...value,
            [key]: newValue,
          };
        }
        return value;
      }),
    }));
  };

  addPair = () => {
    const blankItem = { content: '', media: '', id: Date.now() };
    this.setState((prevState) => ({
      value: [...prevState.value, blankItem],
    }));
  };

  removePair = (id) => {
    this.setState((prevState) => ({
      value: prevState.value.filter((v, index) => {
        return id !== index;
      }),
    }));
  };

  handleSort = ({ oldIndex, newIndex }) => {
    this.setState((prevState) => ({
      value: arrayMove(prevState.value, oldIndex, newIndex),
    }));
  };

  renderPair = (id, index) => {
    const item = this.state.value[id];
    const onTitleChange = (e) => this.handleInputChange(e, index, 'content');
    const onValueChange = (e) => this.handleInputChange(e, index, 'media');
    const onRemoveClick = () => this.removePair(index);

    return (
      <SortableGridItem className="rich-text-media-row" key={item.id} index={index}>
        <Textarea id={`item-content-${item.id}`} onChange={onTitleChange} value={item.content} />
        <Media id={`item-media-${item.id}`} onChange={onValueChange} value={item.media} />
        <div className="meta-media__grid__btns">
          <SortableDragHandle className="meta-rich-text-media__order" />
        </div>
        <ButtonIncognito className="meta-input-pairs__item__remove" onClick={onRemoveClick}>
          <Icon.cross size="10px" />
        </ButtonIncognito>
      </SortableGridItem>
    );
  };

  render() {
    if (!this.state.value) return;
    const valueIds = Object.keys(this.state.value);
    const canAddMore = this.props.limit ? valueIds.length < this.props.limit : true;

    return (
      <div
        className={classnames('meta-input-pairs', this.props.className, {
          'meta-input-pairs--list': this.props.listView,
        })}
      >
        <div className="meta-input-pairs__preview">
          <div className="rich-text-media-row mb-20">
            <span className="meta-input-pairs__label">{this.props.textLabel}</span>
            <span className="meta-input-pairs__label">{this.props.imageLabel}</span>
          </div>
          <SortableGrid
            axis="xy"
            className="meta-media__grid"
            helperClass="sorting-helper"
            items={valueIds}
            onSortEnd={this.handleSort}
            useDragHandle
          >
            {valueIds.map(this.renderPair)}
          </SortableGrid>
        </div>
        {canAddMore && (
          <WpButton className="select-media__trigger" onClick={this.addPair} primary>
            {this.props.buttonText}
          </WpButton>
        )}
        <SaveInput name={this.props.metaRef} value={this.value} />
      </div>
    );
  }
}
