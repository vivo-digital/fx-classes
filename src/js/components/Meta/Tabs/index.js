import { h, Component } from 'preact';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import shallowEqual from 'shallowequal';

import MetaInputs from '../../MetaInputs';
import SaveInput from '../../SaveInput';

/**
 * Tabs can be used to group sets of inputs and save them all together. They are a useful
 * alternative to multiple groups of inputs.
 * Tabs takes all normal props, as well as `inputs`.
 *
 * ## Value
 * **Returns** `any[][]`
 * ```php
 * $tabs = json_decode(fx_get_meta('id'), true);
 * $title = $tabs['About']['title'];
 * $description = $tabs['About']['description'];
 * $gallery_images = $tabs['Gallery']['images'];
 * $view_more_link = $tabs['Gallery']['view_more'];
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'tabs',
 *   'className' => 'my-class-name',
 *   'tabPosition' => 'left',
 *   'inputs' => [
 *     'About' => [
 *       'title' => [
 *         'type' => 'textfield',
 *         'title' => 'Title',
 *       ],
 *       'description' => [
 *         'type' => 'wp-editor',
 *         'title' => 'Description',
 *       ],
 *     ],
 *     'Gallery' => [
 *       'view_more' => [
 *         'type' => 'link',
 *         'title' => 'View more link',
 *       ],
 *       'images' => [
 *         'type' => 'media',
 *         'className' => 'section-media-class',
 *         'detailed' => true,
 *         'multiple' => true,
 *         'title' => 'Gallery',
 *       ],
 *     ],
 *   ],
 * ],
 * ```
 */
export default class Switcher extends Component {
  static displayName = 'Tabs';

  static propTypes = {
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    id: PropTypes.string.isRequired,
    /**
     * Meta inputs to render
     */
    inputs: PropTypes.object.isRequired,
    metaRef: PropTypes.string.isRequired,
    /**
     * Position of the tabs, either top or left
     */
    tabPosition: PropTypes.oneOf(['top', 'left']),
    /**
     * Metabox title
     */
    title: PropTypes.string,
    value: PropTypes.string.isRequired,
  };

  static defaultProps = {
    tabPosition: 'top',
    inputs: {},
  };

  constructor(props) {
    super(props);

    this.state = {
      active: this.tabs[0],
      value: props.value || {},
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (!shallowEqual(prevState.value, this.state.value)) {
      if (typeof this.props.onChange === 'function') {
        this.props.onChange(this.value);
      }
    }
  }

  get tabs() {
    return Object.keys(this.props.inputs);
  }

  get inputs() {
    if (this.state.active) {
      return this.props.inputs[this.state.active];
    }

    return undefined;
  }

  setActiveTab = (active) => {
    this.setState({
      active,
    });
  };

  handleInputChange = (value) => {
    this.setState((prevState) => ({
      value: {
        ...prevState.value,
        [prevState.active]: value,
      },
    }));
  };

  renderTab(tab) {
    const active = this.state.active === tab;

    return (
      <button
        aria-pressed={active ? 'true' : 'false'}
        className={classnames('meta-tabs__button', { 'meta-tabs__button--active': active })}
        id={`tab-${tab}`}
        key={`tab-${tab}`}
        onClick={() => this.setActiveTab(tab)}
        type="button"
      >
        {tab}
      </button>
    );
  }

  render() {
    const classes = classnames(
      'meta-tabs',
      `meta-tabs--${this.props.tabPosition}`,
      this.props.className,
    );
    const inputs = this.inputs;

    return (
      <div className={classes}>
        <header className="meta-tabs__header" role="tablist">
          {this.tabs.map((tab) => this.renderTab(tab))}
        </header>
        <div className="meta-tabs__content">
          {inputs && (
            <MetaInputs
              // Using a key will force the component to remount on tab change, this will avoid
              // unnecessary componentDidUpdate methods with reset values.
              key={this.state.active}
              id={this.state.active}
              inputs={inputs}
              onChange={this.handleInputChange}
              value={this.state.value[this.state.active]}
            />
          )}
        </div>
        <SaveInput name={this.props.metaRef} value={this.state.value} />
      </div>
    );
  }
}
