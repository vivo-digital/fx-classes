import { h, Component } from 'preact';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import WpButton from '../../WpButton';
import TextareaInput from '../../TextareaInput';
import { initializeEditor, removeEditor } from '../../../utils/initializeWPEditor';

const editorMap = new Map();

function editorIsBroken(editor) {
  try {
    // Moving an editor in the DOM can cause the iframe to unload it's content which
    // breaks the editor visual mode.
    return !editor.iframeElement.contentDocument.body.innerHTML;
  } catch (err) {
    // Oh dear
  }

  return true;
}

/**
 * A TinyMCE editor similar to the wp-editor content.
 *
 * ## Value
 * **Returns** `string`
 * ```php
 * $value = fx_get_meta('id');
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'wp-editor',
 *   'className' => 'my-class-name',
 *   'title' => 'Some Content',
 * ],
 * ```
 */
class WpEditor extends Component {
  static displayName = 'WpEditor';

  static propTypes = {
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    id: PropTypes.string.isRequired,
    /**
     * Optional textfield label
     */
    label: PropTypes.string,
    metaRef: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    /**
     * tinymce arguments
     */
    tinymce: PropTypes.object.isRequired,
    /**
     * Metabox title
     */
    title: PropTypes.string,
    /**
     * quicktags arguments
     */
    quicktags: PropTypes.object.isRequired,
    value: PropTypes.string.isRequired,
  };

  static defaultProps = {
    tinymce: {
      wpautop: false,
      plugins:
        'charmap,colorpicker,hr,lists,paste,tabfocus,textcolor,fullscreen,wordpress,wpautoresize,wpeditimage,wpemoji,wpgallery,wplink,wptextpattern',
      toolbar1:
        'formatselect bold italic | bullist numlist | blockquote | alignleft aligncenter alignright | link unlink | wp_more | spellchecker',
    },
    quicktags: { buttons: 'strong,em,del,ul,ol,li,close' },
  };

  componentDidMount() {
    this.mountEditor();
  }

  shouldComponentUpdate() {
    if (this.editor && editorIsBroken(this.editor)) {
      this.mountEditor(true);
    }

    // We don't want (or need) the to component to re-render, it will break tinymce
    return false;
  }

  componentWillUnmount() {
    if (this.editor) {
      // Remove event listeners, but keep the editor instance. Removing the editor seems to break
      // the editor on remount, e.g. in Tabs or Switcher.
      this.editor.off();
    }
  }

  onInitializeEditor = (editor) => {
    this.editor = editor;
    this.editor.setContent(this.props.value || '');
    this.editor.on('change keyup setcontent', this.handleChange);

    this.textarea.style.visibility = '';

    editorMap.set(this.props.id, editor);
    // Call show now to guarantee the editor is open and visible
    this.editor.show();
    this.ensureTextareaVisible();
  };

  getInputRef = (c) => {
    if (c && !this.textarea) {
      this.textarea = c;
    }
  };

  mountEditor = (isRefresh = false) => {
    if (editorMap.has(this.props.id)) {
      // Check for an existing editor, then destroy the old instance.
      this.editor = null;
      removeEditor(this.props.id);
      editorMap.delete(this.props.id);
    }

    const initArgs = {
      tinymce: {
        inline: false,
        selector: `#${this.props.id}`,
        ...this.props.tinymce,
        init_instance_callback: this.onInitializeEditor,
      },
      quicktags: {
        ...this.props.quicktags,
        id: this.props.id,
      },
    };

    if (isRefresh) {
      // Remove is async, so wait a tick
      setTimeout(() => {
        initializeEditor(this.props.id, initArgs);
      }, 30);
      return;
    }

    initializeEditor(this.props.id, initArgs);
  };

  ensureTextareaVisible = () => {
    if (!this.textarea) {
      return;
    }
    setTimeout(() => {
      // Make sure our textarea is always visible when in text mode.
      // For some reason aria labels always update, but styles don't
      const hidden = this.textarea.getAttribute('aria-hidden') === 'true';
      this.textarea.style.display = hidden ? 'none' : '';
    }, 0);
  };

  handleTextModeClick = () => {
    this.ensureTextareaVisible();
  };

  handleChange = () => {
    if (!this.editor) {
      return;
    }

    // Using editor.save() will give us back the current value, regardless of visual/text mode
    const value = this.editor.save();
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(value);
    }
  };

  render() {
    return (
      <div className={classnames(this.props.className, 'meta-wp-editor')}>
        <div className="meta-deprecated">
          <strong>WP Editor</strong> has been deprecated in favour of <strong>Text Editor</strong>{' '}
          and is no longer supported.
        </div>
        {this.props.label && <label className="meta-wp-editor__label">{this.props.label}</label>}
        <div
          className="wp-core-ui wp-editor-wrap tmce-active"
          id={`wp-${this.props.id}-wrap`}
          ref={(c) => {
            this.wrapEl = c;
          }}
        >
          <div className="wp-media-buttons">
            <WpButton type="button" className="insert-media add_media">
              <span className="wp-media-buttons-icon" /> Add Media
            </WpButton>
          </div>
          <div className="wp-editor-tabs">
            <button
              type="button"
              data-wp-editor-id={this.props.id}
              id={`${this.props.id}-tmce`}
              className="wp-switch-editor switch-tmce"
            >
              Visual
            </button>
            <button
              type="button"
              data-wp-editor-id={this.props.id}
              id={`${this.props.id}-tmce`}
              className="wp-switch-editor switch-html"
              onClick={this.handleTextModeClick}
            >
              Text
            </button>
          </div>
          <div className="wp-editor-container">
            <TextareaInput
              getInputRef={this.getInputRef}
              id={this.props.id}
              name={this.props.metaRef}
              onChange={this.handleChange}
              style={{ visibility: 'hidden' }}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default WpEditor;
