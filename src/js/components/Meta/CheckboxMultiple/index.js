import { h, Component } from 'preact';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import shallowEqual from 'shallowequal';

import CheckboxInput from '../../CheckboxInput';
import SaveInput from '../../SaveInput';
import withOptions from '../../WithOptions/hoc';

/**
 * Multiple checkbox options, similar to WP taxonomies.
 *
 * Consider using `select-multiple` if you need to limit the number of options.
 *
 * Returns an array of selected values.
 *
 * ## Value
 * **Returns** `(string|number)[]`
 * ```php
 * $value = json_decode(fx_get_meta('id'), true);
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'checkbox-multiple',
 *   'className' => 'my-class-name',
 *   'label' => 'Posts',
 *   'options' => 'post',
 *   'showToggleAll' => true,
 *   'taxonomyLayout' => true,
 * ],
 * ```
 */
class CheckboxMultiple extends Component {
  static displayName = 'CheckboxMultiple';

  static propTypes = {
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    id: PropTypes.string.isRequired,
    /**
     * A list of ids to exlude from the options list
     */
    exclude: PropTypes.array,
    /**
     * Tab label used when in WP taxonomy layout
     */
    label: PropTypes.string,
    metaRef: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    /**
     * Options to choose from. See [options](options.md) for more information.
     */
    options: PropTypes.array.isRequired,
    /**
     * Show a toggle all button
     */
    showToggleAll: PropTypes.bool,
    /**
     * Present the meta box as a WP taxonomy
     */
    taxonomyLayout: PropTypes.bool,
    /**
     * Metabox title
     */
    title: PropTypes.string,
    value: PropTypes.string.isRequired,
  };

  static defaultProps = {
    showToggleAll: true,
    taxonomyLayout: true,
  };

  constructor(props) {
    super(props);

    this.state = {
      selected: Array.isArray(props.value) ? props.value : [],
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (!shallowEqual(prevState.selected, this.state.selected)) {
      if (typeof this.props.onChange === 'function') {
        this.props.onChange(this.state.selected);
      }
    }
  }

  handleInputChange = (event) => {
    const { checked, value } = event.target;

    if (checked) {
      this.setState((prevState) => ({
        selected: prevState.selected.concat(value),
      }));
      return;
    }

    this.setState((prevState) => ({
      selected: prevState.selected.filter((a) => a !== value),
    }));
  };

  handleToggleAll = () => {
    if (this.state.selected.length === this.props.options.length) {
      this.setState({
        selected: [],
      });
      return;
    }

    const selected = this.props.options.map((option) => `${option.id || option}`);
    this.setState({
      selected,
    });
  };

  optionIsSelected(value) {
    // Option values need to be saved as a string to make strict comparison work
    return this.state.selected.indexOf(`${value}`) > -1;
  }

  render() {
    const classes = classnames('checkbox_multiple', this.props.className, {
      taxonomydiv: this.props.taxonomyLayout,
    });

    const panelClasses = classnames({
      'tabs-panel': this.props.taxonomyLayout,
    });

    return (
      <div className={classes}>
        {this.props.taxonomyLayout && (
          <ul className="category-tabs">
            <li className="tabs">{this.props.label}</li>
          </ul>
        )}
        <div className={panelClasses}>
          <ul className="checkbox_multiple__list">
            {this.props.options &&
              this.props.options.map((option) => {
                const title = option.title || option;
                const value = option.id || option;
                const id = `${this.props.metaRef}-${value}`;
                const checked = this.optionIsSelected(value);

                return (
                  <li key={`${title}-${value}`}>
                    <CheckboxInput
                      checked={checked}
                      className="meta-radio"
                      id={id}
                      name={id}
                      onChange={this.handleInputChange}
                      value={value}
                    />
                    <label htmlFor={id}>{title}</label>
                  </li>
                );
              })}
          </ul>
        </div>
        {this.props.showToggleAll && (
          <button
            type="button"
            onClick={this.handleToggleAll}
            className="checkbox_multiple__toggle"
          >
            Toggle All
          </button>
        )}
        <SaveInput name={this.props.metaRef} value={this.state.selected} />
      </div>
    );
  }
}

export default withOptions(CheckboxMultiple);
