import { h, Component } from 'preact';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import InlineLabel from '../../InlineLabel';
import Input from '../../Input';

/**
 * Read-only data display field.
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'datafield',
 *   'className' => 'my-class-name',
 *   'label' => 'Your data',
 *   'value' => 'Read-only value',
 *   'title' => 'Description',
 * ],
 * ```
 */
class Datafield extends Component {
  static displayName = 'Datafield';

  static propTypes = {
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    id: PropTypes.string.isRequired,
    /**
     * Optional label that appears next to the data
     */
    label: PropTypes.string,
    metaRef: PropTypes.string.isRequired,
    /**
     * The value to display
     */
    value: PropTypes.string.isRequired,
    /**
     * Metabox title
     */
    title: PropTypes.string,
  };

  render() {
    const valueAsString =
      typeof this.props.value === 'object'
        ? JSON.stringify(this.props.value, null, 2) // Pretty-printing the object for readability
        : this.props.value; // Fallback for non-object values

    return (
      <div className={classnames(this.props.className, 'meta-datafield')}>
        {this.props.label && (
          <InlineLabel className="meta-datafield__label">{this.props.label}</InlineLabel>
        )}
        <Input
          className="meta-textfield__input"
          id={this.props.id}
          name={this.props.metaRef}
          onChange={this.handleChange}
          placeholder={this.props.placeholder}
          readOnly
          type="hidden"
          value={this.props.value}
        />
        <div
          className="meta-datafield__value"
          id={this.props.id}
          title={this.props.title}
          aria-readonly="true"
          style={{ whiteSpace: 'pre-wrap' }} // Ensures the newlines are preserved
        >
          {valueAsString}
        </div>
      </div>
    );
  }
}

export default Datafield;
