import { h, Component } from 'preact';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import omit from 'lodash.omit';
import shallowEqual from 'shallowequal';

import ButtonIncognito from '../../ButtonIncognito';
import Icon from '../../Icon';
import Input from '../../Input';
import SaveInput from '../../SaveInput';
import TextareaInput from '../../TextareaInput';
import WpButton from '../../WpButton';

/**
 * Store mutliple key-value string pairs.
 *
 * ## Value
 * **Returns** `{ title: string, value: string }[]`
 * ```php
 * $pairs = json_decode(fx_get_meta('id'), true);
 * foreach($pairs as $pair) {
 *   $title = $pair['title'];
 *   $value = $pair['value'];
 * }
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'input-pairs',
 *   'buttonText' => 'Add',
 *   'className' => 'my-class-name',
 *   'limit' => 4,
 *   'listView' => true,
 *   'titleLabel' => 'Title',
 *   'valueInput' => 'textfield',
 *   'valueLabel' => 'Value',
 * ],
 * ```
 */
export default class InputPairs extends Component {
  static displayName = 'InputPairs';

  static propTypes = {
    /**
     * Alternate text for the add button
     */
    buttonText: PropTypes.string,
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    id: PropTypes.string.isRequired,
    /**
     * Limit the number of inputs allowed
     */
    limit: PropTypes.number,
    /**
     * Show the inputs in a list view
     */
    listView: PropTypes.bool,
    metaRef: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    /**
     * Metabox title
     */
    title: PropTypes.string,
    /**
     * Label for the title input
     */
    titleLabel: PropTypes.string,
    value: PropTypes.string.isRequired,
    /**
     * Label for the value input
     */
    valueLabel: PropTypes.string,
    /**
     * Input type for the value input (has limited support).
     */
    valueInput: PropTypes.oneOf(['textarea', 'textfield']),
  };

  static defaultProps = {
    buttonText: 'Add',
    titleLabel: 'Title',
    valueLabel: 'Description',
    valueInput: 'textarea',
  };

  constructor(props) {
    super(props);

    const value = {};
    const id = Date.now();
    if (Array.isArray(props.value)) {
      props.value.forEach((pair, index) => {
        value[`${id}-${index}`] = pair;
      });
    }

    this.state = {
      value,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (!shallowEqual(prevState.value, this.state.value)) {
      if (typeof this.props.onChange === 'function') {
        this.props.onChange(this.value);
      }
    }
  }

  get value() {
    // Save the pairs as an array
    return Object.keys(this.state.value).map((id) => this.state.value[id]);
  }

  handleInputChange = (event, id, key) => {
    const { value } = event.target;

    // Deep update the value in our state
    this.setState((prevState) => ({
      value: {
        ...prevState.value,
        [id]: {
          ...prevState.value[id],
          [key]: value,
        },
      },
    }));
  };

  addPair = () => {
    const id = Date.now();
    this.setState((prevState) => ({
      value: {
        ...prevState.value,
        [id]: { title: '', value: '' },
      },
    }));
  };

  removePair = (id) => {
    this.setState((prevState) => ({
      value: omit(prevState.value, id),
    }));
  };

  renderPair = (id) => {
    const pair = this.state.value[id];
    const onTitleChange = (e) => this.handleInputChange(e, id, 'title');
    const onValueChange = (e) => this.handleInputChange(e, id, 'value');
    const onRemoveClick = () => this.removePair(id);
    const ValueInputComponent = this.props.valueInput === 'textfield' ? Input : TextareaInput;

    if (this.props.listView) {
      return (
        <div key={id} className="meta-input-pairs__item meta-input-pairs__item--list">
          <div className="row">
            <div className="col-sm-4">
              <Input
                id={`item-title-${id}`}
                name={`item-title-${id}`}
                value={pair.title}
                onChange={onTitleChange}
              />
            </div>
            <div className="col-sm-6">
              <ValueInputComponent
                id={`item-value-${id}`}
                name={`item-value-${id}`}
                value={pair.value}
                onChange={onValueChange}
              />
            </div>
            <div className="col-sm-2">
              <ButtonIncognito className="meta-input-pairs__item__remove" onClick={onRemoveClick}>
                <Icon.cross size="10px" />
              </ButtonIncognito>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div key={id} className="meta-input-pairs__item">
        <div className="row">
          <div className="col-sm-6">
            <div className="meta-input-pairs__input">
              <label className="meta-input-pairs__label" htmlFor={`item-title-${id}`}>
                {this.props.titleLabel}
              </label>
              <Input
                id={`item-title-${id}`}
                name={`item-title-${id}`}
                value={pair.title}
                onChange={onTitleChange}
              />
            </div>
          </div>
          <div className="col-sm-6">
            <div className="meta-input-pairs__input">
              <label className="meta-input-pairs__label" htmlFor={`item-value-${id}`}>
                {this.props.valueLabel}
              </label>
              <ValueInputComponent
                id={`item-value-${id}`}
                name={`item-value-${id}`}
                value={pair.value}
                onChange={onValueChange}
              />
            </div>
          </div>
        </div>
        <WpButton onClick={onRemoveClick}>Remove</WpButton>
      </div>
    );
  };

  render() {
    const classes = classnames('meta-input-pairs', this.props.className, {
      'meta-input-pairs--list': this.props.listView,
    });
    const valueIds = Object.keys(this.state.value);
    const canAddMore = this.props.limit ? valueIds.length < this.props.limit : true;

    return (
      <div className={classes}>
        <div className="meta-input-pairs__preview">
          {this.props.listView && (
            <div className="row">
              <div className="col-sm-4">
                <span className="meta-input-pairs__label">{this.props.titleLabel}</span>
              </div>
              <div className="col-sm-6">
                <span className="meta-input-pairs__label">{this.props.valueLabel}</span>
              </div>
            </div>
          )}
          {valueIds.map(this.renderPair)}
        </div>
        {canAddMore && (
          <WpButton className="select-media__trigger" onClick={this.addPair} primary>
            {this.props.buttonText}
          </WpButton>
        )}
        <SaveInput name={this.props.metaRef} value={this.value} />
      </div>
    );
  }
}
