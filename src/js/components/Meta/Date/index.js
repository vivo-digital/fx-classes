import { h, Component } from 'preact';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import format from 'date-fns/format';
import parse from 'date-fns/parse';

import DatePickerInput from '../../DatePickerInput';
import Icon from '../../Icon';
import Input from '../../Input';
import SaveInput from '../../SaveInput';

// NOTE: These should never change
const DATE_GLUE = ' - ';
const SAVE_FORMAT = 'YYYYMMDD';

function parseDate(originalDate) {
  if (!originalDate) {
    return undefined;
  }
  // Ensure date is a string
  let date = `${originalDate}`;

  if (date.indexOf('-') === 2) {
    // Assume format is DD-MM-YYYY
    const [d, m, y] = `${date}`.split('-');
    return parse([y, m, d].join('-'));
  }

  date = date.replace(/-/g, '');
  const [y, md] = date.match(/.{1,4}/g);
  const [m, d] = md.match(/.{1,2}/g);

  return parse([y, m, d].join('-'));
}


/**
  * Date picker with single, range and multiple modes available.
  *
  * ## Value
  *
  * #### `mode => 'single'`
  * **Returns** `string`
  * ```php
  * $date = fx_get_meta('id'); // YYYYMMDD e.g. 19920511 -> 11 May 1992
  * ```
  *
  * #### `mode => 'range'`
  * **Returns** `string`
  * ```php
  * $date = fx_get_meta('id'); // YYYYMMDD - YYYYMMDD
  * $dates = explode(' - ', $date);
  * $min_date = $dates[0];
  * $max_date = $dates[1];
  * ```
  *
  * #### `mode => 'multiple'`
  * **Returns** `string`
  * ```php
  * $date = fx_get_meta('id'); // YYYYMMDD - YYYYMMDD - YYYYMMDD... etc
  * $dates = explode(' - ', $date);
  * ```
  *
  * ## Example
  * ```php
  * 'id' => [
  *   'type' => 'date',
  *   'align' => 'right',
  *   'minDate' => '11/05/1992',
  *   'maxDate' => '30/12/2020',
  *   'displayFormat' => 'dddd Do MMM YYYY',
  *   'mode' => 'single',
  *   'placeholder' => 'Please select a date',
  *   'title' => 'Your date',
  *   'className' => 'my-class-name',
  * ];
  * ```
  */
class DatePicker extends Component {
  static displayName = 'Date';

  static propTypes = {
    /**
     * Dropdown horizontal alignment
     */
    align: PropTypes.oneOf(['left', 'right', 'center']),
    /**
     * Metabox class name
     */
    className: PropTypes.bool,
    /**
     * Date display format (form date-fns)
     */
    displayFormat: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    /**
     * Latest date to show (DD/MM/YYYY)
     */
    maxDate: PropTypes.string,
    metaRef: PropTypes.string.isRequired,
    /**
    * Earliest date to show (DD/MM/YYYY)
    */
    minDate: PropTypes.string,
    /**
     * Minimum width of the dropdown
     */
    minWidth: PropTypes.string,
    /**
     * Datepicker input mode
     */
    mode: PropTypes.oneOf(['single', 'range', 'multiple']).isRequired,
    onChange: PropTypes.func,
    /**
     * Placeholder text for the input
     */
    placeholder: PropTypes.string,
    /**
    * Metabox title
    */
    title: PropTypes.string,
    value: PropTypes.string.isRequired,
  };

  static defaultProps = {
    align: 'right',
    displayFormat: 'dddd Do MMM YYYY',
    minWidth: '200px',
    mode: 'single',
  };

  constructor(props) {
    super(props);

    this.state = {
      active: false,
      value: this.parseDateString(props.value, props.mode),
    };
  }

  // eslint-disable-next-line react/sort-comp
  node;

  formatValue(value, displayFormat) {
    if (!value || typeof value !== 'object') {
      return '';
    }

    if (Array.isArray(value)) {
      return value.map(v => format(v, displayFormat)).join(DATE_GLUE);
    }

    return format(value, displayFormat);
  }

  parseDateString(value, mode) {
    if (mode === 'single') {
      return parseDate(value);
    }

    return value.split(DATE_GLUE).map(date => parseDate(date));
  }

  handleWindowClick = e => {
    if (!this.node || this.node.contains(e.target)) {
      return;
    }

    this.hide();
  };

  handleInputChange = e => {
    e.preventDefault();
  };

  handleChange = value => {
    this.setState({
      value,
    });

    if (typeof this.props.onChange === 'function') {
      this.props.onChange(this.formatValue(value, SAVE_FORMAT));
    }
  };

  show = () => {
    window.addEventListener('click', this.handleWindowClick);
    window.addEventListener('touchstart', this.handleWindowClick);

    this.setState({
      active: true,
    });
  };

  hide = () => {
    window.removeEventListener('click', this.handleWindowClick);
    window.removeEventListener('touchstart', this.handleWindowClick);

    this.setState({
      active: false,
    });
  };

  toggle = () => {
    if (this.state.active) {
      this.hide();
      return;
    }

    this.show();
  };

  render() {
    const {
      align,
      id,
      minDate,
      maxDate,
      mode,
      minWidth,
      metaRef,
      placeholder,
    } = this.props;

    return (
      <div
        className={classnames('meta-date__input-wrap', this.props.className)}
        ref={c => {
          this.node = c;
        }}
      >
        <Input
          id={id}
          name={id}
          onClick={this.show}
          onChange={this.handleInputChange}
          readOnly
          placeholder={placeholder}
          type="text"
          value={this.formatValue(this.state.value, this.props.displayFormat)}
        />
        <div className="meta-date__icon-wrap">
          <Icon.calendar size="14px" color="currentColor" />
        </div>
        <div
          className={classnames('meta-date__dropdown', {
            'meta-date__dropdown--active': this.state.active,
            [`meta-date__dropdown--${align}`]: align,
          })}
          style={{ minWidth }}
        >
          <DatePickerInput
            minDate={parseDate(minDate)}
            maxDate={parseDate(maxDate)}
            mode={mode}
            onDateSelected={this.handleChange}
            value={this.state.value}
          />
        </div>
        <SaveInput name={metaRef} value={this.formatValue(this.state.value, SAVE_FORMAT)} />
      </div>
    );
  }
}

export default DatePicker;
