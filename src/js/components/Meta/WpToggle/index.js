import { h, Component } from 'preact';

import SaveInput from '../../SaveInput';

import PropTypes from 'prop-types';
import classnames from 'classnames';

/**
 * WP ToggleControl
 * ToggleControl is used to generate a toggle user interface.
 * [ToggleControl Docs](https://developer.wordpress.org/block-editor/reference-guides/components/toggle-control/)
 *
 * ## Value
 * **Returns** `bool`
 * ```php
 * $value = fx_get_meta('id');
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'wp-toggle',
 *   'className' => 'my-class-name',
 *   'defaultChecked' => false,
 *   'label' => 'Enter something below',
 * ],
 * ```
 */
class WpToggle extends Component {
  static displayName = 'WpToggle';

  static propTypes = {
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    /**
     * Input is checked by default
     */
    defaultChecked: PropTypes.bool,
    id: PropTypes.string.isRequired,
    /**
     * Label that appears next to the input
     */
    label: PropTypes.string,
    metaRef: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    /**
     * Metabox title
     */
    value: PropTypes.string.isRequired,
  };

  static defaultProps = {
    defaultChecked: false,
  };

  constructor(props) {
    super(props);
    const value = typeof props.value !== 'undefined' ? !!props.value : props.defaultChecked;
    this.state = {
      value,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.renderWpToggle();
    }, 30);
  }

  componentDidUpdate() {
    this.renderWpToggle();
  }

  componentWillUnmount() {
    try {
      wp.element.unmountComponentAtNode(this.wrapEl);
    } catch (err) {
      // don't worry about this for now
    }
  }

  handleToggleChange = (newValue) => {
    this.setState({
      value: newValue,
    });
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(newValue);
    }
  };

  renderWpToggle = () => {
    wp.element.render(
      wp.element.createElement(wp.components.ToggleControl, {
        checked: this.state.value,
        onChange: this.handleToggleChange,
        label: this.props.label,
      }),
      document.getElementById(`wp-${this.props.id}`),
    );
  };

  render() {
    return (
      <div className={classnames(this.props.className, 'meta-wp-toggle')}>
        <div id={`wp-${this.props.id}`} />
        <SaveInput name={this.props.metaRef} value={this.state.value} />
      </div>
    );
  }
}

export default WpToggle;
