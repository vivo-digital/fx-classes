import { h, Component } from 'preact';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import InlineLabel from '../../InlineLabel';
import Input from '../../Input';

/**
 * Simple textfield input.
 *
 * ## Value
 * **Returns** `string`
 * ```php
 * $value = fx_get_meta('id');
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'textfield',
 *   'className' => 'my-class-name',
 *   'label' => 'Enter something below',
 *   'placeholder' => '...',
 *   'readOnly' => false,
 *   'title' => 'Description',
 * ],
 * ```
 */
class Textfield extends Component {
  static displayName = 'Textfield';

  static propTypes = {
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    id: PropTypes.string.isRequired,
    /**
     * Optional label that appears next to the input, rather than ontop
     */
    label: PropTypes.string,
    metaRef: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    /**
     * Placeholder text for the input
     */
    placeholder: PropTypes.string,
    /**
     * Disable editing of the input
     */
    readOnly: PropTypes.bool,
    /**
     * Type of input, e.g. text, number, email
     */
    inputType: PropTypes.string.isRequired,
    /**
     * Metabox title
     */
    title: PropTypes.string,
    value: PropTypes.string.isRequired,
  };

  static defaultProps = {
    readOnly: false,
    inputType: 'text',
  };

  handleChange = (e) => {
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(e.target.value);
    }
  };

  render() {
    return (
      <div className={classnames(this.props.className, 'meta-textfield')}>
        {this.props.label && (
          <InlineLabel className="meta-textfield__label">{this.props.label}</InlineLabel>
        )}
        <Input
          className="meta-textfield__input"
          id={this.props.id}
          name={this.props.metaRef}
          onChange={this.handleChange}
          placeholder={this.props.placeholder}
          readOnly={this.props.readOnly}
          type={this.props.inputType}
          value={this.props.value}
        />
      </div>
    );
  }
}

export default Textfield;
