import { h, Component } from 'preact';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import ButtonIncognito from '../../ButtonIncognito';
import Input from '../../Input';
import Loading from '../../Loading';
import WpButton from '../../WpButton';
import SaveInput from '../../SaveInput';

import getLocation from '../../../utils/getLocation';
import withPromiseHandler from '../../../utils/withPromiseHandler';

/**
 * Get the latitude and longitude for a given address.
 *
 * ## Value
 * **Returns** `{ address: string, lat: number, lng: number }`
 * ```php
 * $location = json_decode(fx_get_meta('id'), true);
 * $location_address = $location['address'];
 * $location_lat = $location['lat'];
 * $location_lng = $location['lng'];
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'location',
 *   'buttonText' => 'Enter your address',
 *   'className' => 'my-class-name',
 * ],
 * ```
 */
class Location extends Component {
  static displayName = 'Location';

  static propTypes = {
    /**
     * Alternate text for the enter location button
     */
    buttonText: PropTypes.string.isRequired,
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    id: PropTypes.string.isRequired,
    metaRef: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    /**
     * Metabox title
     */
    title: PropTypes.string,
    value: PropTypes.string.isRequired,
  };

  static defaultProps = {
    buttonText: 'Enter location',
  };

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      value: props.value,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.input && !prevState.open && this.state.open) {
      this.input.focus();
    }
  }

  open = () => {
    this.setState({
      open: true,
    });
  };

  close = () => {
    this.setState({
      open: false,
    });
  };

  remove = () => {
    this.setState({
      value: '',
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();

    this.props.getLocation.run(this.input.value).then((location) => {
      if (location) {
        const value = {
          address: location.formatted_address,
          lat: location.geometry.location.lat,
          lng: location.geometry.location.lng,
        };

        this.setState({
          open: false,
          value,
        });

        if (typeof this.props.onChange === 'function') {
          this.props.onChange(value);
        }
      }
    });
  };

  render() {
    const hasValue = Boolean(this.state.value);
    const hasError = typeof this.props.getLocation.error !== 'undefined';

    return (
      <div className={classnames(this.props.className, 'meta-location')}>
        {hasValue && (
          <div className="meta-location__value">
            <div className="meta-location__value__address">
              <strong>Address:</strong>
              {this.state.value.address}
            </div>
            <div className="meta-location__value__lat">
              <strong>Latitude:</strong>
              {this.state.value.lat}
            </div>
            <div className="meta-location__value__lng">
              <strong>Longitude:</strong>
              {this.state.value.lng}
            </div>
          </div>
        )}
        {this.state.open ? (
          <form className="meta-location__form" onSubmit={this.handleSubmit}>
            <div className="meta-location__input">
              <Input
                getInputRef={(c) => {
                  this.input = c;
                }}
              />
              <WpButton
                className="meta-location__submit"
                disabled={this.props.getLocation.loading}
                type="submit"
                primary
              >
                Submit
              </WpButton>
              {this.props.getLocation.loading && <Loading />}
            </div>
            {hasError && (
              <div className="meta-location__error">
                {this.props.getLocation.error.message || 'Something went wrong.'}
              </div>
            )}
            <ButtonIncognito className="fx-text-button meta-location__close" onClick={this.close}>
              Cancel
            </ButtonIncognito>
          </form>
        ) : hasValue ? (
          <div>
            <ButtonIncognito className="fx-text-button" onClick={this.open}>
              Change
            </ButtonIncognito>
            <ButtonIncognito className="fx-text-button" onClick={this.remove}>
              Remove
            </ButtonIncognito>
          </div>
        ) : (
          <WpButton onClick={this.open}>{this.props.buttonText}</WpButton>
        )}
        <SaveInput name={this.props.metaRef} value={this.state.value} />
      </div>
    );
  }
}

const WrappedLocation = withPromiseHandler({ getLocation })(Location);

WrappedLocation.displayName = Location.displayName;

export default WrappedLocation;
