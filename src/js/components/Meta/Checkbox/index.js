import { h, Component } from 'preact';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import CheckboxInput from '../../CheckboxInput';

/**
 * Simple checkbox input.
 *
 * ## Value
 * **Returns** `bool`
 * ```php
 * $value = fx_get_meta('id');
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'checkbox',
 *   'className' => 'my-class-name',
 *   'defaultChecked' => false,
 *   'label' => 'Enter something below',
 * ],
 * ```
 */
class Checkbox extends Component {
  static displayName = 'Checkbox';

  static propTypes = {
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    /**
     * Input is checked by default
     */
    defaultChecked: PropTypes.bool,
    id: PropTypes.string.isRequired,
    /**
     * Label that appears next to the input
     */
    label: PropTypes.string,
    metaRef: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    /**
     * Metabox title
     */
    title: PropTypes.string,
    value: PropTypes.string.isRequired,
  };

  static defaultProps = {
    defaultChecked: false,
  };

  handleChange = (e) => {
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(e.target.checked);
    }
  };

  render() {
    return (
      <div className={classnames(this.props.className, 'meta-checkbox')}>
        <CheckboxInput
          checked={this.props.value}
          className="meta-checkbox__input"
          defaultChecked={this.props.defaultChecked}
          id={this.props.id}
          name={this.props.metaRef}
          onChange={this.handleChange}
          placeholder={this.props.placeholder}
        />
        {this.props.label && (
          <label className="meta-checkbox__label" htmlFor={this.props.id}>
            {this.props.label}
          </label>
        )}
      </div>
    );
  }
}

export default Checkbox;
