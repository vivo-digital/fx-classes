import { h, Component } from 'preact';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import shallowEqual from 'shallowequal';
import omit from 'lodash.omit';
import cloneDeep from 'lodash.clonedeep';

import InlineLabel from '../../InlineLabel';
import MetaInputs from '../../MetaInputs';
import SelectInput from '../../SelectInput';
import SaveInput from '../../SaveInput';

const TYPE_KEY = '_type_';

function getDefaultValueFromInputs(inputs) {
  const value = {};
  Object.keys(inputs).forEach((id) => {
    const input = inputs[id];
    const inputValue = typeof input.value !== 'undefined' ? input.value : undefined;
    const inputDefaultValue = typeof input.default !== 'undefined' ? input.default : undefined;
    const valueToUse = inputValue || inputDefaultValue;
    if (typeof valueToUse !== 'undefined') {
      value[id] = valueToUse;
    }
  });
  return value;
}

/**
 * A dynamic meta type which lets you define multiple groups of meta where a user can select from
 * a dropdown and fill out the necessary fields.
 * A switcher takes all normal props, as well as `inputs`. Switchers can be nested.
 *
 * ## Value
 * **Returns** `any[][]`
 * ```php
 * $value = json_decode(fx_get_meta('id'), true);
 * switch ($value['_type_']) {
 *   case 'Banner':
 *     $banner_title = $value['title'];
 *     $banner_description = $value['description'];
 *     break;
 *   case 'Post':
 *     $feature_post_id = $value['feature_post'];
 *     break;
 *   default:
 *     break;
 * }
 *
 * $title = $values['title'];
 * $description = $values['description'];
 * $image = $values['image'];
 * ```
 *
 * ## Example
 * ```php
 * $banner_inputs = [
 *   'title' => [
 *     'type' => 'textfield',
 *     'title' => 'Title',
 *   ],
 *  'description' => [
 *     'type' => 'wp-editor',
 *     'title' => 'Description',
 *   ],
 * ];
 *
 * $post_inputs = [
 *   'feature_post' => [
 *     'type' => 'select',
 *     'title' => 'Featured Post',
 *     'options' => 'post',
 *   ],
 * ];
 *
 * 'id' => [
 *   'type' => 'switcher',
 *   'className' => 'my-class-name',
 *   'inputs' => [
 *     'Banner' => $banner_inputs,
 *     'Post' => $post_inputs,
 *   ],
 * ],
 * ```
 */
export default class Switcher extends Component {
  static displayName = 'Switcher';

  static propTypes = {
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    id: PropTypes.string.isRequired,
    /**
     * Meta inputs to render
     */
    inputs: PropTypes.object.isRequired,
    /**
     * Type select input label
     */
    label: PropTypes.string,
    metaRef: PropTypes.string.isRequired,
    /**
     * Metabox title
     */
    title: PropTypes.string,
    value: PropTypes.string.isRequired,
  };

  static defaultProps = {
    inputs: {},
  };

  constructor(props) {
    super(props);

    const defaultValue = cloneDeep(props.value) || {};
    const value = omit(defaultValue, TYPE_KEY);

    this.state = {
      type: defaultValue[TYPE_KEY],
      value,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (!shallowEqual(prevState.value, this.state.value)) {
      if (typeof this.props.onChange === 'function') {
        this.props.onChange(this.value);
      }
    }
  }

  get value() {
    // Attach the input type to the stored value
    return {
      ...this.state.value,
      [TYPE_KEY]: this.state.type,
    };
  }

  get types() {
    return Object.keys(this.props.inputs);
  }

  get inputs() {
    if (this.state.type) {
      return this.props.inputs[this.state.type];
    }

    return undefined;
  }

  handleTypeChange = (type) => {
    if (type !== this.state.type) {
      this.setState({
        type,
        value: getDefaultValueFromInputs(this.props.inputs[type]),
      });
    }
  };

  handleInputChange = (value) => {
    this.setState({
      value,
    });
  };

  render() {
    const classes = classnames('meta-switcher', this.props.className);
    const inputs = this.inputs;

    return (
      <div className={classes}>
        <div className="meta-switcher__select">
          {this.props.label && (
            <InlineLabel className="meta-switcher__select__label">{this.props.label}</InlineLabel>
          )}
          <SelectInput
            wrapperClassName="meta-switcher__select__input"
            defaultValue={this.state.type}
            options={this.types}
            onChange={this.handleTypeChange}
          />
        </div>
        <div className="meta-switcher__inputs">
          {inputs && (
            <MetaInputs
              id={this.props.id}
              inputs={inputs}
              onChange={this.handleInputChange}
              value={this.state.value}
            />
          )}
        </div>
        <SaveInput name={this.props.metaRef} value={this.value} />
      </div>
    );
  }
}
