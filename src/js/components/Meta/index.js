import kebabCase from 'lodash.kebabcase';

import Checkbox from './Checkbox';
import CheckboxMultiple from './CheckboxMultiple';
import Color from './Color';
import DatePicker from './Date';
import Datafield from './Datafield';
import Group from './Group';
import Icon from './Icon';
import InputPairs from './InputPairs';
import RichTextMedia from './RichTextMedia';
import Media from './Media';
import Section from './Section';
import Select from './Select';
import Link from './Link';
import Location from './Location';
import SelectMultiple from './SelectMultiple';
import SelectOrder from './SelectOrder';
import Switcher from './Switcher';
import Tabs from './Tabs';
import Textarea from './Textarea';
import TextEditor from './TextEditor';
import Textfield from './Textfield';
import WpToggle from './WpToggle';
import WpRichText from './WpRichText';
import WpEditor from './WpEditor';

import { getDisplayName } from '../../utils/helpers';

const metaComponents = [
  Checkbox,
  CheckboxMultiple,
  Color,
  DatePicker,
  Datafield,
  Icon,
  InputPairs,
  RichTextMedia,
  Media,
  Section,
  Select,
  Group,
  Link,
  Location,
  SelectMultiple,
  SelectOrder,
  Tabs,
  Switcher,
  Textarea,
  TextEditor,
  Textfield,
  WpToggle,
  WpRichText,
  WpEditor,
];
const inputMap = {};

metaComponents.forEach((Component) => {
  const key = kebabCase(getDisplayName(Component));
  inputMap[key] = Component;
});

export { metaComponents, inputMap };
