import { h, Component } from 'preact';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import shallowEqual from 'shallowequal';

import SelectInput from '../../SelectInput';
import SaveInput from '../../SaveInput';
import withOptions from '../../WithOptions/hoc';

/**
 * Multiple select dropdowns. Takes all props `select` accepts.
 *
 * Consider using `checkbox-multiple` or `select` with `multiple => true` if you don't need to
 * limit the number of options.
 *
 * Returns an array of selected values.
 *
 * ## Value
 * **Returns** `(string|number)[]`
 * ```php
 * $value = json_decode(fx_get_meta('id'), true);
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'select',
 *   'className' => 'my-class-name',
 *   'creatable' => false,
 *   'excludePostType' => false,
 *   'multiple' => true,
 *   'nOptions' => 4,
 *   'options' => 'page, post',
 *   'placeholder' => 'Pick some content',
 * ],
 * ```
 */
class SelectMultiple extends Component {
  static displayName = 'SelectMultiple';

  static propTypes = {
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    /**
     * Allow new options to be created
     */
    creatable: PropTypes.bool,
    /**
     * A list of ids to exlude from the options list
     */
    exclude: PropTypes.array,
    /**
     * Hide the post type if the option is a post
     */
    excludePostType: PropTypes.bool,
    id: PropTypes.string.isRequired,
    /**
     * Allow multiple selections
     */
    multiple: PropTypes.bool,
    metaRef: PropTypes.string.isRequired,
    /**
     * The number of dropdowns
     */
    nOptions: PropTypes.number.isRequired,
    /**
     * Options to choose from. See [options](options.md) for more information.
     */
    options: PropTypes.array.isRequired,
    /**
     * Placeholder text for the input
     */
    placeholder: PropTypes.string,
    /**
     * Metabox title
     */
    title: PropTypes.string,
    value: PropTypes.string.isRequired,
  };

  static defaultProps = {
    creatable: false,
    excludePostType: false,
    multiple: false,
    nOptions: 1,
    placeholder: 'Please select...',
  };

  constructor(props) {
    super(props);

    this.state = {
      value: Array.isArray(props.value) ? props.value : [],
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (!shallowEqual(prevState.value, this.state.value)) {
      if (typeof this.props.onChange === 'function') {
        this.props.onChange(this.state.value);
      }
    }
  }

  handleInputChange = (value, index) => {
    this.setState((prevState) => {
      const newValue = [...prevState.value];
      newValue[index] = value;
      return {
        value: newValue,
      };
    });
  };

  render() {
    const classes = classnames('meta-select-multiple', this.props.className);

    /* eslint-disable react/no-array-index-key */
    return (
      <div className={classes}>
        {Array.from({ length: this.props.nOptions }).map((a, index) => (
          <SelectInput
            className="meta-select-multiple__select"
            creatable={this.props.creatable}
            defaultValue={this.state.value[index]}
            excludePostType={this.props.excludePostType}
            id={`${index}-${this.props.id}`}
            isMulti={this.props.multiple}
            key={`${index}-${this.props.id}`}
            name={`${index}-${this.props.metaRef}`}
            onChange={(value) => this.handleInputChange(value, index)}
            options={this.props.options}
            placeholder={this.props.placeholder}
          />
        ))}
        <SaveInput name={this.props.metaRef} value={this.state.value} />
      </div>
    );
  }
}

export default withOptions(SelectMultiple);
