import { h } from 'preact';
import { SortableContainer } from 'react-sortable-hoc';
import classnames from 'classnames';

const SortableSectionList = props => {
  const Element = props.tableView ? 'tbody' : 'div';
  return (
    <Element {...props} className={classnames('meta-section__sortable-list', props.className)}>
      {props.children}
    </Element>
  );
};

export default SortableContainer(SortableSectionList);
