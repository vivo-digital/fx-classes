import { h } from 'preact';

import ButtonIncognito from '../../ButtonIncognito';
import Icon from '../../Icon';

export default props => (
  <div className="meta-section__buttons">
    {props.sortable && [
      <ButtonIncognito
        onClick={() => props.moveSection(props.id, 'up')}
        className="meta-section__button meta-section__up"
      >
        <Icon.chevron size="10px" />
      </ButtonIncognito>,
      <ButtonIncognito
        onClick={() => props.moveSection(props.id, 'down')}
        className="meta-section__button meta-section__down"
      >
        <Icon.chevron size="10px" />
      </ButtonIncognito>,
    ]}
    <ButtonIncognito
      className="meta-section__button meta-section__remove"
      onClick={() => props.removeSection(props.id)}
    >
      <Icon.cross size="10px" />
    </ButtonIncognito>
  </div>
);
