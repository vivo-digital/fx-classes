import { h } from 'preact';
import { SortableElement } from 'react-sortable-hoc';
import classnames from 'classnames';

const SortableSection = props => {
  const className = classnames('meta-section__sortable-item', props.className);

  //  Always pass 'native dom element'
  // https://github.com/clauderic/react-sortable-hoc/issues/305#issuecomment-405280374
  if (props.tableView) {
    return (
      <tr {...props} className={className}>
        {props.children}
      </tr>
    );
  }

  return (
    <div {...props} className={className}>
      {props.children}
    </div>
  );
};

export default SortableElement(SortableSection);
