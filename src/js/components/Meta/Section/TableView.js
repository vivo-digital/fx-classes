import { h, Component } from 'preact';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import get from 'lodash.get';

import SectionButtons from './SectionButtons';
import SortableSection from './SortableSection';
import SortableSectionList from './SortableSectionList';
import DragHandle from '../../DragHandle';
import MetaInputs from '../../MetaInputs';
import Modal from '../../Modal';
import WpButton from '../../WpButton';

const ACTIONS = {
  EDIT: 'EDIT',
};

class TableView extends Component {
  static displayName = 'Section';

  static propTypes = {
    addSection: PropTypes.func.isRequired,
    buttonText: PropTypes.string,
    canAddMore: PropTypes.bool,
    handleInputChange: PropTypes.func.isRequired,
    inputs: PropTypes.object.isRequired,
    moveSection: PropTypes.func.isRequired,
    removeSection: PropTypes.func.isRequired,
    useAsTitle: PropTypes.string,
    useModal: PropTypes.bool,
    value: PropTypes.object.isRequired,
    valueIds: PropTypes.array.isRequired,
  };

  state = {
    action: undefined,
    id: undefined,
  };

  handleEdit = (id) => {
    this.setState({
      action: ACTIONS.EDIT,
      id,
    });
  };

  closeActionModal = () => {
    this.setState({
      action: undefined,
      id: undefined,
    });
  };

  renderActionModal() {
    switch (this.state.action) {
      case ACTIONS.EDIT:
        return (
          <Modal active close={this.closeActionModal} width="600px" height="500px">
            <div className="meta-section__inputs-modal">
              <MetaInputs
                id={this.state.id}
                inputs={this.props.inputs}
                onChange={(values) => this.props.handleInputChange(this.state.id, values)}
                value={this.props.value[this.state.id]}
              />
              <div className="meta-section__inputs-modal__footer">
                <WpButton onClick={this.closeActionModal}>Close</WpButton>
              </div>
            </div>
          </Modal>
        );
      default:
        return null;
    }
  }

  renderHeaderRow() {
    if (this.props.useModal) {
      return null;
    }

    const cols = Object.keys(this.props.inputs);
    const classes = classnames('meta-section__table__row', 'meta-section__table__row--header');

    return (
      <thead className={classes}>
        {cols.map((id) => (
          <th
            className={classnames('meta-section__table__col', `meta-section__table__${id}`)}
            key={id}
          >
            {this.props.inputs[id].title}
          </th>
        ))}
        <th />
      </thead>
    );
  }

  renderRow = (id, index) => {
    const value = this.props.value[id];

    if (this.props.useModal) {
      const noun = this.props.noun ? `${this.props.noun} ` : '';
      let title = `${noun}#${index + 1}`;

      if (this.props.useAsTitle && get(value, this.props.useAsTitle)) {
        title = get(value, this.props.useAsTitle);
      }

      return (
        <SortableSection className="meta-section__table__row" key={id} index={index} tableView>
          <td className="meta-section__table__column meta-section__table__column--grow">
            <strong className="meta-section__table__title">{title}</strong>
            <WpButton
              className="meta-section__table__edit-btn"
              onClick={() => this.handleEdit(id, index)}
            >
              Edit
            </WpButton>
          </td>
          <td className="meta-section__table__column meta-section__table__column--small">
            <SectionButtons
              id={id}
              moveSection={this.props.moveSection}
              removeSection={this.props.removeSection}
              sortable={this.props.sortable}
            />
          </td>
          {this.props.sortable && (
            <td className="meta-section__table__column meta-section__table__column--small">
              <DragHandle className="meta-section__drag-handle" />
            </td>
          )}
        </SortableSection>
      );
    }

    return (
      <MetaInputs
        id={id}
        inputs={this.props.inputs}
        onChange={(values) => this.props.handleInputChange(id, values)}
        value={value}
      >
        {({ inputs, inputMap, getInputProps }) => (
          <SortableSection className="meta-section__table__row" key={id} index={index} tableView>
            {inputs.map((input) => {
              const MetaComponent = inputMap[input.type];
              if (MetaComponent) {
                return (
                  <td
                    className={classnames(
                      'meta-section__table__column',
                      `meta-section__table__${input.originalId}`,
                    )}
                    key={input.id}
                  >
                    <MetaComponent {...getInputProps(input)} />
                  </td>
                );
              }

              return null;
            })}
            <td className="meta-section__table__column meta-section__table__column--small">
              <SectionButtons
                id={id}
                moveSection={this.props.moveSection}
                removeSection={this.props.removeSection}
                sortable={this.props.sortable}
              />
            </td>
            {this.props.sortable && (
              <td className="meta-section__table__column meta-section__table__column--small">
                <DragHandle className="meta-section__drag-handle" />
              </td>
            )}
          </SortableSection>
        )}
      </MetaInputs>
    );
  };

  render() {
    return (
      <section>
        <table className="meta-section__table">
          {this.renderHeaderRow()}
          <SortableSectionList
            items={this.props.valueIds}
            lockAxis="y"
            helperClass="sorting-helper meta-section__table__row--sorting"
            onSortEnd={this.props.handleSortSections}
            useDragHandle
            tableView
          >
            {this.props.valueIds.map(this.renderRow)}
          </SortableSectionList>
        </table>
        {this.props.canAddMore && (
          <WpButton className="select-media__trigger" onClick={this.props.addSection} primary>
            {this.props.buttonText}
          </WpButton>
        )}
        {this.renderActionModal()}
      </section>
    );
  }
}

export default TableView;
