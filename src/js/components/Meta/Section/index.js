import { h, Component } from 'preact';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import shortid from 'shortid';
import get from 'lodash.get';
import omit from 'lodash.omit';
import shallowEqual from 'shallowequal';
import { arrayMove } from 'react-sortable-hoc';

import Accordion from '../../Accordion';
import ButtonIncognito from '../../ButtonIncognito';
import DragHandle from '../../DragHandle';
import MetaInputs from '../../MetaInputs';
import SaveInput from '../../SaveInput';
import WpButton from '../../WpButton';
import SectionButtons from './SectionButtons';
import SortableSection from './SortableSection';
import SortableSectionList from './SortableSectionList';
import TableView from './TableView';

const SECTION_ID_KEY = '_id_';

/**
 * A dynamic meta type which lets you define a group of meta a user can then create
 * multiple instances of. A section takes all normal props, as well as `inputs`.
 * Sections can be nested.
 *
 * ## Value
 * **Returns** `any[][]`
 * ```php
 * $sections = json_decode(fx_get_meta('id'), true);
 * foreach($sections as $section) {
 *   $title = $section['title'];
 *   $description = $section['description'];
 *   $image = $section['image'];
 *   // Each section has an auto-generated unique id attached
 *   $id = $section['_id_'];
 * }
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'section',
 *   'className' => 'my-class-name',
 *   'limit' => null,
 *   'defaultOpen' => true,
 *   'tableView' => false,
 *   'useAsTitle' => 'title',
 *   'inputs' => [
 *     'title' => [
 *       'type' => 'textfield',
 *       'title' => 'Title',
 *     ],
 *     'description' => [
 *       'type' => 'wp-editor',
 *       'title' => 'Description',
 *     ],
 *     'image' => [
 *       'type' => 'media',
 *       'className' => 'section-media-class',
 *       'detailed' => true,
 *       'multiple' => false,
 *       'title' => 'Image',
 *     ],
 *   ],
 * ],
 * ```
 */
export default class Section extends Component {
  static displayName = 'Section';

  static propTypes = {
    /**
     * Alternate text for the add button
     */
    buttonText: PropTypes.string,
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    /**
     * A class name prefix to forward onto children elements
     */
    classPrefix: PropTypes.string,
    /**
     * Pass an index to automatically open one of the sections on load. Passing true will open the last.
     */
    defaultOpen: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]),
    id: PropTypes.string.isRequired,
    /**
     * Meta inputs to render
     */
    inputs: PropTypes.object.isRequired,
    /**
     * Limit the number of sections allowed
     */
    limit: PropTypes.number,
    metaRef: PropTypes.string.isRequired,
    /**
     * Noun used to describe the meta type, used when prompting for user actions, e.g. Delete 'noun'
     */
    noun: PropTypes.string.isRequired,
    /**
     * Ask for confirmation before deleting a section
     */
    safeDelete: PropTypes.bool.isRequired,
    /**
     * Allow sections to be sorted
     */
    sortable: PropTypes.bool.isRequired,
    /**
     * Present the section in a table
     */
    tableView: PropTypes.bool,
    /**
     * Metabox title
     */
    title: PropTypes.string,
    /**
     * ID of the input to use as the section title, index will be used by default.
     */
    useAsTitle: PropTypes.string,
    /**
     * Move the inputs to a modal, only available when using table view.
     */
    useModal: PropTypes.bool,
    value: PropTypes.string.isRequired,
  };

  static defaultProps = {
    buttonText: 'Add',
    defaultOpen: true,
    inputs: {},
    limit: undefined,
    noun: 'section',
    safeDelete: true,
    sortable: true,
  };

  constructor(props) {
    super(props);

    const value = {};

    if (Array.isArray(props.value)) {
      props.value.forEach((section) => {
        let id = section[SECTION_ID_KEY];
        if (!id) {
          id = shortid.generate();
          // eslint-disable-next-line no-param-reassign
          section[SECTION_ID_KEY] = id;
        }

        value[id] = section;
      });
    }

    const valueOrderMap = Object.keys(value);
    const sectionCount = valueOrderMap.length;
    let defaultOpen = this.props.defaultOpen ? sectionCount - 1 : -1;

    if (typeof this.props.defaultOpen === 'number') {
      defaultOpen = this.props.defaultOpen;
    }

    this.state = {
      defaultOpen,
      value,
      valueOrderMap,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      !shallowEqual(prevState.value, this.state.value) ||
      !shallowEqual(prevState.valueOrderMap, this.state.valueOrderMap)
    ) {
      if (typeof this.props.onChange === 'function') {
        this.props.onChange(this.value);
      }
    }
  }

  get value() {
    // Save the sections as an array
    return this.state.valueOrderMap.map((id) => this.state.value[id]);
  }

  getClassNames(className) {
    return classnames(className, {
      [`${this.props.classPrefix}${className}`]: this.props.classPrefix,
    });
  }

  handleInputChange = (id, value) => {
    // Deep update the value in our state
    this.setState((prevState) => ({
      value: {
        ...prevState.value,
        [id]: {
          ...prevState.value[id],
          ...value,
        },
      },
    }));
  };

  handleSortSections = ({ oldIndex, newIndex }) => {
    this.setState((prevState) => ({
      valueOrderMap: arrayMove(prevState.valueOrderMap, oldIndex, newIndex),
    }));
  };

  addSection = () => {
    const id = shortid.generate();
    this.setState((prevState) => {
      const value = {
        ...prevState.value,
        [id]: {
          [SECTION_ID_KEY]: id,
        },
      };

      const valueOrderMap = Object.keys(value);
      const defaultOpen = valueOrderMap.length - 1;

      return {
        defaultOpen,
        value,
        valueOrderMap,
      };
    });
  };

  removeSection = (id) => {
    let shouldDelete = true;

    if (this.props.safeDelete) {
      // eslint-disable-next-line no-alert
      shouldDelete = window.confirm(`Are you sure you want to delete this ${this.props.noun}?`);
    }

    if (shouldDelete) {
      this.setState((prevState) => ({
        value: omit(prevState.value, id),
        valueOrderMap: prevState.valueOrderMap.filter((value) => value !== id),
      }));
    }
  };

  moveSection = (id, direction) => {
    this.setState((prevState) => {
      const newOrderMap = [...prevState.valueOrderMap];
      const currentIndex = newOrderMap.findIndex((sectionId) => sectionId === id);

      let newIndex = direction === 'up' ? currentIndex - 1 : currentIndex + 1;

      if (newIndex < 0) {
        newIndex = 0;
      }

      if (newIndex > newOrderMap.length) {
        newIndex = newOrderMap.length;
      }

      if (currentIndex === newIndex) {
        return null;
      }

      newOrderMap.splice(newIndex, 0, newOrderMap.splice(currentIndex, 1)[0]);

      return {
        valueOrderMap: newOrderMap,
      };
    });
  };

  render() {
    const classes = classnames('meta-section', this.props.className, {
      'meta-section--table': this.props.tableView,
    });

    const valueIds = this.state.valueOrderMap;
    const canAddMore = this.props.limit ? valueIds.length < this.props.limit : true;

    if (this.props.tableView) {
      // TODO: Pass getClassNames in here and wrap all elements?
      return (
        <div className={classes}>
          <TableView
            addSection={this.addSection}
            buttonText={this.props.buttonText}
            canAddMore={canAddMore}
            handleInputChange={this.handleInputChange}
            handleSortSections={this.handleSortSections}
            inputs={this.props.inputs}
            moveSection={this.moveSection}
            noun={this.props.noun}
            removeSection={this.removeSection}
            sortable={this.props.sortable}
            useAsTitle={this.props.useAsTitle}
            useModal={this.props.useModal}
            value={this.state.value}
            valueIds={valueIds}
          />
          <SaveInput name={this.props.metaRef} value={this.value} />
        </div>
      );
    }

    return (
      <div className={classes}>
        <div className={this.getClassNames('meta-section__preview')}>
          <SortableSectionList
            items={valueIds}
            lockAxis="y"
            onSortEnd={this.handleSortSections}
            useDragHandle
          >
            {valueIds.map((id, index) => {
              const value = this.state.value[id];
              let title = `#${index + 1}`;

              if (this.props.useAsTitle && get(value, this.props.useAsTitle)) {
                title = get(value, this.props.useAsTitle);
              }

              // If is select option meta
              if (
                this.props.useAsTitle &&
                typeof value === 'object' &&
                value.hasOwnProperty('title')
              ) {
                const optionTitle = value.title;
                title = optionTitle != null && optionTitle !== '' ? optionTitle : title;
              }

              return (
                <SortableSection key={id} index={index}>
                  <Accordion defaultActive={this.state.defaultOpen === index}>
                    {({ active, toggle }) => (
                      <div
                        style={{ 'z-index': valueIds.length - index }}
                        className={classnames(this.getClassNames('meta-section__item'), {
                          'meta-section__item--closed': !active,
                        })}
                      >
                        <div className={this.getClassNames('meta-section__template')}>
                          <div className={this.getClassNames('meta-section__header')}>
                            <strong className={this.getClassNames('meta-section__title')}>
                              {title}
                            </strong>
                            <div className={this.getClassNames('meta-section__controls')}>
                              <ButtonIncognito
                                className={this.getClassNames('meta-section__toggle')}
                                onClick={toggle}
                              >
                                {active ? 'Close' : 'Open'}
                              </ButtonIncognito>
                              <SectionButtons
                                id={id}
                                moveSection={this.moveSection}
                                removeSection={this.removeSection}
                                sortable={this.props.sortable}
                              />
                              {this.props.sortable && (
                                <DragHandle
                                  className={this.getClassNames('meta-section__drag-handle')}
                                />
                              )}
                            </div>
                          </div>
                          <div className={this.getClassNames('meta-section__inputs')}>
                            <MetaInputs
                              id={id}
                              inputs={this.props.inputs}
                              onChange={(values) => this.handleInputChange(id, values)}
                              value={value}
                            />
                          </div>
                        </div>
                      </div>
                    )}
                  </Accordion>
                </SortableSection>
              );
            })}
          </SortableSectionList>
        </div>
        {canAddMore && (
          <WpButton
            className={this.getClassNames('select-media__trigger')}
            onClick={this.addSection}
            primary
          >
            {this.props.buttonText}
          </WpButton>
        )}
        <SaveInput name={this.props.metaRef} value={this.value} />
      </div>
    );
  }
}
