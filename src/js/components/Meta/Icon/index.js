import { h, Component } from 'preact';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import ButtonIncognito from '../../ButtonIncognito';
import Icon from '../../Icon';
import Dropdown from '../../Dropdown';
import WpButton from '../../WpButton';
import SaveInput from '../../SaveInput';

import { getIconsList } from '../../../utils/icons';

/**
 * Select an icon based on icons available from the frontend.
 *
 * ## Value
 * **Returns** `{ name: string, className: string }`
 * ```php
 * $icon = json_decode(fx_get_meta('id'), true);
 * $icon_name = $icon['name'];
 * $icon_class = $icon['className'];
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'icon',
 *   'buttonText' => 'Select an icon',
 *   'className' => 'my-class-name',
 * ],
 * ```
 */
class IconPicker extends Component {
  static displayName = 'Icon';

  static propTypes = {
    /**
     * Alternate text for the select icon button
     */
    buttonText: PropTypes.string,
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    id: PropTypes.string.isRequired,
    metaRef: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    /**
     * Metabox title
     */
    title: PropTypes.string,
    value: PropTypes.string.isRequired,
  };

  static defaultProps = {
    buttonText: 'Select icon',
  };

  constructor(props) {
    super(props);

    this.iconsList = getIconsList();

    this.state = {
      filter: '',
      value: props.value,
    };
  }

  get icons() {
    return this.iconsList.filter((icon) => icon.name.indexOf(this.state.filter) > -1);
  }

  removeIcon = () => {
    this.setState({
      value: undefined,
    });
  };

  selectIcon = (value) => {
    this.setState({
      value,
    });

    if (typeof this.props.onChange === 'function') {
      this.props.onChange(value);
    }
  };

  handleFilterChange = () => {
    this.setState({
      filter: this.input.value,
    });
  };

  renderIcon(icon) {
    return (
      <ButtonIncognito
        className={classnames(
          'meta-icon__preview',
          `meta-icon__preview--${icon.name}`,
          {
            'meta-icon__preview--selected': this.state.value && this.state.value.name === icon.name,
          },
          icon.className,
        )}
        onClick={() => this.selectIcon(icon)}
      />
    );
  }

  render() {
    return (
      <div className={classnames(this.props.className, 'meta-icon')}>
        <Dropdown
          triggerComponent={({ active, toggle }) => (
            <div className="meta-icon__trigger">
              {this.state.value ? (
                <div>
                  <div
                    className={classnames('meta-icon__value-preview', this.state.value.className)}
                  />
                  <div style={{ display: active ? 'none' : 'block' }}>
                    <ButtonIncognito className="fx-text-button" onClick={toggle}>
                      Change
                    </ButtonIncognito>
                    <ButtonIncognito className="fx-text-button" onClick={this.removeIcon}>
                      Remove
                    </ButtonIncognito>
                  </div>
                </div>
              ) : (
                <WpButton onClick={toggle}>{this.props.buttonText}</WpButton>
              )}
            </div>
          )}
        >
          {({ hide }) => (
            <div className="meta-icon__dropdown">
              <header className="meta-icon__dropdown__header">
                <div className="meta-icon__dropdown__search">
                  <input
                    type="text"
                    ref={(c) => {
                      this.input = c;
                    }}
                    placeholder="Filter icons"
                    value={this.state.filter}
                    onKeyUp={this.handleFilterChange}
                  />
                </div>
                <ButtonIncognito className="meta-icon__dropdown__close" onClick={hide}>
                  <Icon.cross size="15px" />
                </ButtonIncognito>
              </header>
              <ul className="meta-icon__list">
                {this.icons.map((icon) => (
                  <li className="meta-icon__list__item" key={icon}>
                    {this.renderIcon(icon)}
                  </li>
                ))}
              </ul>
            </div>
          )}
        </Dropdown>
        <SaveInput name={this.props.metaRef} value={this.state.value} />
      </div>
    );
  }
}

export default IconPicker;
