import { h } from 'preact';
import { SortableContainer } from 'react-sortable-hoc';

const SortableItemsList = props => (
  <ul className="meta-select-order__list">{props.children}</ul>
);

export default SortableContainer(SortableItemsList);
