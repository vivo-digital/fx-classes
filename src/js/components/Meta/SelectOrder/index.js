import { h, Component } from 'preact';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import shallowEqual from 'shallowequal';
import { arrayMove } from 'react-sortable-hoc';

import DragHandle from '../../DragHandle';
import SelectInput from '../../SelectInput';
import SaveInput from '../../SaveInput';
import withOptions from '../../WithOptions/hoc';

import SortableItem from './SortableItem';
import SortableItemsList from './SortableItemsList';

/**
 * Multi select dropdown with drag-and-drop reordering.
 *
 * ## Value
 * **Returns** `(string|number)[]`
 * ```php
 * $value = json_decode(fx_get_meta('id'), true);
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'select-order',
 *   'className' => 'my-class-name',
 *   'creatable' => false,
 *   'excludePostType' => false,
 *   'nOptions' => 4,
 *   'options' => 'page, post',
 *   'placeholder' => 'Please select...',
 * ],
 * ```
 */
class SelectOrder extends Component {
  static displayName = 'SelectOrder';

  static propTypes = {
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    /**
     * Allow new options to be created
     */
    creatable: PropTypes.bool,
    /**
     * A list of ids to exlude from the options list
     */
    exclude: PropTypes.array,
    /**
     * Hide the post type if the option is a post
     */
    excludePostType: PropTypes.bool,
    id: PropTypes.string.isRequired,
    metaRef: PropTypes.string.isRequired,
    /**
     * Options to choose from. See [options](options.md) for more information.
     */
    options: PropTypes.array.isRequired,
    /**
     * Placeholder text for the input
     */
    placeholder: PropTypes.string,
    /**
     * Metabox title
     */
    title: PropTypes.string,
    value: PropTypes.string.isRequired,
  };

  static defaultProps = {
    creatable: false,
    excludePostType: false,
    placeholder: 'Please select...',
  };

  constructor(props) {
    super(props);

    this.state = {
      value: props.value || [],
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (!shallowEqual(prevState.value, this.state.value)) {
      if (typeof this.props.onChange === 'function') {
        this.props.onChange(this.state.value);
      }
    }
  }

  getTitleFromValue(value) {
    const foundOption = this.props.options.find((option) => `${option.id}` === `${value}`);
    if (foundOption) {
      return foundOption.title;
    }

    return value;
  }

  handleInputChange = (value) => {
    this.setState((prevState) => ({
      value: prevState.value.concat(value),
    }));
  };

  handleSortItems = ({ oldIndex, newIndex }) => {
    this.setState((prevState) => ({
      value: arrayMove(prevState.value, oldIndex, newIndex),
    }));
  };

  handleDeleteItem = (index) => {
    this.setState((prevState) => ({
      value: prevState.value.filter((v, i) => i !== index),
    }));
  };

  render() {
    const classes = classnames('meta-select-order', this.props.className);

    const listItems = this.state.value.map((item, index) => {
      const id = item;

      return (
        <SortableItem
          // eslint-disable-next-line react/no-array-index-key
          key={`${id}-${index}`}
          index={index}
          itemIndex={index}
          onDelete={this.handleDeleteItem}
        >
          {this.getTitleFromValue(id)}
          <DragHandle className="meta-select-order__drag-handle" />
        </SortableItem>
      );
    });

    return (
      <div className={classes}>
        <SelectInput
          creatable={this.props.creatable}
          excludePostType={this.props.excludePostType}
          id={this.props.id}
          isMulti={this.props.isMulti}
          key={this.props.id}
          name={this.props.metaRef}
          onChange={(value) => this.handleInputChange(value)}
          options={this.props.options}
          placeholder={this.props.placeholder}
        />
        <SortableItemsList
          helperClass="sorting-helper"
          items={this.state.value}
          lockAxis="y"
          onSortEnd={this.handleSortItems}
          useDragHandle
        >
          {listItems}
        </SortableItemsList>
        <SaveInput name={this.props.metaRef} value={this.state.value} />
      </div>
    );
  }
}

export default withOptions(SelectOrder);
