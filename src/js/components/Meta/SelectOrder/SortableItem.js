import { h, Component } from 'preact';
import { SortableElement } from 'react-sortable-hoc';

import ButtonIncognito from '../../ButtonIncognito';
import Icon from '../../Icon';

class Item extends Component {
  handleDelete = () => {
    this.props.onDelete(this.props.itemIndex);
  };

  render() {
    return (
      <li {...this.props} className="meta-select-order__item">
        {this.props.children}
        <ButtonIncognito className="meta-select-order__item__delete" onClick={this.handleDelete}>
          <Icon.cross />
        </ButtonIncognito>
      </li>
    );
  }
}

export default SortableElement(Item);
