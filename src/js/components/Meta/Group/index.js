import { h, Component } from 'preact';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import shallowEqual from 'shallowequal';
import cloneDeep from 'lodash.clonedeep';

import WpButton from '../../WpButton';
import ButtonIncognito from '../../ButtonIncognito';
import CheckboxInput from '../../CheckboxInput';
import MetaInputs from '../../MetaInputs';
import Modal from '../../Modal';
import SaveInput from '../../SaveInput';

const OPEN_ID_KEY = '_open_';

/**
 * A dynamic group of meta inputs which are available in a variety of display options.
 * A group takes all normal props, as well as `inputs`. Groups can be nested.
 *
 * ## Value
 * **Returns** `any[]`
 * ```php
 * $group = json_decode(fx_get_meta('id'), true);
 * $title = $group['title'];
 * $description = $group['description'];
 * $image = $group['image'];
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'group',
 *   'className' => 'my-class-name',
 *   'display' => 'accordion',
 *   'triggerText' => 'Has extra content',
 *   'triggerType' => 'checkbox',
 *   'triggerClass' => 'my-class-name__trigger',
 *   'inputs' => [
 *     'title' => [
 *       'type' => 'textfield',
 *       'title' => 'Title',
 *     ],
 *     'description' => [
 *       'type' => 'wp-editor',
 *       'title' => 'Description',
 *     ],
 *     'image' => [
 *       'type' => 'media',
 *       'className' => 'section-media-class',
 *       'detailed' => true,
 *       'multiple' => false,
 *       'title' => 'Image',
 *     ],
 *   ],
 * ],
 * ```
 */
export default class Group extends Component {
  static displayName = 'Group';

  static propTypes = {
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    id: PropTypes.string.isRequired,
    /**
     * Meta inputs to render
     */
    inputs: PropTypes.object.isRequired,
    /**
     * Display type for the inputs, defaults to 'accordion'
     */
    display: PropTypes.oneOf(['modal', 'accordion']).isRequired,
    /**
     * Text for the trigger element
     */
    triggerText: PropTypes.string,
    /**
     * Type of trigger to use to display the inputs, defaults to 'text'
     */
    triggerType: PropTypes.oneOf(['button', 'checkbox', 'text']),
    /**
     * Trigger element class name
     */
    triggerClass: PropTypes.string,
    metaRef: PropTypes.string.isRequired,
    /**
     * Metabox title
     */
    title: PropTypes.string,
    value: PropTypes.string.isRequired,
  };

  static defaultProps = {
    display: 'accordion',
    triggerText: 'Enter options',
    triggerType: 'text',
    inputs: {},
  };

  constructor(props) {
    super(props);

    const value = cloneDeep(props.value) || {};
    // Modals should never open by default
    // This may cause issues with triggerType checkbox, but she'll be right for now
    const open = Boolean(value[OPEN_ID_KEY]) && props.display !== 'modal';

    this.state = {
      open,
      value,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (!shallowEqual(prevState.value, this.state.value)) {
      if (typeof this.props.onChange === 'function') {
        this.props.onChange(this.state.value);
      }
    }
  }

  handleInputChange = (value) => {
    this.setState({
      value,
    });
  };

  handleOpenChange = (open) => {
    this.setState((prevState) => {
      if (this.props.triggerType === 'checkbox' && !open) {
        // If using a checkbox, clear the value when closing
        return {
          open,
          value: {
            [OPEN_ID_KEY]: open,
          },
        };
      }

      return {
        open,
        value: {
          ...prevState.value,
          [OPEN_ID_KEY]: open,
        },
      };
    });
  };

  toggleInputs = () => {
    this.handleOpenChange(!this.state.open);
  };

  showInputs = () => {
    this.handleOpenChange(true);
  };

  hideInputs = () => {
    this.handleOpenChange(false);
  };

  renderModalDisplay(inputs) {
    return (
      <Modal active={this.state.open} close={this.hideInputs} width="400px">
        <div className="meta-group__modal">
          {inputs}
          <div className="meta-group__modal__footer">
            <WpButton onClick={this.hideInputs}>Close</WpButton>
          </div>
        </div>
      </Modal>
    );
  }

  renderAccordionDisplay(inputs) {
    return (
      <div
        className={classnames('meta-group__accordion', {
          'meta-group__accordion--open': this.state.open,
        })}
      >
        {inputs}
      </div>
    );
  }

  renderInputs() {
    const inputs = (
      <div className="meta-group__inputs">
        <MetaInputs
          id={this.props.id}
          inputs={this.props.inputs}
          onChange={this.handleInputChange}
          value={this.state.value}
        />
      </div>
    );

    switch (this.props.display) {
      case 'modal':
        return this.renderModalDisplay(inputs);
      default:
        return this.renderAccordionDisplay(inputs);
    }
  }

  renderButtonTrigger(className) {
    return (
      <WpButton
        className={classnames(className, 'meta-group__trigger--button')}
        onClick={this.toggleInputs}
      >
        {this.props.triggerText}
      </WpButton>
    );
  }

  renderCheckboxTrigger(className) {
    return (
      <label className={classnames(className, 'meta-group__trigger--checkbox')}>
        <CheckboxInput
          defaultChecked={this.state.open}
          className={classnames('meta-group__trigger__checkbox')}
          onChange={this.toggleInputs}
        />
        {this.props.triggerText}
      </label>
    );
  }

  renderTextTrigger(className) {
    return (
      <ButtonIncognito
        className={classnames(className, 'meta-group__trigger--default')}
        onClick={this.toggleInputs}
      >
        {this.props.triggerText}
      </ButtonIncognito>
    );
  }

  renderTrigger() {
    const triggerClasses = classnames('meta-group__trigger', this.props.triggerClass, {
      'meta-group__trigger--open': this.state.open,
    });

    switch (this.props.triggerType) {
      case 'button':
        return this.renderButtonTrigger(triggerClasses);
      case 'checkbox':
        return this.renderCheckboxTrigger(triggerClasses);
      default:
        return this.renderTextTrigger(triggerClasses);
    }
  }

  render() {
    const classes = classnames('meta-group', this.props.className);

    return (
      <div className={classes}>
        {this.renderTrigger()}
        {this.renderInputs()}
        <SaveInput name={this.props.metaRef} value={this.state.value} />
      </div>
    );
  }
}
