import { h, Component } from 'preact';

import SaveInput from '../../SaveInput';

import PropTypes from 'prop-types';
import classnames from 'classnames';

/**
 * WP RichText
 * Render a rich contenteditable input, providing users with the option to format the content.
 * [RichText Docs](https://github.com/WordPress/gutenberg/tree/trunk/packages/block-editor/src/components/rich-text). Note, this is a simple (dumb) input and only allows for bolding and italics via keyboard shortcuts. There are no controls. Also honours paragraph spacing. Useful for when you need multiple paragraphs but no list formatting options.
 *
 * ## Value
 * **Returns** `string`
 * ```php
 * $value = fx_get_meta('id');
 * ```
 *
 * ## Example
 * ```php
 * 'id' => [
 *   'type' => 'wp-rich-text',
 *   'title' => 'Some content',
 *   'description' => 'Some content',
 *   'className' => 'my-class-name',
 *   'defaultChecked' => false,
 *   'label' => 'Enter something below',
 *   'multiline' => true,
 * ],
 * ```
 */
class WpRichText extends Component {
  static displayName = 'WpRichText';

  static propTypes = {
    /**
     * Metabox class name
     */
    className: PropTypes.string,
    /**
     * Input is checked by default
     */
    id: PropTypes.string.isRequired,
    /**
     * Label that appears as a placeholder
     */
    label: PropTypes.string,
    metaRef: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    /**
     * Metabox title
     */
    title: PropTypes.string,
    description: PropTypes.string,
    value: PropTypes.string.isRequired,
    /**
     * Creates new paragraphs on enter
     */
    multiline: PropTypes.bool,
  };

  static defaultProps = {
    multiline: true,
  };

  constructor(props) {
    super(props);
    this.state = {
      value: '',
    };
  }

  componentDidMount() {
    setTimeout(() => {
      const el = document.getElementById(`wp-${this.props.id}`);
      if (!el) {
        return;
      }
      this.renderWpRichText();
    }, 30);
  }

  componentDidUpdate() {
    this.renderWpRichText();
  }

  componentWillUnmount() {
    try {
      wp.element.unmountComponentAtNode(this.wrapEl);
    } catch (err) {
      // don't worry about this for now
    }
  }

  handleTextChange = (newValue) => {
    this.setState({
      value: newValue,
    });
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(newValue);
    }
  };

  renderWpRichText = () => {
    const element = document.getElementById(`wp-${this.props.id}`);
    if (element) {
      wp.element.render(
        wp.element.createElement(wp.blockEditor.RichText, {
          value: this.props.value,
          onChange: this.handleTextChange,
          placeholder: this.props.label,
          multiline: this.props.multiline,
        }),
        element,
      );
    }
  };

  render() {
    return (
      <div className={classnames(this.props.className, 'meta-wp-rich-text')}>
        <div id={`wp-${this.props.id}`} />
        <SaveInput name={this.props.metaRef} value={this.state.value} />
      </div>
    );
  }
}

export default WpRichText;
