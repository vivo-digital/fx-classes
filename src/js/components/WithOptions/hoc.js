import { h } from 'preact';
import WithOptions from './index';

export default function withOptions(WrappedComponent) {
  const ComponentWithOptions = props => (
    <WithOptions
      options={props.options}
      optionsArgs={{
        post__not_in: props.exclude,
      }}
    >
      {options => <WrappedComponent {...props} options={options} />}
    </WithOptions>
  );

  ComponentWithOptions.displayName = WrappedComponent.displayName;

  return ComponentWithOptions;
}
