import { h, Component } from 'preact';

import withPromiseHandler from '../../utils/withPromiseHandler';
import { fetchOptions } from '../../api/options';

class WithOptions extends Component {
  constructor(props) {
    super(props);

    let options;
    if (Array.isArray(props.options)) {
      options = props.options;
    }

    this.state = {
      options,
    };
  }

  componentDidMount() {
    if (this.state.options) {
      return;
    }

    this.props.fetchOptions.run(this.props.options, this.props.optionsArgs).then(options => {
      this.setState({
        options,
      });
    });
  }

  componentWillUnmount() {
    // Cancel the options request
    this.props.fetchOptions.cancel();
  }

  render() {
    // TODO: Loading spinner?
    if (!this.state.options) {
      return null;
    }

    return this.props.children[0](this.state.options);
  }
}

export default withPromiseHandler({ fetchOptions })(WithOptions);
