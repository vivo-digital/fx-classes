import { h } from 'preact';

import { sanitizeValue } from '../../utils/helpers';

const SaveInput = props => (
  <input
    type="hidden"
    className={props.className}
    name={props.name}
    value={sanitizeValue(props.value)}
  />
);

SaveInput.displayName = 'SaveInput';

export default SaveInput;
