import { h } from 'preact';
import classnames from 'classnames';

const TextareaInput = props => (
  <textarea
    {...props}
    className={classnames('meta-field__input', props.className)}
    ref={props.getInputRef}
  />
);

TextareaInput.displayName = 'TextareaInput';

export default TextareaInput;
