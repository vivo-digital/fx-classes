import { h, Component } from 'preact';
import classnames from 'classnames';

import ButtonIncognito from '../ButtonIncognito';

class WpButton extends Component {
  static defaultProps = {
    type: 'button',
  };

  state = {
    hasAsked: false,
  };

  cancel = () => {
    this.setState({
      hasAsked: false,
    });
  };

  handleClick = () => {
    if (this.props.confirm && !this.state.hasAsked) {
      this.setState({
        hasAsked: true,
      });
      return;
    }

    if (typeof this.props.onClick === 'function') {
      this.props.onClick();
    }
  };

  /* eslint-disable react/button-has-type */
  render() {
    return (
      <span>
        <button
          disabled={this.props.disabled}
          type={this.props.type}
          className={classnames('button', 'components-button', this.props.className, {
            'button-primary': this.props.primary,
          })}
          onClick={this.handleClick}
        >
          {this.state.hasAsked ? this.props.confirm : this.props.children}
        </button>
        {Boolean(this.props.confirm && this.state.hasAsked) && (
          <ButtonIncognito onClick={this.cancel}>Cancel</ButtonIncognito>
        )}
      </span>
    );
  }
}

export default WpButton;
