import { h, Component } from 'preact';
import PropTypes from 'prop-types';
import Dayzed from 'dayzed';
import classnames from 'classnames';

import ButtonIncognito from '../ButtonIncognito';

import { weekdayNamesShort } from './constants';
import { getMonthName } from './utils';

class DatePicker extends Component {
  static propTypes = {
    minDate: PropTypes.instanceOf(Date),
    maxDate: PropTypes.instanceOf(Date),
    mode: PropTypes.oneOf(['single', 'range', 'multiple']).isRequired,
    onDateSelected: PropTypes.func,
    value: PropTypes.oneOfType([PropTypes.instanceOf(Date), PropTypes.array]),
  };

  static defaultProps = {
    mode: 'single',
  };

  state = { hoveredDate: null, offset: 0 };

  // Calendar level
  onMouseLeave = () => {
    this.setState({ hoveredDate: null });
  };

  // Date level
  onMouseEnter(date) {
    if (!this.props.value.length) {
      return;
    }
    this.setState({ hoveredDate: date });
  }

  get defaultDate() {
    if (Array.isArray(this.props.value)) {
      return this.props.value[0];
    }

    return this.props.value || undefined;
  }

  isInRange = date => {
    if (this.props.mode !== 'range') {
      return false;
    }

    const { value } = this.props;
    const { hoveredDate } = this.state;
    if (Array.isArray(value) && value.length) {
      const firstSelected = value[0].getTime();
      if (value.length === 2) {
        const secondSelected = value[1].getTime();
        return firstSelected < date && secondSelected > date;
      }

      return Boolean(
        hoveredDate
          && ((firstSelected < date && hoveredDate >= date)
            || (date < firstSelected && date >= hoveredDate)),
      );
    }
    return false;
  };

  handleDateSelected = ({ selected, selectable, date }) => {
    if (!selectable) {
      return;
    }

    let value = date;

    if (this.props.mode === 'range') {
      const dateTime = date.getTime();
      value = Array.isArray(this.props.value) ? [...this.props.value] : [];

      if (value.length) {
        if (value.length === 1) {
          const firstTime = value[0].getTime();
          if (firstTime < dateTime) {
            value.push(date);
          } else {
            value.unshift(date);
          }
        } else if (value.length === 2) {
          // Restarting the selection
          value = [date];
        }
      } else {
        value.push(date);
      }
    }

    if (this.props.mode === 'multiple') {
      value = Array.isArray(this.props.value) ? [...this.props.value] : [];

      const selectedValue = date.getTime();
      if (selected) {
        // Remove
        value = value.filter(d => d.getTime() !== selectedValue);
      } else {
        // Add
        value.push(date);
      }

      value.sort((a, b) => new Date(a.getTime()) - new Date(b.getTime()));
    }

    if (typeof this.props.onDateSelected === 'function') {
      this.props.onDateSelected(value);
    }
  };

  handleOffsetChanged = offset => {
    this.setState({ offset });
  };

  transformMonthProps(props) {
    return {
      ...props,
      onClick: e => {
        // Override the onClick event so we can stop the event bubbling up to the window
        e.stopPropagation();
        props.onClick(e);
      },
    };
  }

  render() {
    return (
      <Dayzed
        minDate={this.props.minDate}
        maxDate={this.props.maxDate}
        offset={this.state.offset}
        onOffsetChanged={this.handleOffsetChanged}
        onDateSelected={this.handleDateSelected}
        selected={this.props.value}
        date={this.defaultDate}
      >
        {({
          calendars, getBackProps, getForwardProps, getDateProps,
        }) => {
          if (calendars.length) {
            return (
              <div className="meta-date__wrap">
                {calendars.map(calendar => (
                  <div
                    className="meta-date__calendar"
                    key={`${calendar.month}${calendar.year}`}
                  >
                    <header className="meta-date__header">
                      <ButtonIncognito
                        className="meta-date__month-btn"
                        {...this.transformMonthProps(getBackProps({ calendars }))}
                      >
                        {getMonthName(calendar.month - 1)}
                      </ButtonIncognito>
                      {getMonthName(calendar.month)} {calendar.year}
                      <ButtonIncognito
                        className="meta-date__month-btn"
                        {...this.transformMonthProps(getForwardProps({ calendars }))}
                      >
                        {getMonthName(calendar.month + 1)}
                      </ButtonIncognito>
                    </header>
                    <div
                      className="meta-date__calendar-body"
                      key={`${calendar.year}-${calendar.firstDayOfMonth}`}
                      onMouseLeave={this.onMouseLeave}
                    >
                      {weekdayNamesShort.map(weekday => (
                        <span
                          className="meta-date__week-day"
                          key={`${calendar.month}${calendar.year}${weekday}`}
                        >
                          {weekday}
                        </span>
                      ))}
                      {calendar.weeks.map((week, windex) => week.map((dateObj, index) => {
                        const key = `${calendar.month}${calendar.year}${windex}${index}`;

                        if (!dateObj) {
                          return <span className="meta-date__date-number" key={key} />;
                        }
                        const {
                          date, selected, selectable, today,
                        } = dateObj;
                        return (
                          <ButtonIncognito
                            className={classnames(
                              'meta-date__date-number',
                              'meta-date__date-btn',
                              {
                                'meta-date__date-btn--selected': selected,
                                'meta-date__date-btn--unavailable': !selectable,
                                'meta-date__date-btn--today': today,
                                'meta-date__date-btn--in-range': this.isInRange(date),
                              },
                            )}
                            key={key}
                            {...getDateProps({
                              dateObj,
                              onMouseEnter:
                                  this.props.mode === 'range'
                                    ? () => {
                                      this.onMouseEnter(date);
                                    }
                                    : null,
                            })}
                          >
                            {selectable ? date.getDate() : 'X'}
                          </ButtonIncognito>
                        );
                      }))}
                    </div>
                  </div>
                ))}
              </div>
            );
          }
          return null;
        }}
      </Dayzed>
    );
  }
}

export default DatePicker;
