import { monthNamesShort } from './constants';

export const getMonthName = (index) => {
  let realIndex = index;

  if (index >= monthNamesShort.length) {
    realIndex = index - monthNamesShort.length;
  }

  if (index < 0) {
    realIndex = monthNamesShort.length - Math.abs(index);
  }

  return monthNamesShort[realIndex];
}
