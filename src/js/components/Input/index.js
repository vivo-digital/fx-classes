import { h } from 'preact';
import classnames from 'classnames';

const Input = props => (
  <input
    {...props}
    className={classnames('meta-field__input', props.className)}
    ref={props.getInputRef}
  />
);

Input.defaultProps = {
  type: 'text',
};

Input.displayName = 'Input';

export default Input;
