import calendar from './calendar.svg';
import chevron from './chevron.svg';
import cross from './cross.svg';
import drag from './drag.svg';
import file from './file.svg';
import move from './move.svg';
import pencil from './pencil.svg';

export {
  calendar,
  chevron,
  cross,
  drag,
  file,
  move,
  pencil,
};
