import { h } from 'preact';
import SVGInline from 'react-svg-inline';
import classnames from 'classnames';

import * as svgs from './assets';

const Icon = ({ size, svg }) => (
  <SVGInline
    className={classnames('fx-icon', `fx-icon--${svg}`)}
    svg={svgs[svg]}
    style={{ width: size, height: size }}
  />
);

Icon.defaultProps = {
  size: undefined,
};

// Export all icons in a single object and then use an icon like so: <Icon.plus />
const allIcons = {};

Object.keys(svgs).forEach(svg => {
  allIcons[svg] = props => <Icon {...props} svg={svg} />;
});

export default allIcons;
