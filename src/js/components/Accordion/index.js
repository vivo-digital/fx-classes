import { h, Component } from 'preact';

class Accordion extends Component {
  static defaultProps = {
    defaultActive: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      active: props.defaultActive,
    };
  }

  show = () => {
    this.setState({
      active: true,
    });
  };

  hide = () => {
    this.setState({
      active: false,
    });
  };

  toggle = () => {
    this.setState(prevState => ({
      active: !prevState.active,
    }));
  };

  render() {
    return (
      <div>
        {this.props.children[0]({
          active: this.state.active,
          show: this.show,
          hide: this.hide,
          toggle: this.toggle,
        })}
      </div>
    );
  }
}

export default Accordion;
