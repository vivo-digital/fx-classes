import styled, { css } from 'styled-components';
import { color } from 'styles/helpers';

export const Box = styled.div`
  width: 100%;
  max-width: ${props => props.width};
  height: ${props => props.height};
`;

export const Content = styled.div`
  position: relative;
  width: 100%;
  padding: ${props => props.padding};
  background-color: ${color('white')};
  border-radius: 5px;
  z-index: 10;
  overflow: hidden;
`;

export const Shadow = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: ${color('ebonyClay')};
  opacity: 0;
  z-index: -1;
  transition: opacity 0.35s;

  ${props =>
    props.active &&
    css`
      opacity: 0.5;
    `};
`;

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  top: 0;
  left: 0;
  width: 0;
  min-height: 100%;
  transition: opacity 0.35s, width 0s 0.35s, z-index 0s 0.35s;
  opacity: 0;
  overflow: hidden;
  z-index: ${props => props.theme.layer.behind};
  pointer-events: none;

  ${props =>
    props.active &&
    css`
      width: 100%;
      opacity: 1;
      overflow-y: overlay;
      -webkit-overflow-scrolling: touch;
      pointer-events: all;
      transition: opacity 0.35s;
      z-index: ${props.theme.layer.modal};
    `};
`;
