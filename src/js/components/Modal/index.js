import { h, Component } from 'preact';
import { PropTypes } from 'prop-types';
import classnames from 'classnames';

import { Tunnel } from '../Tunnel';

import { KEY_CODES, TUNNELS } from '../../constants';

class Modal extends Component {
  componentDidMount() {
    window.addEventListener('keydown', this.handleKeyDown);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeyDown);
  }

  handleKeyDown = e => {
    if (!this.props.active) {
      return;
    }

    if (e.which === KEY_CODES.ESC) {
      if (this.props.closeOnEscape) {
        this.props.close(e);
      }
    }
  };

  render() {
    const {
      active, children, width, height,
    } = this.props;

    if (!active) {
      return null;
    }

    return (
      <Tunnel id={TUNNELS.MODAL}>
        <div className={classnames('fx-modal__wrapper', { 'fx-modal__wrapper--active': active })}>
          <div
            className={classnames('fx-modal__shadow', { 'fx-modal__shadow--active': active })}
            onClick={this.props.close}
          />
          <div className="fx-modal__box" style={{ maxWidth: width, height }}>
            <div className="fx-modal__content">{children}</div>
          </div>
        </div>
      </Tunnel>
    );
  }
}

Modal.propTypes = {
  active: PropTypes.bool,
  children: PropTypes.node,
  close: PropTypes.func.isRequired,
  closeOnEscape: PropTypes.bool,
  height: PropTypes.string,
  width: PropTypes.string,
};

Modal.defaultProps = {
  closeOnEscape: true,
  height: 'auto',
  width: '580px',
};

export default Modal;
