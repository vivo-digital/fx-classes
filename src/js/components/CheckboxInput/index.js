import { h } from 'preact';

const CheckboxInput = props => <input {...props} type="checkbox" ref={props.getInputRef} />;

CheckboxInput.displayName = 'CheckboxInput';

export default CheckboxInput;
