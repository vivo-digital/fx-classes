const kebabCase = require('lodash.kebabcase');

function generateFrontMatter({ id, title, label }) {
  return `---
id: ${id}
title: ${title}
sidebar_label: ${label}
---
`;
}

function generateTitle(name) {
  return `# ${name}`;
}

function generateDesciption(description) {
  return `${description}\n`;
}

function generateExample(name, props) {
  return `
Example:
\`\`\`
'id' => [\n\t'type' => '${kebabCase(name)}',
${Object.keys(props)
.sort()
.map((propName) => {
  const value = props[propName];
  return `\t'${propName}' => ${typeof value === 'string' && value[0] !== "'"  ? `'${value}'` : value},`;
})
.join('\n')}
];
\`\`\`
`;
}

function generatePropType(type) {
  let values;
  if (Array.isArray(type.value)) {
    values = `(${
      type.value
        .map((typeValue) => {
          return typeValue.name || typeValue.value;
        })
        .join('|')
    })`;
  } else {
    values = type.value;
  }

  return `type: \`${type.name}${values || ''}\`\n`;
}

function generatePropDefaultValue(value) {
  return `defaultValue: \`${value.value}\`\n`;
}

function generateProp(propName, prop) {
  return (
    `#### \`${
      propName
    }\`${
      prop.required ? ' (required)' : ''
    }\n`
    + `\n${
      prop.description ? `${prop.description}\n\n` : ''
    }${prop.type ? generatePropType(prop.type) : ''
  }${prop.defaultValue ? `\n${generatePropDefaultValue(prop.defaultValue)}` : ''
    }\n`
  );
}

function generateProps(props = {}) {
  return (
    `\n${
      Object.keys(props)
        .sort()
        .map((propName) => {
          return generateProp(propName, props[propName]);
        })
        .join('\n')}`
  );
}

function generateMarkdown(name, reactAPI) {
  const frontMatter = {
    id: kebabCase(name),
    title: name,
    label: name,
  }
  const markdownString = `${generateFrontMatter(frontMatter)}\n${
    generateDesciption(reactAPI.description)
  }\n## Props\n${
    generateProps(reactAPI.props)
  }\n`;

  return markdownString;
}

module.exports = generateMarkdown;
