const fs = require('fs');
const path = require('path');
const glob = require('glob');
const reactDocs = require('react-docgen');
const kebabCase = require('lodash.kebabcase');

const generateMarkdown = require('./generateMarkdown');

const OUTPUT_DIR =
  process.env.OUTPUT_DIR || path.resolve(__dirname, '..', '..', 'fx-classes-website', 'docs');
const OUTPUT_INDEX = path.resolve(OUTPUT_DIR, 'README.md');
const COMPONENTS_DIR = path.resolve(__dirname, '..', 'src', 'js', 'components', 'Meta');
const IGNORE_PROPS = ['value', 'id', 'metaRef', 'onChange', 'onBlur'];

const components = glob.sync(path.join(COMPONENTS_DIR, '**', 'index.js'));

let markdown = '';
const componentTypes = [];

function getFileName(name) {
  return `${kebabCase(name)}.md`;
}

components.forEach((filePath) => {
  const name = path.dirname(path.relative(COMPONENTS_DIR, filePath));
  if (name === '.') {
    return;
  }

  const src = fs.readFileSync(filePath);
  const componentInfo = reactDocs.parse(src);

  if (componentInfo.props) {
    IGNORE_PROPS.forEach((prop) => {
      delete componentInfo.props[prop];
    });
  }

  componentTypes.push([name, componentInfo]);

  const componentMarkdown = generateMarkdown(name, componentInfo);
  const fileName = path.join(OUTPUT_DIR, getFileName(name));
  fs.writeFileSync(fileName, componentMarkdown);
});

markdown = `### Meta Types\n\n${componentTypes
  .map(([name, componentInfo]) => {
    const link = getFileName(name);
    return `### [${name}](${link})\n${componentInfo.description.split('**Value')[0]}\n`;
  })
  .join('')}\n\n`;

fs.writeFileSync(OUTPUT_INDEX, markdown);
