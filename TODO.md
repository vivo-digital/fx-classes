## TODO

- Revert default value iot's not gonna work
- Add message about defaultValue still requiring first save
- Return mailgun status code and message body in response.
- Take phone number out of site config?
- FIX get_opts function cross compatabile with get_config for new
- If move GTM to Site Config it will require backwards compat for old projects to make sure it pulls from site_config || site_opts
- add email template code (later)
- sitemap migration (later)
- migrate fx-functions to fx-classes (later)
- remove section code, form code and other redundancies from blank

## Breaking

- get_opts see helpers get_config
- FX Classes is private now, will this affect anything?? auth.json or repman.io

## Later to do

- Remove sections
- Forms etc
